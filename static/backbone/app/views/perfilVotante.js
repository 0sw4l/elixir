var app = app || {};

var $contactado = '#contactado_input';
var $no_volver_a_llamar = '#no_volver_a_llamar_input';
var $request_votante_object_info = {};
var $request_votante_objects_checks = {};
var $update_status_info = false;
var $update_status_checks = false;

app.mainView = Backbone.View.extend({
    el: '#app',

    events: {
        'click .click': 'getValue',
        'change .toggle-info': 'setCheckValue',
        'click #departamento': 'changeDepartamento',
        'change #departamento_input': 'fetchMunicipios',
        'click #municipio': 'changeMunicipio',
        'change #municipio_input': 'changeValueMunicipio',
        'click #no_volver_a_llamar_input': 'noCallAgain',
        'click #contactado_input': 'wasCalled',
        'click #guardar': 'enviarActualizacion',
        'click #back': 'handleBackEvent'
    },

    initialize: function () {
        this.fetchDepartamentos();
        app.webSocketBridge = new channels.WebSocketBridge();
        app.webSocketBridge.connect(app.ws_path);
        this.onBindData();
        this.onListen();
    },

    onBindData: function () {
        var self = this;
        app.webSocketBridge.listen(function (votante) {

        });
    },

    onListen: function () {
        app.webSocketBridge.socket.onopen = function () {
            console.log("Connected to notification socket");
        };

        app.webSocketBridge.socket.onclose = function () {
            console.log("Disconnected to notification socket");
        };
    },

    updateStatusInfo: function () {
        $update_status_info = true;
        app.showDomElement('#guardar');
        $('#back').css("right", "105px");
    },

    updateStatusCheks: function () {
        $update_status_checks = true;
        app.showDomElement('#guardar');
        $('#back').css("right", "105px");
    },

    getValue: function (e) {
        e.preventDefault();
        var $element = $(e.currentTarget);
        var id = $element.attr('id');
        var text = $element.text();
        var selector = '#' + id + '_input';
        var $input = this.getInput(selector);
        if (text !== 'Agregar') {
            $input.val(text);
        }
        app.showDomElement(selector);
        app.hideDomElement('#' + id);
        var self = this;
        $input.focusout(function () {
            app.showDomElement('#' + id);
            app.hideDomElement(selector);
            if ($input.val() === '' || $input.val() === ' ' || $input.val() === '  ') {
                $element.text('Agregar');
            } else {
                if ($input.val() !== text) {
                    $request_votante_object_info[id] = $input.val();
                    $element.text($input.val());
                    if (parseInt($input.val()) >= 1 && !$input.attr('disable-int')) {
                        $element.text($(selector + ' option:selected').text());
                    }
                    self.updateStatusInfo();
                }
            }

        });
    },

    getInput: function (selector) {
        return $(selector);
    },

    setCheckValue: function (e) {
        e.preventDefault();
        var $element = $(e.currentTarget);
        var id = $element.attr('id');
        $request_votante_objects_checks[id] = this.getCheckValue($('#' + id + '_input'));
        this.updateStatusCheks();
    },

    getCheckValue: function (selector) {
        return !!$(selector).is(":checked");
    },

    fetchDepartamentos: function () {
        var dropdown = $('#departamento_input');
        var url = '/api/geodata/viewsets/departamentos_/';
        $.getJSON(url, function (data) {
            $.each(data, function (key, item) {
                $("<option></option>", {
                    value: item.id,
                    text: item.nombre
                }).appendTo(dropdown);
            })
        });
    },

    fetchMunicipios: function (e) {
        this.changeValueDepartamento();
        app.resetSelect('#municipio_input', app.DEFAULT_MSG_MUNICIPIO);
        if (!isNaN(e.target.value)) {
            var id = parseInt(e.target.value);
            var dropdown = $('#municipio_input');
            var url = '/api/geodata/viewsets/departamentos_/' + id + '/municipios_/';
            $.getJSON(url, function (data) {
                $.each(data.municipios, function (key, item) {
                    $("<option></option>", {
                        value: item.id,
                        text: item.nombre
                    }).appendTo(dropdown);
                });
            });
            app.showDomElement('#select_municipios');
        } else {
            app.hideDomElement('#select_municipios');
            app.hideDomElement('#select_corregimientos');
        }
    },

    changeDepartamento: function () {
        app.hideDomElement('#departamento');
        app.showDomElement('#departamento_filter');
    },

    changeValueDepartamento: function () {
        var data = $('#departamento_input').select2('data');
        $('#departamento').text(data[0].text);
        app.showDomElement('#departamento');
        app.hideDomElement('#departamento_filter');
        $request_votante_object_info['departamento'] = app.int($('#departamento_input').val());
        this.updateStatusInfo();
    },

    changeMunicipio: function () {
        app.hideDomElement('#municipio');
        app.showDomElement('#municipio_filter');
    },

    changeValueMunicipio: function () {
        var data = $('#municipio_input').select2('data');
        $('#municipio').text(data[0].text);
        app.showDomElement('#municipio');
        app.hideDomElement('#municipio_filter');
        $request_votante_object_info['municipio'] = app.int($('#municipio_input').val())
        this.updateStatusInfo();
    },

    noCallAgain: function () {
        if ($($no_volver_a_llamar).prop('checked') === true) {
            if ($($contactado).prop('checked') === false) {
                $($contactado).prop('checked', true);
                if (this.getCheckValue($('#contactado_input')) !== false) {
                    $request_votante_objects_checks['contactado'] = true;
                } else {
                    $request_votante_objects_checks['contactado'] = false;
                }
            }
            $request_votante_objects_checks['no_volver_a_llamar'] = true;
        } else {
            $request_votante_objects_checks['no_volver_a_llamar'] = false;
        }
        this.updateStatusCheks();
    },

    wasCalled: function () {
        $request_votante_objects_checks['contactado'] = this.getCheckValue($('#contactado_input'));
        this.updateStatusCheks();
    },

    enviarActualizacion: function () {
        var self = this;
        if ($update_status_checks || $update_status_info) {
            if ($update_status_checks) {
                setTimeout(function () {
                    self.ajaxRequestCheck($request_votante_objects_checks)
                }, 1000);
            }
            if ($update_status_info) {
                setTimeout(function () {
                    self.ajaxRequestInfo($request_votante_object_info)
                }, 2000);
            }
            app.hideDomElement('#guardar');
            $('#back').removeAttr('style');
        }
    },

    ajaxRequestCheck: function (data) {
        data['last_update_user'] = window.user_id;
        data['id'] = window.votante_id;
        if (navigator.onLine) {
            $.ajax({
                url: '/api/votantes/update_votantes/',
                method: 'POST',
                dataType: "json",
                data: data,
                success: function (data) {
                    app.showNotify('Datos del votante actualizados ;)', 'info', app.SHORT_MORE_LENGHT);
                    $request_votante_objects_checks = {};
                    $update_status_checks = false;
                },
                error: function (xhr, status, error) {
                    app.showCommonAlert('error',
                        'Ha ocurrido un error',
                        'el registro no se guardo correctamente debido a un error interno');
                }
            });
        } else {
            app.showNotificacionErrorConnection();
        }
    },

    ajaxRequestInfo: function (data) {
        if (navigator.onLine) {
            data['last_update_user'] = window.user_id;
            $.ajax({
                url: '/api/votantes/modificar_votante/' + window.votante_id + '/',
                method: 'PUT',
                dataType: "json",
                data: data,
                success: function (data) {
                    app.showNotify('Datos personales del votante actualizados ;)', 'success', app.SHORT_MORE_LENGHT);
                    $request_votante_object_info = {};
                    $update_status_info = false;
                },
                error: function (xhr, status, error) {
                    app.showCommonAlert('error',
                        'Ha ocurrido un error',
                        'el registro no se guardo correctamente debido a un error interno');
                }
            });
        } else {
            app.showNotificacionErrorConnection();
        }
    },

    handleBackEvent: function (e) {
        e.preventDefault();
        if ($update_status_checks || $update_status_info) {
            swal({
                title: '¿Desea salir sin guardar cambios?',
                text: 'Esta a punto de salir sin guardar los cambios de este votante, desea continuar?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dd8d4c',
                cancelButtonColor: '#849964',
                confirmButtonText: 'salir sin guardar',
                cancelButtonText: 'continuar editando',
                closeOnConfirm: false
            }, function () {
                window.location.href = $(e.currentTarget).attr('data-url');
            });
        } else {
            window.location.href = $(e.currentTarget).attr('data-url');
        }
    }

});