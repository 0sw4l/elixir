var app = app || {};



app.mainView = Backbone.View.extend({
    el: '#app',

    events: {
        'change #departamentos': 'fetchMunicipios',
        'click #modal-votantes': 'openModalVotantes',
        'change #municipios': 'fetchCorregimientos'
    },

    initialize: function () {
        this.fetchDepartamentos();
    },

    openModalVotantes: function () {
        $('#modalVotante').modal('show');
    },

    fetchDepartamentos: function () {
        var dropdown = $('#departamentos');
        var url = '/api/geodata/viewsets/departamentos_/';
        $.getJSON(url, function (data) {
            $.each(data.features, function (key, item) {
                $("<option></option>", {
                    value: item.id,
                    text: item.properties.nombre
                }).appendTo(dropdown);
            })
        });
    },


    fetchMunicipios: function (e) {
        app.resetSelect('#municipios', app.DEFAULT_MSG_MUNICIPIO);
        if (!isNaN(e.target.value)) {
            var id = parseInt(e.target.value);
            var dropdown = $('#municipios');
            var url = '/api/geodata/viewsets/departamentos_/' + id + '/municipios/';
            $.getJSON(url, function (data) {
                $.each(data.municipios.features, function (key, item) {
                    $("<option></option>", {
                        value: item.id,
                        text: item.properties.nombre
                    }).appendTo(dropdown);
                });
            });
            app.showDomElement('#select_municipios');
        } else {
            app.hideDomElement('#select_municipios');
            app.hideDomElement('#select_corregimientos');
        }
    },

    fetchCorregimientos: function (e) {
        app.resetSelect('#corregimientos', app.DEFAULT_MSG_CORREGIMIENTO);
        if (!isNaN(e.target.value)) {
            var id = parseInt(e.target.value);
            this.getCorregimientosMunicipio(id);
        } else {
            app.hideDomElement('#select_corregimientos');
        }
    },

    getCorregimientosMunicipio: function (id) {
        var url = '/api/geodata/viewsets/municipios_/' + id + '/';
        var self = this;
        $.getJSON(url, function (item) {
             self.showCorregimientosMunicipio(item.properties.corregimientos, id);
        });
    },

    showCorregimientosMunicipio: function (corregimientos, id) {
        if (corregimientos > 0) {
            var dropdown = $('#corregimientos');
            var url = '/api/geodata/viewsets/municipios_/' + id + '/corregimientos/';
            $.getJSON(url, function (data) {
                $.each(data.corregimientos.features, function (key, item) {
                    $("<option></option>", {
                        value: item.id,
                        text: item.properties.nombre
                    }).appendTo(dropdown);
                });
            });
            app.showDomElement('#select_corregimientos');
        } else {
            app.hideDomElement('#select_corregimientos');
        }
    }
    
});
