var app = app || {};

app.mainView = Backbone.View.extend({
    el: '#app',

    events: {
        'click .show-data': 'hideRelevantTables'
    },

    initialize: function () {
        console.log('im here, im here!');
    },

    hideRelevantTables: function (e) {
        var id = $(e.currentTarget).attr('data-show');
        app.hideDomElement('.table_info');
        app.showDomElement('#'+id);
    }

});

