var app = app || {};

var $departamento = $('#departamentos'),
    $nombre = $('#nombre'),
    $municipio = $('#municipios'),
    $corregimiento = $('#corregimientos'),
    $cedula = $('#cedula'),
    $direccion = $('#direccion'),
    $celular = $('#celular'),
    $email = $('#email'),
    $barrio = $('#barrio'),
    $senado = $('#senado'),
    $camara = $('#camara'),
    $presidencia = $('#presidencia'),
    $cedula_lider = $('#cedula_lider'),
    $consecutivo_padron = $('#consecutivo_padron'),
    $voto_fuerte = $('#voto_fuerte'),
    $contactado = $('#contactado');


app.mainView = Backbone.View.extend({
    el: '#app',

    events: {
        'change #departamentos': 'fetchMunicipios',
        'click #modal-votantes': 'openModalVotantes',
        'change #municipios': 'fetchCorregimientos',
        'click #agregar_votante': 'agregarVotante',
        'click #enviar_datos': 'enviarDatos',
        'click .borrar': 'borrarVotante',
        'click #eliminar_registros': 'clearCollection',
        'keyup input#cedula_lider': 'comprobarLiderExistente'
    },

    initialize: function () {
        this.fetchDepartamentos();
        VotantesCollection.localStorage = new Backbone.LocalStorage('votantes');
        VotantesCollection.fetch();
        this.agregarVotanteTabla(VotantesCollection);
    },

    fetchValues: function () {
        $departamento = $('#departamentos');
        $municipio = $('#municipios');
        $corregimiento = $('#corregimientos');
        $direccion = $('#direccion').val();
        $cedula = $('#cedula').val();
        $nombre = $('#nombre').val();
        $celular = $('#celular').val();
        $email = $('#email').val();
        $barrio = $('#barrio').val();
        $senado = this.getCheckValue('#senado');
        $camara = this.getCheckValue('#camara');
        $presidencia = this.getCheckValue('#presidencia');
        $cedula_lider = $('#cedula_lider').val();
        $consecutivo_padron = $('#consecutivo_padron').val();
        $voto_fuerte = this.getCheckValue('#voto_fuerte');
        $contactado = this.getCheckValue('#contactado');
    },

    getCheckValue: function (selector) {
        return !!$(selector).is(":checked");
    },

    openModalVotantes: function () {
        $('#modalVotante').modal('show');
    },

    fetchDepartamentos: function () {
        var dropdown = $('#departamentos');
        var url = '/api/geodata/viewsets/departamentos_/';
        $.getJSON(url, function (data) {
            $.each(data, function (key, item) {
                $("<option></option>", {
                    value: item.id,
                    text: item.nombre
                }).appendTo(dropdown);
            })
        });
    },


    fetchMunicipios: function (e) {
        app.resetSelect('#municipios', app.DEFAULT_MSG_MUNICIPIO);
        if (!isNaN(e.target.value)) {
            var id = parseInt(e.target.value);
            var dropdown = $('#municipios');
            var url = '/api/geodata/viewsets/departamentos_/' + id + '/municipios_/';
            $.getJSON(url, function (data) {
                $.each(data.municipios, function (key, item) {
                    $("<option></option>", {
                        value: item.id,
                        text: item.nombre
                    }).appendTo(dropdown);
                });
            });
            app.showDomElement('#select_municipios');
        } else {
            app.hideDomElement('#select_municipios');
            app.hideDomElement('#select_corregimientos');
        }
    },

    fetchCorregimientos: function (e) {
        app.resetSelect('#corregimientos', app.DEFAULT_MSG_CORREGIMIENTO);
        if (!isNaN(e.target.value)) {
            var id = parseInt(e.target.value);
            this.getCorregimientosMunicipio(id);
        } else {
            app.hideDomElement('#select_corregimientos');
        }
    },

    getCorregimientosMunicipio: function (id) {
        var url = '/api/geodata/viewsets/municipios_/' + id + '/';
        var self = this;
        $.getJSON(url, function (item) {
            self.showCorregimientosMunicipio(item.corregimientos, id);
        });
    },

    showCorregimientosMunicipio: function (corregimientos, id) {
        if (corregimientos > 0) {
            var dropdown = $('#corregimientos');
            var url = '/api/geodata/viewsets/municipios_/' + id + '/corregimientos/';
            $.getJSON(url, function (data) {
                $.each(data.corregimientos.features, function (key, item) {
                    $("<option></option>", {
                        value: item.id,
                        text: item.properties.nombre
                    }).appendTo(dropdown);
                });
            });
            app.showDomElement('#select_corregimientos');
        } else {
            app.hideDomElement('#select_corregimientos');
        }
    },

    agregarVotante: function () {
        this.fetchValues();

        if (app.int($departamento.val()) > 0) {
            if ($cedula.length > 0){
                this.addVotanteToCollection();
            } else {
                app.showNotify('Debe ingresar la cedula', 'danger');
            }
        } else {
            app.showCommonAlert(
                'error',
                'Error',
                'debe seleccionar un departamento'
            );
        }
    },

    addVotanteToCollection: function () {
        var self = this;
        var tipos = null;
        var pk = null;
        var existente = false;
        var $tipos_select = $('#tipo_votante').val();
        if ($tipos_select) {
            tipos = $tipos_select;
            tipos = tipos.join(", ");
        }

        $.post('/api/votantes/comprobar_votante_existente/', {'cedula': $cedula})
            .done(function (data) {
                if (data.existe) {
                    existente = true;
                    pk = data.pk;
                }
                var votante = {
                    consecutivo_padron: null,
                    cedula_lider: null,
                    departamento: app.int($departamento.val()),
                    municipio: app.int($municipio.val()),
                    corregimiento: app.int($corregimiento.val()),
                    cedula: $cedula,
                    nombre: $nombre,
                    celular: $celular,
                    correo_electronico: $email,
                    direccion: $direccion,
                    barrio_registrado: $barrio,
                    votaria_senado: $senado,
                    votaria_camara: $camara,
                    votaria_presidencia: $presidencia,
                    ubicacion: self.get_location(
                        $departamento.val(),
                        $departamento,
                        $municipio.val(),
                        $municipio,
                        $corregimiento.val(),
                        $corregimiento
                    ),
                    tipo_votante: tipos,
                    voto_fuerte: $voto_fuerte,
                    contactado: $contactado,
                    existente: existente,
                    pk: pk
                };
                if ($consecutivo_padron) {
                    votante['consecutivo_padron'] = app.int($consecutivo_padron);
                }
                if ($cedula_lider) {
                    votante['cedula_lider'] = $cedula_lider;
                }
                VotantesCollection.add([votante]);
                VotantesCollection.each(function (model) {
                    model.save();
                });
                self.resetInputs();
                self.agregarVotanteTabla(VotantesCollection);
            });

    },

    get_location: function (val1, departamento, val2, municipio, val3, corregimiento) {
        var location = 'x';
        if (app.int(val1) > 0) {
            location = departamento.select2('data')[0].text;
            if (app.int(val2) > 0) {
                location += ' / ' + municipio.select2('data')[0].text;
                if (app.int(val3) > 0) {
                    location += ' / ' + corregimiento.select2('data')[0].text;
                }
            }
        }
        return location;
    },

    mostrarCantidadVotantes: function (collection) {
        app.AnimateSelector(
            '#contador_votantes',
            collection.length
        );
        if (collection.length > 0) {
            app.showDomElement('#eliminar_registros');
        } else {
            app.hideDomElement('#eliminar_registros');
        }
    },

    agregarVotanteTabla: function (collection) {
        this.$('#votantes').html('');
        this.mostrarCantidadVotantes(collection);
        collection.forEach(this.agregarVotantesTabla, this);
    },

    agregarVotantesTabla: function (model) {
        var vista = new app.VotantesView({model: model});
        $('#votantes').prepend(vista.render().$el);
    },


    enviarDatos: function () {
        if (navigator.onLine) {
            this.enviarDatosApi();
        } else {
            app.showCommonAlert(
                'error',
                'Error',
                'No hay conexion a internet'
            );
        }
    },

    borrarVotante: function (e) {
        e.preventDefault();
        var id = $(e.currentTarget).attr('id');
        var votante = VotantesCollection.find({'id': id});
        var self = this;
        swal({
                title: 'Estas seguro de eliminar este votante?',
                text: 'si lo eliminas no lo podras recuperar en el futuro.',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d68723',
                cancelButtonColor: '#d33',
                confirmButtonText: 'SI, eliminar votante!',
                closeOnConfirm: false
            },
            function () {
                VotantesCollection.remove(votante);
                app.clearLocalStorageKey('votantes-' + id);
                self.agregarVotanteTabla(VotantesCollection);
                swal(
                    'Votante Eliminado!',
                    'El votante ha sido eliminado exitosamente.',
                    'success'
                );
            });


    },

    enviarDatosApi: function () {

        var votantes_ = JSON.stringify(VotantesCollection.toJSON());
        votantes_ = JSON.parse(votantes_);
        votantes_.forEach(function (votante) {
            delete votante.ubicacion;
            delete votante.id;
            delete votante.existente;
            delete votante.pk;
        });

        var self = this;

        var json_object = {
            'votantes': votantes_
        };

        if (navigator.onLine) {
            $.ajax({
                url: '/api/votantes/crear_votantes/',
                method: 'POST',
                dataType: "json",
                data: json_object,
                success: function (data) {
                    if (data.success) {
                        app.clearLocalStorage();
                        VotantesCollection.reset();
                        self.agregarVotanteTabla(VotantesCollection);
                        app.showNotify(
                            'Datos Enviados',
                            'success'
                        );
                    } else {
                        app.showNotify(
                            'Ha ocurrido un problema al enviar los datos, intente mas tarde',
                            'danger'
                        );
                    }
                },
                error: function (xhr, status, error) {

                }
            });
        }


    },
    resetInputs: function () {
        $('#nombre').val('');
        $('#cedula').val('');
        $('#direccion').val('');
        $('#celular').val('');
        $('#email').val('');
        $('#barrio').val('');
        $('#senado').removeAttr('checked');
        $('#camara').removeAttr('checked');
        $('#presidencia').removeAttr('checked');
        $('#cedula_lider').val('');
        $('#consecutivo_padron').val('');
        $('#voto_fuerte').removeAttr('checked');
        $('#contactado').removeAttr('checked');
    },

    resetSelectInputs: function () {
        $('#departamentos').val('');
        $('#municipios').val('');
        $('#corregimientos').val('');
    },

    clearCollection: function () {

        var self = this;
        swal({
                title: '¿Desea Eliminar todos los votantes?',
                text: 'si eliminas toda la informacion de la tabla podras recuperar en el futuro.',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d68723',
                cancelButtonColor: '#d33',
                confirmButtonText: 'SI, deseo eliminar todos los votantes!',
                closeOnConfirm: false
            },
            function () {
                app.clearLocalStorage();
                VotantesCollection.reset();
                self.agregarVotanteTabla(VotantesCollection);
                app.hideDomElement('#eliminar_registros');
                swal(
                    'Votantes Eliminados!',
                    'La coleccion de votantes ha sido eliminada exitosamente.',
                    'success'
                );
                app.showNotify('Votantes Eliminados', 'success');
            });
    },

    comprobarLiderExistente: function () {
        var cc = $('#cedula_lider').val();
        if (cc.length > 0 ){
             $.post('/api/votantes/comprobar_votante_existente/', {'cedula': cc})
            .done(function (data) {
                if (data.existe) {
                    app.showDomElement('#lider_valido, #agregar_votante');
                    app.hideDomElement('#lider_invalido');
                } else {
                    app.hideDomElement('#lider_valido, #agregar_votante');
                    app.showDomElement('#lider_invalido');
                }
            });
        } else {
            app.hideDomElement('#lider_valido, #lider_invalido');
            app.showDomElement('#agregar_votante')
        }
    }


});


app.VotantesView = Backbone.View.extend({
    tagName: 'tr',
    template: _.template($('#votantes_item').html()),
    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});
