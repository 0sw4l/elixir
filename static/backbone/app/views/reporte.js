var app = app || {};
var $xhr = null;
var $departamento = $('#departamentos'),
    $municipio = $('#municipios'),
    $corregimiento = $('#corregimientos'),
    $cedula = $('#cedula'),
    $edad_minima = $('#edad_minima'),
    $edad_maxima = $('#edad_maxima'),
    $contactado = $('#contactado'),
    $no_llamar = $('#no_volver_a_llamar'),
    $nombre = $('#nombre');

var $pagination = $('#pagination');
var $filter_object = null;
var $base_url = '/api/votantes/filtro_votantes/';
var $next = 0;
var $total_votantes = 0,
    $votantes_camara = 0,
    $votantes_senado = 0,
    $votantes_presidencia = 0,
    $votantes_contactados = 0,
    $votantes_sin_cedula = 0,
    $votantes_no_contactados = 0;


VotanteFilterModel = Backbone.Model.extend({
    url: ''
});

VotantesFiltroCollection = Backbone.Collection.extend({
    model: VotanteFilterModel
});


app.mainView = Backbone.View.extend({
    el: '#app',

    events: {
        'change #departamentos': 'fetchMunicipios',
        'change #municipios': 'fetchCorregimientos',
        'click #filtro': 'agregarFiltro',
        'click #cancelar': 'stopRequest',
        'click #reiniciar': 'resetSearch'
    },

    initialize: function () {
        this.fetchDepartamentos();
    },

    fetchValues: function () {
        $departamento = $('#departamentos');
        $municipio = $('#municipios');
        $corregimiento = $('#corregimientos');
        $cedula = $('#cedula');
        $edad_minima = $('#edad_minima');
        $edad_maxima = $('#edad_maxima');
        $nombre = $('#nombre');
        $contactado = this.getCheckValue($('#contactado'));
        $no_llamar = this.getCheckValue($('#no_volver_a_llamar'));
    },

    getCheckValue: function (selector) {
        return !!$(selector).is(":checked");
    },

    fetchDepartamentos: function () {
        var dropdown = $('#departamentos');
        var url = '/api/geodata/viewsets/departamentos_/';
        $.getJSON(url, function (data) {
            $.each(data, function (key, item) {
                $("<option></option>", {
                    value: item.id,
                    text: item.nombre
                }).appendTo(dropdown);
            });
        }).always(function () {
            app.fadeOut('#main_loader');
            setTimeout(function () {
                app.fadeIn('#formulario');
            }, 300);
        });
    },


    fetchMunicipios: function (e) {
        this.resetContadores();
        app.hideDomElement('#resultados_busqueda, #resultados, #contador_resultados, .extras');
        app.resetSelect('#municipios', app.DEFAULT_MSG_MUNICIPIO);
        if (!isNaN(e.target.value)) {
            var id = parseInt(e.target.value);
            var dropdown = $('#municipios');
            var self = this;
            var url = '/api/geodata/viewsets/departamentos_/' + id + '/municipios_/';
            $.getJSON(url, function (data) {
                self.setContadores(
                    data.votantes,
                    data.votantes_camara,
                    data.votantes_contactados,
                    data.votantes_no_contactados,
                    data.votantes_presidencia,
                    data.votantes_senado,
                    data.votantes_sin_cedula,
                    data.simpatizantes,
                    data.referidos,
                    data.voluntarios,
                    data.nuevos_hoy
                );
                $.each(data.municipios, function (key, item) {
                    $("<option></option>", {
                        value: item.id,
                        text: item.nombre
                    }).appendTo(dropdown);
                });
            }).done(function () {

            });
            app.showDomElement('#select_municipios');
        } else {
            app.hideDomElement('#select_municipios');
            app.hideDomElement('#select_corregimientos');
        }
    },

    fetchCorregimientos: function (e) {
        app.hideDomElement('#resultados_busqueda, #resultados, #contador_resultados, .extras');
        app.resetSelect('#corregimientos', app.DEFAULT_MSG_CORREGIMIENTO);
        if (!isNaN(e.target.value)) {
            var id = parseInt(e.target.value);
            this.getCorregimientosMunicipio(id);
        } else {
            app.hideDomElement('#select_corregimientos');
        }
    },

    getCorregimientosMunicipio: function (id) {
        var url = '/api/geodata/viewsets/municipios/' + id + '/';
        var self = this;
        $.getJSON(url, function (data) {
            self.setContadores(
                data.votantes,
                data.votantes_camara,
                data.votantes_contactados,
                data.votantes_no_contactados,
                data.votantes_presidencia,
                data.votantes_senado,
                data.votantes_sin_cedula,
                data.simpatizantes,
                data.referidos,
                data.voluntarios,
                data.nuevos_hoy
            );
            self.showCorregimientosMunicipio(data.properties.corregimientos, id);
        });
    },

    showCorregimientosMunicipio: function (corregimientos, id) {
        if (corregimientos > 0) {
            var dropdown = $('#corregimientos');
            var self = this;
            var url = '/api/geodata/viewsets/municipios/' + id + '/corregimientos/';
            $.getJSON(url, function (data) {
                self.setContadores(
                    data.votantes,
                    data.votantes_camara,
                    data.votantes_contactados,
                    data.votantes_no_contactados,
                    data.votantes_presidencia,
                    data.votantes_senado,
                    data.votantes_sin_cedula,
                    data.simpatizantes,
                    data.referidos,
                    data.voluntarios,
                    data.nuevos_hoy
                );
                $.each(data.corregimientos.features, function (key, item) {
                    $("<option></option>", {
                        value: item.id,
                        text: item.properties.nombre
                    }).appendTo(dropdown);
                });
            });
            app.showDomElement('#select_corregimientos');
        } else {
            app.hideDomElement('#select_corregimientos');
        }
    },

    agregarFiltro: function () {
        this.fetchValues();
        app.hideDomElement('#resultados_busqueda, #resultados, #contador_resultados, #sin_resultados, #reiniciar, .extras');

        var json_object = {};

        if ($cedula.val()) {
            json_object['cedula'] = $cedula.val();
        }

        if (window.filtro_rapido) {

            if (app.int($departamento.val())) {
                json_object['departamento'] = app.int($departamento.val());
            }

            if (app.int($municipio.val())) {
                json_object['municipio'] = app.int($municipio.val());
            }

            if (app.int($corregimiento.val())) {
                json_object['corregimiento'] = app.int($corregimiento.val());
            }


            if (app.int($edad_minima.val())) {
                json_object['edad_final__gte'] = app.int($edad_minima.val())
            }

            if (app.int($edad_maxima.val())) {
                json_object['edad_final__lte'] = app.int($edad_maxima.val())
            }
        } else {
            if ($nombre.val()) {
                json_object['nombre'] = $nombre.val();
            }
        }

        $filter_object = json_object;
        this.sendRequest($base_url);
    },

    getDataPage: function (page) {
        app.hideDomElement('#resultados_busqueda, #resultados, #contador_resultados, #sin_resultados, #reiniciar, .extras');
        this.sendRequest($base_url + '?page=' + page);
    },

    sendRequest: function (url) {
        var self = this;
        app.showDomElement('#cancelar');

        $xhr = $.ajax({
            url: url,
            dataType: 'json',
            method: 'GET',
            data: $filter_object,
            success: function (data) {
                if (data.count > 0) {
                    var totalPages = data.pages;
                    var currentPage = $pagination.twbsPagination('getCurrentPage');
                    if (data.current_page > 0) {
                        $next = data.current_page;
                    }
                    $pagination.twbsPagination('destroy');
                    $pagination.twbsPagination($.extend({}, {}, {
                        startPage: currentPage,
                        totalPages: totalPages,
                        currentPage: data.current_page,
                        initiateStartPageClick: false,
                        prev: '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
                        next: '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
                        onPageClick: function (evt, page) {
                            self.getDataPage(page);
                        }
                    }));
                    self.fillDataTable(data.count, data.results);
                    app.showDomElement('#resultados_busqueda, #resultados, #contador_resultados, #reiniciar, .extras');
                } else {
                    app.showDomElement('#sin_resultados, #reiniciar');
                }
            },
            error: function (xhr, status, error) {

            },
            beforeSend: function (xhr, settings) {
                app.showDomElement('#loader');
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", app.getCookie('csrftoken'));
                }
            },
            complete: function () {
                app.hideDomElement('#cancelar');
                app.hideDomElement('#loader');
            }
        })
    },

    fillDataTable: function (cantidad, resultados) {
        var votantesCollection = new VotantesFiltroCollection();
        resultados.forEach(function (votante) {
            votantesCollection.add(votante);
        });
        this.mostrarCantidadVotantes(cantidad);
        this.agregarVotanteTabla(votantesCollection);
    },

    agregarVotanteTabla: function (collection) {
        this.$('#votantes').html('');
        collection.forEach(this.agregarVotantesTabla, this);
    },

    agregarVotantesTabla: function (model) {
        var vista = new app.VotantesView({model: model});
        $('#votantes').prepend(vista.render().$el);
    },

    mostrarCantidadVotantes: function (valor) {
        var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
        $('#contador_votantes').animateNumber({
            number: valor,
            numberStep: comma_separator_number_step
        });
    },

    stopRequest: function () {
        $xhr.abort();
        this.hideResultsElements();
    },

    hideResultsElements: function () {
        app.hideDomElement('#loader');
        app.hideDomElement('#resultados_busqueda, #resultados, #contador_resultados, #reiniciar');
    },

    resetSearch: function () {
        $('#cedula, #edad_minima, #edad_minima').val('');
        $('#contactado, #no_volver_a_llamar').attr('checked', false);
        app.hideDomElement('#resultados_busqueda, #resultados, #contador_resultados, #select_municipios , #select_corregimientos, .extras');
    },

    setContadores: function (total, camara, contactados, no_contactados, presidencia, senado, sin_cedula,
                             simpatizantes, referidos, voluntarios, nuevos_hoy) {
        $('#total_votantes_').text(total);
        $('#votaria_camara_').text(camara);
        $('#votaria_senado_').text(senado);
        $('#votaria_presidencia_').text(presidencia);
        $('#votantes_contactados_').text(contactados);
        $('#votantes_sin_cedula_').text(sin_cedula);
        $('#votantes_no_contactados_').text(no_contactados);
        $('#simpatizantes_').text(simpatizantes);
        $('#referidos_').text(referidos);
        $('#voluntarios_').text(voluntarios);
        $('#nuevos_hoy_').text(nuevos_hoy);
    },

    resetContadores: function () {
        $('#total_votantes_').text('');
        $('#votaria_camara_').text('');
        $('#votaria_senado_').text('');
        $('#votaria_presidencia_').text('');
        $('#votantes_contactados_').text('');
        $('#votantes_sin_cedula_').text('');
        $('#votantes_no_contactados_').text('');
        $('#simpatizantes_').text('');
        $('#referidos_').text('');
        $('#voluntarios_').text('');
        $('#nuevos_hoy_').text('');
    }
});


app.VotantesView = Backbone.View.extend({
    tagName: 'tr',
    template: _.template($('#datos').html()),
    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});

