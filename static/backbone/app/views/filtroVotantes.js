var app = app || {};
var $xhr = null;
var $departamento = $('#departamentos'),
    $municipio = $('#municipios'),
    $corregimiento = $('#corregimientos'),
    $cedula = $('#cedula'),
    $edad_minima = $('#edad_minima'),
    $edad_maxima = $('#edad_maxima'),
    $contactado = $('#contactado'),
    $no_llamar = $('#no_volver_a_llamar'),
    $nombre = $('#nombre'),
    $no_contesto = $('#no_contesto'),
    $apagado = $('#apagados'),
    $volver_a_llamar = $('#volver_a_llamar'),
    $voluntario = $('#voluntario'),
    $simpatizante = $('#simpatizante'),
    $voto_fuerte = $('#voto_fuerte'),
    $telefono = $('#telefono');

var $votantes_sin_cedula = false;
var $votantes_sin_nombre = false;
var $pagination = $('#pagination');
var $filter_object = null;
var $totalPages = null;
var $current_page = 1;
var $base_url = '/api/votantes/filtro_votantes/';
var $next = 0;
var $json_object = {};


VotanteFilterModel = Backbone.Model.extend({
    url: ''
});

VotantesFiltroCollection = Backbone.Collection.extend({
    model: VotanteFilterModel
});


app.mainView = Backbone.View.extend({
    el: '#app',

    events: {
        'change #departamentos': 'fetchMunicipios',
        'change #municipios': 'fetchCorregimientos',
        'click #filtro': 'agregarFiltro',
        'click #cancelar': 'stopRequest',
        'click #reiniciar': 'resetSearch',
        'click #votantes_sin_cedula': 'hideRelevantFields',
        'click #votantes_sin_nombre': 'votantesSinNombre',
        'click #reporte': 'reporte',
        'click #buscar_pagina': 'goToPage'
    },

    initialize: function () {
        this.fetchDepartamentos();
        app.webSocketBridge = new channels.WebSocketBridge();
        app.webSocketBridge.connect(app.ws_path);
        this.onBindData();
        this.onListen();
    },

    onBindData: function () {
        var self = this;
        app.webSocketBridge.listen(function (votante) {
            var $votante_row = $('#tr_votante_' + votante.id);
            if (votante.editor) {
                app.hideDomElement('#' + votante.id);
                $votante_row.parent().addClass('busy-call');

            } else {
                $votante_row.parent().removeClass('busy-call');
                $votante_row.parent().addClass('busy-call-free', 1000);
                $('.votante_attr_' + votante.id).removeClass('busy-call').addClass('busy-call-free', 1000);
                setTimeout(function () {
                    $votante_row.parent().removeClass('busy-call-free');
                    $('.votante_attr_' + votante.id).removeClass('busy-call-free').removeClass('hidden');
                    app.showDomElement('#' + votante.id);
                }, 2000);

            }

        });
    },

    onListen: function () {
        app.webSocketBridge.socket.onopen = function () {
            console.log("Connected to notification socket");
        };

        app.webSocketBridge.socket.onclose = function () {
            console.log("Disconnected to notification socket");
        };
    },

    fetchValues: function () {
        $departamento = $('#departamentos');
        $municipio = $('#municipios');
        $corregimiento = $('#corregimientos');
        $cedula = $('#cedula');
        $edad_minima = $('#edad_minima');
        $edad_maxima = $('#edad_maxima');
        $nombre = $('#nombre');
        $contactado = this.getCheckValue($('#contactado'));
        $no_llamar = this.getCheckValue($('#no_volver_a_llamar'));
        $no_contesto = this.getCheckValue($('#no_contesto'));
        $apagado = this.getCheckValue($('#apagados'));
        $volver_a_llamar = this.getCheckValue($('#volver_a_llamar'));
        $voluntario = this.getCheckValue($('#voluntario'));
        $simpatizante = this.getCheckValue($('#simpatizante'));
        $telefono = $('#telefono');
        $voto_fuerte = this.getCheckValue($('#voto_fuerte'));
    },

    getCheckValue: function (selector) {
        return !!$(selector).is(":checked");
    },

    fetchDepartamentos: function () {
        var dropdown = $('#departamentos');
        var url = '/api/geodata/viewsets/departamentos_/';
        $.getJSON(url, function (data) {
            $.each(data, function (key, item) {
                $("<option></option>", {
                    value: item.id,
                    text: item.nombre
                }).appendTo(dropdown);
            })
        }).always(function () {
            app.fadeOut('#main_loader');
            setTimeout(function () {
                app.fadeIn('#formulario');
            }, 300);
        });
    },


    fetchMunicipios: function (e) {
        app.resetSelect('#municipios', app.DEFAULT_MSG_MUNICIPIO);
        if (!isNaN(e.target.value)) {
            var id = parseInt(e.target.value);
            var dropdown = $('#municipios');
            var url = '/api/geodata/viewsets/departamentos_/' + id + '/municipios_/';
            $.getJSON(url, function (data) {
                $.each(data.municipios, function (key, item) {
                    $("<option></option>", {
                        value: item.id,
                        text: item.nombre
                    }).appendTo(dropdown);
                });
            });
            app.showDomElement('#select_municipios');
        } else {
            app.hideDomElement('#select_municipios');
            app.hideDomElement('#select_corregimientos');
        }
    },

    fetchCorregimientos: function (e) {
        app.resetSelect('#corregimientos', app.DEFAULT_MSG_CORREGIMIENTO);
        if (!isNaN(e.target.value)) {
            var id = parseInt(e.target.value);
            this.getCorregimientosMunicipio(id);
        } else {
            app.hideDomElement('#select_corregimientos');
        }
    },

    getCorregimientosMunicipio: function (id) {
        var url = '/api/geodata/viewsets/municipios_/' + id + '/';
        var self = this;
        $.getJSON(url, function (item) {
            self.showCorregimientosMunicipio(item.corregimientos, id);
        });
    },

    showCorregimientosMunicipio: function (corregimientos, id) {
        if (corregimientos > 0) {
            var dropdown = $('#corregimientos');
            var url = '/api/geodata/viewsets/municipios_/' + id + '/corregimientos/';
            $.getJSON(url, function (data) {
                $.each(data.corregimientos.features, function (key, item) {
                    $("<option></option>", {
                        value: item.id,
                        text: item.properties.nombre
                    }).appendTo(dropdown);
                });
            });
            app.showDomElement('#select_corregimientos');
        } else {
            app.hideDomElement('#select_corregimientos');
        }
    },

    agregarFiltro: function () {
        this.fetchValues();
        app.hideDomElement('#resultados_busqueda, #resultados, .resultados, #contador_resultados, #sin_resultados, #reiniciar, .extras');
        app.showDomElement('#pagination');
        $json_object = {};

        if ($cedula.val()) {
            $json_object['cedula'] = $cedula.val();
        }

        if (window.filtro_rapido) {
            $json_object['contactado'] = $contactado;
            $json_object['no_volver_a_llamar'] = $no_llamar;
            $json_object['celular_apagado'] = $apagado;
            $json_object['no_contesto'] = $no_contesto;
            $json_object['volver_a_llamar'] = $volver_a_llamar;
            $json_object['voto_fuerte'] = $voto_fuerte;

            if (app.int($departamento.val())) {
                $json_object['departamento'] = app.int($departamento.val());
            }

            if (app.int($municipio.val())) {
                $json_object['municipio'] = app.int($municipio.val());
            }

            if (app.int($corregimiento.val())) {
                $json_object['corregimiento'] = app.int($corregimiento.val());
            }


            if (app.int($edad_minima.val())) {
                $json_object['edad_final__gte'] = app.int($edad_minima.val())
            }

            if (app.int($edad_maxima.val())) {
                $json_object['edad_final__lte'] = app.int($edad_maxima.val())
            }
        } else {

            if ($nombre.val()) {
                $json_object['nombre'] = $nombre.val();
            }

            if ($telefono.val()) {
                $json_object['telefono'] = $telefono.val();
            }

            $json_object['simpatizante'] = $simpatizante;
            $json_object['voluntario'] = $voluntario;
            if ($votantes_sin_cedula) {
                $json_object = {};
                $json_object['votantes_sin_cedula'] = true;
            }

            if ($votantes_sin_nombre) {
                $json_object = {};
                $json_object['votantes_sin_nombre'] = true;
            }


        }
        $filter_object = $json_object;
        this.sendRequest($base_url);
    },

    goToPage: function () {
        $current_page = parseInt(($('#go_to').val()));
        if ($current_page > 0 && $current_page <= $totalPages) {
            app.hideDomElement('#pagination');
            app.showNotify('Pagina actual ' + $current_page, 'info');
            this.getDataPage($current_page);
        } else {
            $current_page = null;
            app.showNotify('Error, numero de pagina invalido', 'danger');
        }

    },

    getDataPage: function (page) {
        app.hideDomElement('#resultados_busqueda, #resultados, #contador_resultados, #sin_resultados, #reiniciar, .extras');
        this.sendRequest($base_url + '?page=' + page);
    },

    sendRequest: function (url) {
        var self = this;
        app.showDomElement('#cancelar');
        $xhr = $.ajax({
            url: url,
            dataType: 'json',
            method: 'GET',
            data: $filter_object,
            success: function (data) {
                if (data.count > 0) {
                    $totalPages = data.pages;
                    self.mostrarPaginas($totalPages);
                    var currentPage = $pagination.twbsPagination('getCurrentPage');
                    if (data.current_page > 1 && $current_page !== 1) {
                        $current_page = data.current_page;
                    }
                    $pagination.twbsPagination('destroy');
                    $pagination.twbsPagination($.extend({}, {}, {
                        startPage: currentPage,
                        totalPages: $totalPages,
                        currentPage: $current_page,
                        initiateStartPageClick: false,
                        prev: '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
                        next: '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
                        onPageClick: function (evt, page) {
                            self.getDataPage(page);
                        }
                    }));
                    self.fillDataTable(data.count, data.results);
                    app.showDomElement('#resultados_busqueda, .resultados, #resultados, #contador_resultados, #reiniciar, .extras, #reporte');
                } else {
                    app.hideDomElement('reporte');
                    app.showDomElement('#sin_resultados, #reiniciar');
                }
            },
            error: function (xhr, status, error) {

            },
            beforeSend: function (xhr, settings) {
                app.showDomElement('#loader');
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", app.getCookie('csrftoken'));
                }
            },
            complete: function () {
                app.hideDomElement('#cancelar');
                app.hideDomElement('#loader');
            }
        })
    },

    fillDataTable: function (cantidad, resultados) {
        var votantesCollection = new VotantesFiltroCollection();
        resultados.forEach(function (votante) {
            votantesCollection.add(votante);
        });
        this.mostrarCantidadVotantes(cantidad);
        this.agregarVotanteTabla(votantesCollection);
    },

    agregarVotanteTabla: function (collection) {
        this.$('#votantes').html('');
        collection.forEach(this.agregarVotantesTabla, this);
    },

    agregarVotantesTabla: function (model) {
        var vista = new app.VotantesView({model: model});
        $('#votantes').prepend(vista.render().$el);
    },

    mostrarCantidadVotantes: function (valor) {
        var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
        $('#contador_votantes').animateNumber({
            number: valor,
            numberStep: comma_separator_number_step
        });
    },

    mostrarPaginas: function (valor) {
        var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
        $('#paginas_encontradas').animateNumber({
            number: valor,
            numberStep: comma_separator_number_step
        });
    },

    stopRequest: function () {
        $xhr.abort();
        this.hideResultsElements();
    },

    hideResultsElements: function () {
        app.hideDomElement('#loader');
        app.hideDomElement('#resultados_busqueda, #resultados, #contador_resultados, #reiniciar, .resultados');
    },

    resetSearch: function () {
        $json_object = {};
        $votantes_sin_cedula = false;
        $votantes_sin_nombre = false;
        $('#cedula, #edad_minima, #edad_minima').val('');
        $('#contactado, #no_volver_a_llamar, #no_contesto, #apagados, #volver_a_llamar, #simpatizante, #voluntario, #votantes_sin_cedula, #votantes_sin_nombre').attr('checked', false);
        app.hideDomElement('#reporte ,#resultados_busqueda, #resultados, .resultados, #contador_resultados, #select_municipios , #select_corregimientos, .extras');
        app.showDomElement('.relevant, #pagination');
    },

    hideRelevantFields: function () {
        if (this.getCheckValue($('#votantes_sin_cedula'))) {
            $('#votantes_sin_nombre').removeAttr('checked');
            $votantes_sin_cedula = true;
            $votantes_sin_nombre = false;
            app.hideDomElement('.relevant');
        } else {
            $votantes_sin_cedula = false;
            app.showDomElement('.relevant');
        }
    },

    votantesSinNombre: function () {
        if (this.getCheckValue($('#votantes_sin_nombre'))) {
            $('#votantes_sin_cedula').removeAttr('checked');
            $votantes_sin_nombre = true;
            $votantes_sin_cedula = false;
            app.hideDomElement('.relevant');
        } else {
            $votantes_sin_nombre = false;
            app.showDomElement('.relevant');
        }
    },

    reporte: function () {
        $filter_object['reporte'] = true;
        $.ajax({
            url: '/api/reportes/generar_reporte/',
            dataType: 'json',
            method: 'POST',
            data: $filter_object,
            success: function (data) {
                if (data.success === true) {
                    swal(
                        'Envio de datos exitoso',
                        'su reporte se generara en un momento',
                        'success'
                    );
                }
            },
            error: function (xhr, status, error) {
                swal('Error', 'se ha producido un error', 'error');
            }
        })
    }

});


app.VotantesView = Backbone.View.extend({
    tagName: 'tr',
    template: _.template($('#datos').html()),
    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});

