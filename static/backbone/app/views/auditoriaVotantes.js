var app = app || {};
var $xhr = null;
var $sin_email = $('#email'),
    $sin_cedula = $('#cedula'),
    $sin_telefono = $('#telefono'),
    $sin_nombre = $('#nombre'),
    $sin_departamento = $('#departamento'),
    $sin_municipio = $('#municipio');

var $pagination = $('#pagination');
var $filter_object = null;
var $base_url = '/api/votantes/auditoria_votantes/';
var $next = 0;
var $json_object = {};


VotanteFilterModel = Backbone.Model.extend({
    url: ''
});

VotantesFiltroCollection = Backbone.Collection.extend({
    model: VotanteFilterModel
});


app.mainView = Backbone.View.extend({
    el: '#app',

    events: {
        'click #filtro': 'agregarFiltro',
        'click #cancelar': 'stopRequest',
        'click #reiniciar': 'resetSearch',
        'click #reporte': 'reporte',
        'change .switch': 'hideRelevantFields'
    },

    initialize: function () {
        app.webSocketBridge = new channels.WebSocketBridge();
        app.webSocketBridge.connect(app.ws_path);
        this.onBindData();
        this.onListen();
        app.fadeOut('#main_loader');
        setTimeout(function () {
            app.fadeIn('#formulario');
        }, 300);
    },

    onBindData: function () {
        var self = this;
        app.webSocketBridge.listen(function (votante) {
            var $votante_row = $('#tr_votante_' + votante.id);
            if (votante.editor) {
                app.hideDomElement('#' + votante.id);
                $votante_row.parent().addClass('busy-call');

            } else {
                $votante_row.parent().removeClass('busy-call');
                $votante_row.parent().addClass('busy-call-free', 1000);
                $('.votante_attr_' + votante.id).removeClass('busy-call').addClass('busy-call-free', 1000);
                setTimeout(function () {
                    $votante_row.parent().removeClass('busy-call-free');
                    $('.votante_attr_' + votante.id).removeClass('busy-call-free').removeClass('hidden');
                    app.showDomElement('#' + votante.id);
                }, 2000);

            }

        });
    },

    onListen: function () {
        app.webSocketBridge.socket.onopen = function () {
            console.log("Connected to notification socket");
        };

        app.webSocketBridge.socket.onclose = function () {
            console.log("Disconnected to notification socket");
        };
    },

    fetchValues: function () {
        $sin_telefono = this.getCheckValue($('#telefono'));
        $sin_cedula = this.getCheckValue($('#cedula'));
        $sin_email = this.getCheckValue($('#email'));
        $sin_nombre = this.getCheckValue($('#nombre'));
        $sin_departamento = this.getCheckValue($('#departamento'));
        $sin_municipio = this.getCheckValue($('#municipio'));
    },

    getCheckValue: function (selector) {
        return !!$(selector).is(":checked");
    },

    agregarFiltro: function () {
        app.hideDomElement('#resultados_busqueda, #resultados, #contador_resultados, #sin_resultados, #reiniciar, .extras');
        $filter_object = $json_object;
        console.log($json_object);
        this.sendRequest($base_url);
    },

    getDataPage: function (page) {
        app.hideDomElement('#resultados_busqueda, #resultados, #contador_resultados, #sin_resultados, #reiniciar, .extras');
        this.sendRequest($base_url + '?page=' + page);
    },

    sendRequest: function (url) {
        var self = this;
        app.showDomElement('#cancelar');
        $xhr = $.ajax({
            url: url,
            dataType: 'json',
            method: 'GET',
            data: $filter_object,
            success: function (data) {
                if (data.count > 0) {
                    var totalPages = data.pages;
                    var currentPage = $pagination.twbsPagination('getCurrentPage');
                    if (data.current_page > 0) {
                        $next = data.current_page;
                    }
                    $pagination.twbsPagination('destroy');
                    $pagination.twbsPagination($.extend({}, {}, {
                        startPage: currentPage,
                        totalPages: totalPages,
                        currentPage: data.current_page,
                        initiateStartPageClick: false,
                        prev: '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
                        next: '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
                        onPageClick: function (evt, page) {
                            self.getDataPage(page);
                        }
                    }));
                    self.fillDataTable(data.count, data.results);
                    app.showDomElement('#resultados_busqueda, #resultados, #contador_resultados, #reiniciar, .extras, #reporte');
                } else {
                    app.hideDomElement('reporte');
                    app.showDomElement('#sin_resultados, #reiniciar');
                }
            },
            error: function (xhr, status, error) {

            },
            beforeSend: function (xhr, settings) {
                app.showDomElement('#loader');
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", app.getCookie('csrftoken'));
                }
            },
            complete: function () {
                app.hideDomElement('#cancelar');
                app.hideDomElement('#loader');
            }
        })
    },

    fillDataTable: function (cantidad, resultados) {
        var votantesCollection = new VotantesFiltroCollection();
        resultados.forEach(function (votante) {
            votantesCollection.add(votante);
        });
        this.mostrarCantidadVotantes(cantidad);
        this.agregarVotanteTabla(votantesCollection);
    },

    agregarVotanteTabla: function (collection) {
        this.$('#votantes').html('');
        collection.forEach(this.agregarVotantesTabla, this);
    },

    agregarVotantesTabla: function (model) {
        var vista = new app.VotantesView({model: model});
        $('#votantes').prepend(vista.render().$el);
    },

    mostrarCantidadVotantes: function (valor) {
        var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
        $('#contador_votantes').animateNumber({
            number: valor,
            numberStep: comma_separator_number_step
        });
    },

    stopRequest: function () {
        $xhr.abort();
        this.hideResultsElements();
    },

    hideResultsElements: function () {
        app.hideDomElement('#loader');
        app.hideDomElement('#resultados_busqueda, #resultados, #contador_resultados, #reiniciar');
    },

    resetSearch: function () {
        $json_object = {};
        $votantes_sin_cedula = false;
        $votantes_sin_nombre = false;
        $('#cedula, #edad_minima, #edad_minima').val('');
        $('#contactado, #no_volver_a_llamar, #no_contesto, #apagados, #volver_a_llamar, #simpatizante, #voluntario, #votantes_sin_cedula, #votantes_sin_nombre').attr('checked', false);
        app.hideDomElement('#reporte ,#resultados_busqueda, #resultados, #contador_resultados, #select_municipios , #select_corregimientos, .extras');
        app.showDomElement('.relevant');
    },

    hideRelevantFields: function (e) {
        var id = $(e.currentTarget).attr('id');
        var selector = $('#' + id);
        $json_object = {};
        if (this.getCheckValue(selector)) {
            var key = 'sin_'+id;
            $json_object[key] = true;
            $('.switch:not(#' + id + ')').removeAttr('checked');
            $(selector).attr('checked');
        } else {
            $('.switch').removeAttr('checked');
        }
    }

});


app.VotantesView = Backbone.View.extend({
    tagName: 'tr',
    template: _.template($('#datos').html()),
    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});

