var app = app || {};


app.mainView = Backbone.View.extend({
    el: '#app',

    events: {
        'change #rango': 'changeRange'
    },

    getCheckValue: function (selector) {
        return !!$(selector).is(":checked");
    },

    changeRange: function () {
        if (this.getCheckValue($('#rango'))){
            app.showDomElement('.fechas');
            app.hideDomElement('#fecha_unica');
        } else {
            app.showDomElement('#fecha_unica');
            app.hideDomElement('.fechas');
        }
    }

});


