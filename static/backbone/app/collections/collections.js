var app = app || {};

var departamentos = Backbone.Collection.extend({
    model: app.DepartamentoModel,
    url: '/api/geodata/viewsets/departamentos/'
});

app.departamentosCollection = new departamentos();

app.votantesCollection = Backbone.Collection.extend({
    model: app.VotanteModel,
    localStorage: new Backbone.LocalStorage('votantes')
});

var VotantesCollection = new app.votantesCollection();