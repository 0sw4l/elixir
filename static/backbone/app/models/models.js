var app = app || {};

app.DepartamentoModel = Backbone.Model.extend({
    urlRoot: '/api/geodata/viewsets/departamentos/'
});

app.VotanteModel = Backbone.Model.extend({
    defaults: {
        digitador: null,
        departamento: null,
        municipio: null,
        corregimiento: null,
        cedula: null,
        nombre: null,
        celular: null,
        correo_electronico: null,
        direccion: null,
        barrio_registrado: null,
        votaria_senado: false,
        votaria_camara: false,
        votaria_presidencia: false,
        ubicacion: null,
        existente: false,
        pk: null
    }

})
;