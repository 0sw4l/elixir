var app = app || {};
app.base_url = window.location.pathname;
app.DOMAIL_URL = window.location.host;
app.ws_path = "/ws" +  app.base_url + "stream/";
app.DEFAULT_MSG_MUNICIPIO = 'Seleccione un municipio';
app.DEFAULT_MSG_CORREGIMIENTO = 'Seleccione un corregimiento';

app.LONG_LENGTH = 60000;
app.MEDIUM_LENGHT = 30000;
app.SHORT_LENGHT = 15000;
app.SHORT_MORE_LENGHT = 25000;
app.ANIMATION_FADE_IN = 'animated fadeIn';
app.ANIMATION_FADE_OUT = 'animated fadeOut';
app.ALIGN_TOP = 'top';
app.ALIGN_RIGHT = 'right';

app.appendSelectOption = function (selector, text) {
    $("<option disabled selected value></option>", {
        text: text
    }).appendTo(selector);

};

app.int = function (val) {
    return isNaN(parseInt(val)) ? null : parseInt(val);
};

app.comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');

app.AnimateSelector = function (selector, number) {
    $(selector).animateNumber({
        number: number
    });
};

app.clearLocalStorage = function () {
    window.localStorage.clear();
};

app.clearLocalStorageKey = function (key) {
    window.localStorage.removeItem(key);
};

app.disableRightClick = function () {
    $("body").on("contextmenu", function (e) {
        return false;
    });
};

app.showDomElement = function (selector) {
    $(selector).removeClass('hidden');
};


app.hideDomElement = function (selector) {
    $(selector).addClass('hidden');
};


app.fadeIn = function (selector) {
    $(selector).fadeIn("slow", function () {
        setTimeout(function () {
            $(selector).removeClass('custom-hide');
            $(selector).addClass('in');
        }, 100);
    });
};

app.fadeOut = function (selector) {
    $(selector).fadeOut("slow", function () {
        $(selector).addClass('out');
        $(selector).addClass('custom-hide')
    });
};

app.resetSelect = function (selector, text) {
    $(selector)
        .find('option')
        .remove()
        .end()
        .append('<option>' + text + '</option>');
};

app.showCommonAlert = function (type, title, m) {
    swal({
        type: type,
        title: title,
        text: m
    });
};

app.showNotificacionSuccessConnection = function () {
    app.showNotify(
        "Volvio La conexion a internet",
        "success",
        null,
        app.ALIGN_TOP,
        app.ALIGN_RIGHT,
        app.ANIMATION_FADE_IN,
        app.ANIMATION_FADE_OUT,
        app.MEDIUM_LENGHT
    );
};


app.showNotificacionErrorConnection = function () {
    app.showNotify(
        "Se perdio la conexion a internet",
        "danger",
        null,
        app.ALIGN_TOP,
        app.ALIGN_RIGHT,
        app.ANIMATION_FADE_IN,
        app.ANIMATION_FADE_OUT,
        app.SHORT_LENGHT
    );
};

app.showNotify = function (message,
                           type,
                           model,
                           from,
                           align,
                           animation_enter,
                           animation_exit,
                           time) {
    $.growl({
        message: message
    }, {
        type: type,
        allow_dismiss: true,
        label: 'Cancel',
        className: 'btn-xs btn-inverse',
        placement: {
            from: from,
            align: align
        },
        delay: time,
        animate: {
            enter: animation_enter,
            exit: animation_exit
        },
        offset: {
            x: 20,
            y: 85
        }
    });
};


app.WindowNotification = function (theTitle, theBody, theIcon, time, url_) {

    if (!("Notification" in window)) {
        alert("Este navegador no soporta notificaciones de escritorio");
    } else {
        if (Notification.permission === "granted") {
            var options = {
                body: theBody,
                icon: theIcon
            };
            var n = new Notification(theTitle, options);
            if (url_) {
                n.onclick = function (url) {
                    window.open(window.location.origin + url_);
                };
            }
            setTimeout(n.close.bind(n), time);
        }
    }
};

window.onload = function () {
    WireMonkey.init();
    WireMonkey.on('connected', function () {
        app.showNotificacionSuccessConnection();
        app.showDomElement('.action-button');
        $('.fab-left').css("right", "105px");
    });

    WireMonkey.on('disconnected', function () {
        app.showNotificacionErrorConnection();
        app.hideDomElement('.action-button');
        $('.fab-left').removeAttr('style');
    });

};


app.getCookie = function (name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?

            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};

$(function () {
    new app.mainView();


    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", app.getCookie('csrftoken'));
            }
        },
        error: function (xhr, textStatus, error) {
            console.log(error);
        }
    });

});