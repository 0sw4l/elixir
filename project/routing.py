from channels import route
from apps.votantes import consumers

channel_routing = [
    route('websocket.connect',
          consumers.connect_update_votante,
          path=r'^/ws/votantes/perfil_votante/(?P<pk>\d+)/stream/'),

    route('websocket.disconnect',
          consumers.disconnect_update_votante,
          path=r'^/ws/votantes/perfil_votante/(?P<pk>\d+)/stream/'),

    route('websocket.connect',
          consumers.connect_votantes,
          path=r'^/ws/votantes/filtro_rapido/stream/'),

    route('websocket.disconnect',
          consumers.disconnect_votantes,
          path=r'^/ws/votantes/busqueda_facil/stream/'),

    route('websocket.connect',
          consumers.connect_votantes,
          path=r'^/ws/votantes/busqueda_facil/stream/'),

    route('websocket.disconnect',
          consumers.disconnect_votantes,
          path=r'^/ws/votantes/busqueda_facil/stream/'),

    route('websocket.connect',
          consumers.connect_votantes,
          path=r'^/ws/votantes/auditoria_db/stream/'),

    route('websocket.disconnect',
          consumers.disconnect_votantes,
          path=r'^/ws/votantes/auditoria_db/stream/'),


]
