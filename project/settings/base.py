import os
from django.utils.translation import ugettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = 'r8dwmyjle$580asvxxgp4x8mfv&8f0y@*wf7=rk!afbx8$il)p'

# Application definition

DJANGO_CORE_APPS = (
    # disable django-material if you use smart-selects
    # 'material', 'material.admin',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'apps.third_party_apps.gis',
)

THRID_PARTY_APPS = (
    'opbeat.contrib.django',
    'widget_tweaks',
    'smart_selects',
    'rest_framework',
    'rest_framework.authtoken',
    'apps.third_party_apps.rest_framework_gis',
    'rest_auth',
    'rest_auth.registration',
    'easy_select2',
    'debug_toolbar',
    'channels',
    'corsheaders',
    'apps.third_party_apps.fcm',
    'apps.third_party_apps.mapwidgets',
    'django_extensions',
    'djcelery',
    'kombu.transport.django',
    'pure_pagination',
)

# default site id
SITE_ID = 1

PROJECT_CORE_APPS = (
    'apps',
    'apps.app',
    'apps.main',
    'apps.geodata',
    'apps.votantes',
    'apps.reportes',
)

INSTALLED_APPS = DJANGO_CORE_APPS + THRID_PARTY_APPS + PROJECT_CORE_APPS

MIDDLEWARE = (
    # opbeat
    'opbeat.contrib.django.middleware.OpbeatAPMMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    # corsheaders
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    # old SessionAuthenticationMiddleware
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # debug_toolbar
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

MIDDLEWARE_CLASSES = MIDDLEWARE

DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
]

OPBEAT = {
    'ORGANIZATION_ID': '0ae0843da7b74b07a041534c57d33fab',
    'APP_ID': 'cdfac093c1',
    'SECRET_TOKEN': 'aa75436776dac80e6c748cca23f27cd4d0d09158',
}

CONFIG_DEFAULTS = {
    # Toolbar options
    'RESULTS_CACHE_SIZE': 3,
    'SHOW_COLLAPSED': True,
    # Panel options
    'SQL_WARNING_THRESHOLD': 100,  # milliseconds
}

INTERNAL_IPS = (
    # '0.0.0.0',
    # '127.0.0.1',
)

CORS_ORIGIN_ALLOW_ALL = True

ROOT_URLCONF = 'project.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, '../templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],

        },
    },
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
}

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'es-co'

TIME_ZONE = 'America/Bogota'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-f

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

STATIC_ROOT = os.path.abspath(os.path.join(BASE_DIR, '../../public/static'))
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.abspath(
        os.path.join(BASE_DIR, '../static')
    ),
)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, '../../public/media')

SELECT2_USE_BUNDLED_JQUERY = True
LOGIN_URL = '/login'

MAPS_KEY = 'AIzaSyBlWtikV6S7fGuK2WkvHV2KCbF4PwC6euk'
GEOPOSITION_GOOGLE_MAPS_API_KEY = MAPS_KEY
GOOGLE_MAP_API_KEY = MAPS_KEY

MAP_WIDGETS = {
    "GooglePointFieldWidget": (
        ("zoom", 17),
        ("mapCenterLocation", [10.9278992, -74.7829164]),
        ("markerFitZoom", 11),
        ("GooglePlaceAutocompleteOptions", {'componentRestrictions': {'country': 'co'}})
    ),
    "GOOGLE_MAP_API_KEY": GOOGLE_MAP_API_KEY,
}

# Celery config
CELERY_RESULT_BACKEND = 'djcelery.backends.database:DatabaseBackend'
BROKER_URL = 'redis://localhost:6379/0'
#: Only add pickle to this list if your broker is secured
#: from unwanted access (see userguide/security.html)
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'

PAGINATION_SETTINGS = {
    'PAGE_RANGE_DISPLAYED': 2,
    'MARGIN_PAGES_DISPLAYED': 2,
    'SHOW_FIRST_PAGE_WHEN_INVALID': True,
}
