from .base import *

DEBUG = False

redis_host = os.environ.get('REDIS_HOST', 'localhost')

# Channel layer definitions
# http://channels.readthedocs.org/en/latest/deploying.html#setting-up-a-channel-backend
CHANNEL_LAYERS = {
    "default": {
        # This example app uses the Redis channel layer implementation asgi_redis
        "BACKEND": "asgi_redis.RedisChannelLayer",
        "CONFIG": {
            "hosts": [(redis_host, 6379)],
        },
        "ROUTING": "project.routing.channel_routing",
    },
}

INSTALLED_APPS += ('gunicorn',)

ALLOWED_HOSTS = ['*']

AUTH_PASSWORD_VALIDATORS =  [
    {
        'NAME': 'apps.utils.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'apps.utils.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'apps.utils.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'apps.utils.password_validation.NumericPasswordValidator',
    },
    {
        'NAME': 'apps.utils.password_validation.UpperCasePasswordValidator',
    },
]

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

WSGI_APPLICATION = 'project.wsgi.production.application'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'opbeat': {
            'level': 'WARNING',
            'class': 'opbeat.contrib.django.handlers.OpbeatHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'mysite': {
            'level': 'WARNING',
            'handlers': ['opbeat'],
            'propagate': False,
        },
        # Log errors from the Opbeat module to the console (recommended)
        'opbeat.errors': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}

DATABASES = {
    'default': {
        'ENGINE': 'apps.third_party_apps.gis.db.backends.postgis',
        'NAME': 'project',
        'USER': 'project',
        'PASSWORD': 'project',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

CELERY_REDIS_MAX_CONNECTIONS = 1