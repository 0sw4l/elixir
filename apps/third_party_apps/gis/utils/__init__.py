"""
 This module contains useful utilities for GeoDjango.
"""
from apps.third_party_apps.gis.utils.ogrinfo import ogrinfo  # NOQA
from apps.third_party_apps.gis.utils.ogrinspect import mapping, ogrinspect  # NOQA
from apps.third_party_apps.gis.utils.srs import add_srs_entry  # NOQA
from apps.third_party_apps.gis.utils.wkt import precision_wkt  # NOQA
from django.core.exceptions import ImproperlyConfigured

try:
    # LayerMapping requires DJANGO_SETTINGS_MODULE to be set,
    # so this needs to be in try/except.
    from apps.third_party_apps.gis.utils.layermapping import LayerMapping, LayerMapError  # NOQA
except ImproperlyConfigured:
    pass
