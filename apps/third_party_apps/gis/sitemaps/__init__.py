# Geo-enabled Sitemap classes.
from apps.third_party_apps.gis.sitemaps.kml import KMLSitemap, KMZSitemap

__all__ = ['KMLSitemap', 'KMZSitemap']
