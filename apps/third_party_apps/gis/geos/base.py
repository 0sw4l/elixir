from apps.third_party_apps.gis.geos.error import GEOSException
from apps.third_party_apps.gis.ptr import CPointerBase


class GEOSBase(CPointerBase):
    null_ptr_exception_class = GEOSException
