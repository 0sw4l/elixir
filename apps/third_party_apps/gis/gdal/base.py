from apps.third_party_apps.gis.gdal.error import GDALException
from apps.third_party_apps.gis.ptr import CPointerBase


class GDALBase(CPointerBase):
    null_ptr_exception_class = GDALException
