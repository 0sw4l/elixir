"""
 This module houses ctypes interfaces for GDAL objects.  The following GDAL
 objects are supported:

 CoordTransform: Used for coordinate transformations from one spatial
  reference system to another.

 Driver: Wraps an OGR data source driver.

 DataSource: Wrapper for the OGR data source object, supports
  OGR-supported data sources.

 Envelope: A ctypes structure for bounding boxes (GDAL library
  not required).

 OGRGeometry: Object for accessing OGR Geometry functionality.

 OGRGeomType: A class for representing the different OGR Geometry
  types (GDAL library not required).

 SpatialReference: Represents OSR Spatial Reference objects.

 The GDAL library will be imported from the system path using the default
 library name for the current OS. The default library path may be overridden
 by setting `GDAL_LIBRARY_PATH` in your settings with the path to the GDAL C
 library on your system.
"""
from apps.third_party_apps.gis.gdal.datasource import DataSource
from apps.third_party_apps.gis.gdal.driver import Driver
from apps.third_party_apps.gis.gdal.envelope import Envelope
from apps.third_party_apps.gis.gdal.error import (
    GDALException, OGRException, OGRIndexError, SRSException, check_err,
)
from apps.third_party_apps.gis.gdal.geometries import OGRGeometry
from apps.third_party_apps.gis.gdal.geomtype import OGRGeomType
from apps.third_party_apps.gis.gdal.libgdal import (
    GDAL_VERSION, gdal_full_version, gdal_version,
)
from apps.third_party_apps.gis.gdal.raster.source import GDALRaster
from apps.third_party_apps.gis.gdal.srs import CoordTransform, SpatialReference

__all__ = (
    'Driver', 'DataSource', 'CoordTransform', 'Envelope', 'GDALException',
    'GDALRaster', 'GDAL_VERSION', 'OGRException', 'OGRGeometry', 'OGRGeomType',
    'OGRIndexError', 'SpatialReference', 'SRSException',
    'check_err', 'gdal_version', 'gdal_full_version',
)
