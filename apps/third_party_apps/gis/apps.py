from django.apps import AppConfig
from django.core import serializers
from django.utils.translation import ugettext_lazy as _


class GISConfig(AppConfig):
    name = 'apps.third_party_apps.gis'
    verbose_name = _("GIS")

    def ready(self):
        if 'geojson' not in serializers.BUILTIN_SERIALIZERS:
            serializers.BUILTIN_SERIALIZERS['geojson'] = "apps.third_party_apps.gis.serializers.geojson"
