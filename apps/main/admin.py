from django.contrib import admin
from . import models


@admin.register(models.LogVotante)
class LogVotanteAdmin(admin.ModelAdmin):
    list_display = ['id',
                    'usuario',
                    'votante_info',
                    'accion',
                    'creacion',
                    'detalles',
                    'ip',
                    'llamada',
                    'no_volver_a_contactar',
                    'no_contesto',
                    'celular_equivocado',
                    'celular_apagado',
                    ]
    list_filter = [
        'usuario',
        'llamada',
        'no_volver_a_contactar',
        'no_contesto',
        'celular_equivocado',
        'celular_apagado',
    ]

