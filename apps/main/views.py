#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime

from django.db.models import Q
from django.utils.translation import ugettext as _
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, logout, login as auth_login
from django.shortcuts import render, redirect
from apps.geodata.models import Departamento, Municipio, Corregimiento
from apps.utils.security import DigitadorLockAccessTimeMixin
from apps.utils.views import BaseTemplateView
from apps.votantes.models import Votante


def log_in(request):
    context = {'error': False}
    time_error = False
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if hasattr(user, 'digitador'):
            now = datetime.datetime.now()
            hora_entrada = now.replace(hour=7, minute=30, second=0, microsecond=0)
            hora_salida = now.replace(hour=20, minute=0, second=0, microsecond=0)
            if now <= hora_entrada or now >= hora_salida:
                logout(request)
                user = None
                time_error = True
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                return redirect('main:inicio')
            else:
                context = {'msj': _('El usuario ha sido desactivado'), 'error': True}
        else:
            if time_error:
                context = {
                    'msj': _('su ingreso no esta permitido a esta hora'),
                    'error': True,
                    'warning': True
                }
            else:
                context = {'msj': _('usuario o contraseña incorrecta'), 'error': True}

    return render(request, 'login.html', context)


@login_required
def salir(request):
    logout(request)
    return redirect('main:entrar')


class InicioView(BaseTemplateView):
    template_name = 'apps/main/inicio.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['departamentos'] = Departamento.objects.all().count()
        context['municipios'] = Municipio.objects.all().count()
        context['corregimientos'] = Corregimiento.objects.all().count()
        context['votos'] = Votante.objects.filter(
            Q(duplicado=None) | Q(duplicado=False)
        ).exclude(
            Q(cedula__isnull=True) | Q(cedula__exact='')
        ).count()
        return context

