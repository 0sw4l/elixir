from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.InicioView.as_view(), name='inicio'),
    url(r'^login/', views.log_in, name='entrar'),
    url(r'^salir/', views.salir, name='salir'),

]
