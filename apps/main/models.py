from django.db import models
from datetime import datetime

from django.db.models import Q


class LogVotante(models.Model):
    usuario = models.ForeignKey('auth.User')
    votante = models.ForeignKey('votantes.Votante')
    accion = models.CharField(max_length=255)
    creacion = models.DateTimeField(auto_now_add=True)
    detalles = models.TextField(blank=True, null=True)
    ip = models.CharField(max_length=255, blank=True, null=True)
    llamada = models.BooleanField(default=False)
    no_volver_a_contactar = models.BooleanField(default=False)
    no_contesto = models.BooleanField(default=False)
    celular_equivocado = models.BooleanField(default=False)
    celular_apagado = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Cambio Votante'
        verbose_name_plural = 'Cambios Votantes'

    def votante_info(self):
        return '{} , cc: {}, {} - {}'.format(
            self.votante.nombre,
            self.votante.cedula,
            self.votante.municipio,
            self.votante.departamento
        )

    @staticmethod
    def get_date(date):
        return datetime.strptime(date, '%Y-%m-%d')

    @classmethod
    def get_llamadas_from_date(cls, date):
        date = cls.get_date(date)

        return cls.objects.filter(
            Q(no_contesto=True) |
            Q(celular_apagado=True) |
            Q(llamada=True) |
            Q(no_volver_a_contactar=True) |
            Q(celular_equivocado=True),
            creacion__day=date.day,
            creacion__month=date.month,
            creacion__year=date.year
        ).exclude(
            no_contesto=False,
            celular_apagado=False,
            llamada=False,
            no_volver_a_contactar=False,
            celular_equivocado=False
        )

    @classmethod
    def get_llamadas_contestadas_form_date(cls, date):
        date = cls.get_date(date)
        return cls.objects.filter(
            creacion__day=date.day,
            creacion__month=date.month,
            creacion__year=date.year,
            llamada=True
        ).exclude(
            no_contesto=False,
            celular_apagado=False,
            llamada=False,
            no_volver_a_contactar=False,
            celular_equivocado=False
        ).count()

    @classmethod
    def get_llamadas_no_contestadas_form_date(cls, date):
        date = cls.get_date(date)
        return cls.objects.filter(
            creacion__day=date.day,
            creacion__month=date.month,
            creacion__year=date.year,
            no_contesto=True
        ).exclude(
            no_contesto=False,
            celular_apagado=False,
            llamada=False,
            no_volver_a_contactar=False,
            celular_equivocado=False
        ).count()

    @classmethod
    def get_no_volver_a_llamar_form_date(cls, date):
        date = cls.get_date(date)
        return cls.objects.filter(
            creacion__day=date.day,
            creacion__month=date.month,
            creacion__year=date.year,
            no_volver_a_contactar=True
        ).exclude(
            no_contesto=False,
            celular_apagado=False,
            llamada=False,
            no_volver_a_contactar=False,
            celular_equivocado=False
        ).count()

    @classmethod
    def get_celular_equivocado_form_date(cls, date):
        date = cls.get_date(date)
        return cls.objects.filter(
            creacion__day=date.day,
            creacion__month=date.month,
            creacion__year=date.year,
            celular_equivocado=True
        ).exclude(
            no_contesto=False,
            celular_apagado=False,
            llamada=False,
            no_volver_a_contactar=False,
            celular_equivocado=False
        ).count()

    @classmethod
    def get_celular_apagado_form_date(cls, date):
        date = cls.get_date(date)
        return cls.objects.filter(
            creacion__day=date.day,
            creacion__month=date.month,
            creacion__year=date.year,
            celular_apagado=True
        ).exclude(
            no_contesto=False,
            celular_apagado=False,
            llamada=False,
            no_volver_a_contactar=False,
            celular_equivocado=False
        ).count()
