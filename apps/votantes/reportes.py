from . import models


class ReportMain(object):

    def __init__(self):
        self.query = models.Votante.objects.filter(last_update_user__isnull=False)

    @property
    def file_name(self):
        return 'votantes actualizados.xls'
