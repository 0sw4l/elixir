from django.contrib import admin

from apps.votantes.forms import DigitadorForm
from . import models
# Register your models here.


class DisableUpdateAdmin(admin.ModelAdmin):
    pass


@admin.register(models.LiderElectoral)
class LiderElectoralAdmin(DisableUpdateAdmin):
    list_display = ['id',
                    'nombre',
                    'municipio',
                    'barrio',
                    'cedula',
                    'cantidad_votos_negativos',
                    'lista_negra']


@admin.register(models.Votante)
class VotanteAdmin(DisableUpdateAdmin):
    list_display = ['id',
                    'duplicado',
                    'comparado',
                    'creacion',
                    'editor',
                    'nombre',
                    'fuerza_ciudadana',
                    'cedula',
                    'puesto_votacion',
                    'mesa_votacion',
                    'cedula_falsa',
                    'duplicados',
                    'tipo_votante',
                    'votaria_camara',
                    'votaria_senado',
                    'votaria_presidencia',
                    'contactado',
                    'no_volver_a_llamar',
                    'last_update_user',
                    'lider'
                    ]
    search_fields = ['cedula', 'nombre', 'telefono_fijo', 'id']
    list_filter = ['duplicado', 'comparado', 'voto_fuerte']
    list_per_page = 12


@admin.register(models.VotosNegativosLider)
class VotosNegativosLider(DisableUpdateAdmin):
    list_display = ['id', 'lider_electoral', 'cedula', 'registro', 'motivo', 'descripcion']


@admin.register(models.Genero)
class GeneroAdmin(admin.ModelAdmin):
    list_display = ['id', 'nombre']


@admin.register(models.Digitador)
class DigitadorAdmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'get_full_name', 'departamento', 'municipio', 'barrio', 'is_active']
    form = DigitadorForm

    actions = ['activar', 'desactivar']

    def activar(self, request, queryset):
        queryset.update(is_active=True)

    def desactivar(self, request, queryset):
        queryset.update(is_active=False)

    activar.short_description = 'activar usuario/s'
    desactivar.short_description = 'desactivar usuario/s'


@admin.register(models.Supervisor)
class SupervisorAdmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'get_full_name']
