# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-01-17 22:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('votantes', '0010_auto_20180117_0249'),
    ]

    operations = [
        migrations.AddField(
            model_name='digitador',
            name='redes_sociales',
            field=models.BooleanField(default=False),
        ),
    ]
