#!/usr/bin/env python
# -*- coding: utf-8 -*-
from io import BytesIO
from datetime import datetime
import pure_pagination
import xlwt
# Create your views here.
from django.core.files.base import ContentFile
from django.db.models import Q
from django.http import StreamingHttpResponse
from django.urls import reverse_lazy
from django.views.generic import TemplateView

from apps.main.models import LogVotante
from apps.reportes.models import Reporte
from apps.utils.fitsheet import FitSheetWrapper
from apps.utils.security import AdminPermissionMixin
from apps.utils.views import BaseListView, BaseTemplateView, BaseDetailView, BaseCreateView
from apps.votantes.forms import SupervisorForm, DigitadorForm, CoordinadorForm
from apps.votantes.models import Votante, Supervisor, Digitador, Coordinador
from apps.votantes.reportes import ReportMain


class VotanteListView(pure_pagination.PaginationMixin,
                      BaseListView):
    model = Votante
    template_name = 'apps/votante/lista_votante.html'
    paginate_by = 50

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['total_votantes'] = self.model.objects.all().count()
        return context


class DigitacionRapidaView(BaseTemplateView):
    template_name = 'apps/votante/digitacion/formulario_digitacion.html'


class CargueMasivoView(BaseTemplateView):
    template_name = 'apps/votante/cargue_masivo.html'


class EstadisticaView(BaseTemplateView):
    template_name = 'apps/votante/estadistica_campaña.html'


class FiltroRapidoListView(BaseTemplateView):
    template_name = 'apps/votante/filtro_rapido.html'


class CallCenterListView(BaseTemplateView):
    template_name = 'apps/votante/call_center.html'


class PerfilVotanteView(BaseDetailView):
    model = Votante
    template_name = 'apps/votante/perfil_votante.html'


class SupervisorCreateView(AdminPermissionMixin, BaseCreateView):
    form_class = SupervisorForm
    success_url = reverse_lazy('votantes:lista_supervisores')


class SupervisorListView(pure_pagination.PaginationMixin,
                         BaseListView):

    model = Supervisor
    paginate_by = 20
    template_name = 'apps/votante/digitacion/supervisores/lista_supervisores.html'


class DigitadorCreateView(BaseCreateView):
    form_class = DigitadorForm
    success_url = reverse_lazy('votantes:lista_digitadores')


class DigitadorListView(BaseListView):
    model = Digitador
    template_name = 'apps/votante/digitacion/digitadores/lista_digitadores.html'

    def get_queryset(self):
        queryset = self.model.objects.all()
        return queryset


class DigitadorDetailView(BaseDetailView):
    model = Digitador
    template_name = 'apps/votante/digitacion/digitadores/perfil_digitador.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        id = self.get_object().id
        context['creados'] = Votante.objects.filter(
            digitador_id=id
        )
        context['llamadas'] = LogVotante.objects.filter(
            Q(llamada=True) |
            Q(no_volver_a_contactar=True) |
            Q(no_contesto=True) |
            Q(celular_equivocado=True) |
            Q(celular_apagado=True),
            usuario_id=id
        )
        context['modificados'] = Votante.objects.filter(
            last_update_user_id=id
        )
        context['modificaciones'] = LogVotante.objects.filter(
            usuario_id=id
        )
        return context


class GenerarReporteTemplateView(BaseTemplateView):
    template_name = 'apps/reportes/generar_reporte.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['total_votantes'] = Votante.objects.filter(
            Q(duplicado=None) | Q(duplicado=False)
        ).exclude(
            Q(cedula__isnull=True) | Q(cedula__exact='')
        ).count()
        context['senado'] = Votante.objects.filter(
            Q(duplicado=None) | Q(duplicado=False),
            votaria_senado=True
        ).exclude(
            Q(cedula__isnull=True) | Q(cedula__exact='')
        ).count()
        context['camara'] = Votante.objects.filter(
            Q(duplicado=None) | Q(duplicado=False),
            votaria_camara=True
        ).exclude(
            Q(cedula__isnull=True) | Q(cedula__exact='')
        ).count()
        context['presidencia'] = Votante.objects.filter(
            Q(duplicado=None) | Q(duplicado=False),
            votaria_presidencia=True
        ).exclude(
            Q(cedula__isnull=True) | Q(cedula__exact='')
        ).count()
        context['contactados'] = Votante.objects.filter(
            Q(duplicado=None) | Q(duplicado=False),
            contactado=True
        ).exclude(
            Q(cedula__isnull=True) | Q(cedula__exact='')
        ).count()
        context['no_contactados'] = Votante.objects.filter(
            Q(duplicado=None) | Q(duplicado=False),
            contactado=False
        ).exclude(
            Q(cedula__isnull=True) | Q(cedula__exact='')
        ).count()
        context['sin_cedula'] = Votante.get_votantes_sin_cedula().count()
        context['voluntario'] = Votante.objects.filter(
            Q(duplicado=None) | Q(duplicado=False),
            tipo_votante__icontains='VOLUNTARIO'
        ).exclude(
            Q(cedula__isnull=True) | Q(cedula__exact='')
        ).count()
        context['referido'] = Votante.objects.filter(
            Q(duplicado=None) | Q(duplicado=False),
            tipo_votante__icontains='REFERIDO'
        ).count()
        context['simpatizante'] = Votante.objects.filter(
            Q(duplicado=None) | Q(duplicado=False),
            tipo_votante__icontains='SIMPATIZANTE'
        ).exclude(
            Q(cedula__isnull=True) | Q(cedula__exact='')
        ).count()
        context['nuevos_hoy'] = Votante.nuevos_hoy().count()
        return context


class CoordinadorCreateView(AdminPermissionMixin, BaseCreateView):
    form_class = CoordinadorForm
    success_url = reverse_lazy('votantes:lista_coordinadores')


class CoordinadorListView(AdminPermissionMixin, BaseListView):
    model = Coordinador
    template_name = 'apps/votante/digitacion/lista_coordinadores.html'


class TotalVotantesModificadosListView(AdminPermissionMixin,
                                       pure_pagination.PaginationMixin,
                                       BaseListView):
    model = Votante
    template_name = 'apps/votante/lista_votantes_actualizados.html'
    paginate_by = 50

    def get_queryset(self):
        return Votante.objects.filter(last_update_user__isnull=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['votantes_modificados'] = self.get_queryset().count()
        return context


class BusquedaFacilListView(BaseTemplateView):
    template_name = 'apps/votante/busqueda_facil.html'


def generar_reporte_votantes_modificados(request):
    report = ReportMain()
    query = report.query
    archivo = report.file_name
    workbook = xlwt.Workbook(encoding='utf8')
    worksheet = workbook.add_sheet("votantes modificados")
    row_num = 0
    worksheet = FitSheetWrapper(worksheet)
    columns = [
        'cedula',
        'creacion',
        'ultima_actualizacion',
        'departamento',
        'municipio',
        'corregimiento',
        'digitador',
        'last_update_user',
        'barrio_registrado',
        'nombre',
        'ceular',
        'cedula',
        'email',
        'telefono_fijo',
        'direccion',
        'edad_final',
        'tipo_votante',
        'fecha_nacimiento',
        'genero',
        'sector_social',
        'ocupacion',
        'referidos_fuera_residencia',
        'nivel_formacion',
        'lider_electoral',
        'tipo_material_publicitario',
        'tipo_aporte',
        'cantidad_aportada',
        'barrio_registrado',
        'egresado_unimag',
        'volanteo',
        'reuniones',
        'sondeos_opinion',
        'divulgacion_propuestas',
        'distribuir_material',
        'participacion_hechos_politicos',
        'avanzada_territorial',
        'capacitacion_voluntarios',
        'apoyos_redes_sociales',
        'coordinar_logistica_eventos',
        'procesamiento_base_de_datos',
        'donacion_material_publicitario',
        'aportes_en_especie',
        'aportes_economicos',
        'prestamo_vehiculo',
        'movilizar_avanzada',
        'movilizacion_personal',
        'alojamiento_voluntarios',
        'visitas_puerta_puerta',
        'votaria_senado',
        'votaria_camara',
        'votaria_presidencia',
        'confirmado_registraduria',
        'ultima_confirmacion_registraduria',
        'contactado',
        'no_volver_a_llamar',
    ]

    for col_num in range(len(columns)):
        worksheet.write(row_num, col_num, columns[col_num])

    for votante in query:
        row_num += 1
        row = [
            votante.cedula,
            votante.creacion.strftime('%Y-%m-%d %H:%M:%S'),
            votante.ultima_actualizacion.strftime('%Y-%m-%d %H:%M:%S'),
            votante.departamento.__str__(),
            votante.municipio.__str__(),
            votante.corregimiento.__str__(),
            '{} ({})'.format(
                votante.digitador.get_full_name(),
                votante.digitador.username
            ),
            '{} ({})'.format(
                votante.last_update_user.get_full_name(),
                votante.last_update_user.username
            ),
            votante.barrio_registrado,
            votante.nombre,
            votante.ceular,
            votante.cedula,
            votante.email,
            votante.telefono_fijo,
            votante.direccion,
            votante.edad_final,
            votante.tipo_votante,
            votante.fecha_nacimiento,
            votante.genero.__str__(),
            votante.sector_social,
            votante.ocupacion,
            votante.referidos_fuera_residencia,
            votante.nivel_formacion,
            votante.lider_electoral,
            votante.tipo_material_publicitario,
            votante.tipo_aporte,
            votante.cantidad_aportada,
            votante.barrio_registrado,
            votante.egresado_unimag,
            votante.volanteo,
            votante.reuniones,
            votante.sondeos_opinion,
            votante.divulgacion_propuestas,
            votante.distribuir_material,
            votante.participacion_hechos_politicos,
            votante.avanzada_territorial,
            votante.capacitacion_voluntarios,
            votante.apoyos_redes_sociales,
            votante.coordinar_logistica_eventos,
            votante.procesamiento_base_de_datos,
            votante.donacion_material_publicitario,
            votante.aportes_en_especie,
            votante.aportes_economicos,
            votante.prestamo_vehiculo,
            votante.movilizar_avanzada,
            votante.movilizacion_personal,
            votante.alojamiento_voluntarios,
            votante.visitas_puerta_puerta,
            votante.votaria_senado,
            votante.votaria_camara,
            votante.votaria_presidencia,
            votante.confirmado_registraduria,
            votante.ultima_confirmacion_registraduria,
            votante.contactado,
            votante.no_volver_a_llamar,
        ]
        for col_num in range(len(row)):
            worksheet.write(row_num, col_num, row[col_num])

    file = BytesIO()
    workbook.save(file)
    file.seek(0)
    response = StreamingHttpResponse(streaming_content=ContentFile(file.getvalue()),
                                     content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename={}'.format(archivo)
    file.close()
    return response


class AuditoriadbTemplateView(TemplateView):
    template_name = 'apps/votante/auditoria_db.html'