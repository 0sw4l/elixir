import json
from datetime import datetime
from django.contrib.auth.models import User
from django.db import models
from django import urls
from channels import Group
# Create your models here.
from django.db.models import Q
from smart_selects.db_fields import ChainedForeignKey

from apps.utils.print_colors import _green, _cyan, _red
from apps.utils.shortcuts import get_object_or_none, get_list_or_none
from apps.votantes import constants


class BaseLocation(models.Model):
    region = models.ForeignKey('geodata.Region', blank=True, null=True)
    departamento = models.ForeignKey('geodata.Departamento', blank=True, null=True)
    municipio = ChainedForeignKey(
        'geodata.Municipio',
        blank=True,
        null=True,
        chained_field='departamento',
        chained_model_field='departamento'
    )
    corregimiento = ChainedForeignKey(
        'geodata.Corregimiento',
        blank=True,
        null=True,
        chained_field='municipio',
        chained_model_field='municipio'
    )
    localidad = ChainedForeignKey(
        'geodata.Localidad',
        blank=True,
        null=True,
        chained_field=''
    )
    upz = models.ForeignKey('geodata.Upz', blank=True, null=True)
    barrio = models.ForeignKey('geodata.Barrio', blank=True, null=True)

    class Meta:
        abstract = True


class BasePersona(BaseLocation):
    nombre = models.CharField(max_length=255, blank=True, null=True)
    cedula = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    ceular = models.CharField(max_length=255, blank=True, null=True)
    telefono_fijo = models.CharField(max_length=255, blank=True, null=True)
    direccion = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        abstract = True


class BaseNombre(models.Model):
    nombre = models.CharField(max_length=255)

    class Meta:
        abstract = True

    def __str__(self):
        return self.nombre


class LiderElectoral(BasePersona):
    lista_negra = models.BooleanField(default=False, editable=False)

    class Meta:
        verbose_name = 'Lider Electoral'
        verbose_name_plural = 'Lideres Electorales'

    def __str__(self):
        return '{} : {}'.format(self.nombre, self.cedula)

    def get_votante(self):
        return get_object_or_none(Votante, cedula=self.cedula)

    def get_datos(self):
        return '{}'.format(
            self.get_votante().nombre,
            self.get_votante().municipio
        ) if self.get_votante() else 'Votante sin registrar, registrelo'

    @staticmethod
    def calcular_porcentaje(numero, divisor):
        return round(
            (numero * 100) / divisor,
            constants.NUMBERS_AFTER_POINT
        ) if divisor > 0 else 0

    def check_lista_negra(self):
        self.lista_negra = True
        self.save()

    def cantidad_sub_lideres(self):
        return self.sub_lideres().count()

    def votos_negativos(self):
        return VotosNegativosLider.objects.filter(lider_electoral=self)

    def cantidad_votos_negativos(self):
        return self.votos_negativos().count()

    def get_votos_duplicados(self):
        return Votante.objects.filter(
            lider_electoral=self,
            voto_duplicado=True
        )

    def get_votos_fallecidos_duplicados(self):
        return Votante.objects.filter(
            lider_electoral=self,
            voto_duplicado=True,
            persona_fallecida=True
        )

    def get_porcentaje_fallecidos_duplicados(self):
        return self.calcular_porcentaje(
            self.get_votos_fallecidos_duplicados().count(),
            self.get_votos_duplicados().count()
        )

    def get_votos_cedulas_falsas_duplicadas(self):
        return Votante.objects.filter(
            lider_electoral=self,
            voto_duplicado=True,
            cedula_falsa=True
        )

    def get_porcentaje_cedulas_falsas_duplicadas(self):
        return self.calcular_porcentaje(
            self.get_votos_cedulas_falsas_duplicadas().count(),
            self.get_votos_duplicados().count()
        )

    def get_votos_reales_duplicados(self):
        return Votante.objects.filter(
            lider_electoral=self,
            voto_duplicado=True,
            cedula_falsa=False,
            persona_fallecida=False
        )

    def get_porcentaje_votos_reales_duplicados(self):
        return self.calcular_porcentaje(
            self.get_votos_reales_duplicados().count(),
            self.get_votos_duplicados().count()
        )

    def get_personas_fallecidas(self):
        return Votante.objects.filter(
            lider_electoral=self,
            persona_fallecida=True
        )

    def get_cedulas_falsas(self):
        return Votante.objects.filter(
            lider_electoral=self,
            cedula_falsa=True
        )

    def get_absolute_url(self):
        return urls.reverse_lazy('app:perfil_lider', kwargs={
            'pk': self.pk
        })

    def get_total_votantes(self):
        return Votante.objects.filter(lider_electoral=self)

    def get_cantidad_votantes(self):
        return self.get_total_votantes().count()

    def get_total_votos_reales(self):
        return Votante.objects.filter(
            lider_electoral=self,
            persona_fallecida=False,
            cedula_falsa=False,
            voto_duplicado=False
        )

    def get_cantidad_votos_reales(self):
        return self.votos_reales().count()

    def get_porcentaje_votos_negativos(self):
        return self.calcular_porcentaje(
            self.cantidad_votos_negativos(),
            self.get_cantidad_votantes()
        )

    def get_porcentaje_votos_negativos_lider_global(self):
        return self.calcular_porcentaje(
            self.cantidad_votos_negativos(),
            Votante.get_votos_falsos().count()
        )

    def get_porcentaje_votos_positivos(self):
        return self.calcular_porcentaje(
            self.get_cantidad_votos_reales(),
            self.get_cantidad_votantes()
        )

    def get_porcentaje_total_votos(self):
        return self.calcular_porcentaje(
            self.get_cantidad_votantes(),
            LiderElectoral.get_cantidad_votos()
        )

    def get_porcentaje_cedulas_falsas(self):
        return self.calcular_porcentaje(
            self.get_cedulas_falsas().count(),
            self.get_cantidad_votantes()
        )

    def porcentaje_cedulas_falsas_lider_global(self):
        return self.calcular_porcentaje(
            self.get_cedulas_falsas().count(),
            Votante.get_cedulas_falsas().count()
        )

    def get_porcentaje_fallecidos(self):
        return self.calcular_porcentaje(
            self.get_personas_fallecidas().count(),
            self.get_cantidad_votantes()
        )

    def get_porcentaje_fallecidos_lider_global(self):
        return self.calcular_porcentaje(
            self.get_personas_fallecidas().count(),
            Votante.get_personas_fallecidas().count()
        )

    def get_porcentaje_duplicados(self):
        return self.calcular_porcentaje(
            self.get_votos_duplicados().count(),
            self.get_cantidad_votantes()
        )

    def get_porcentaje_votos_duplicados_lider_global(self):
        return self.calcular_porcentaje(
            self.get_votos_duplicados().count(),
            Votante.get_votos_duplicados().count()
        )

    def get_porcentaje_lideres(self):
        return self.calcular_porcentaje(
            self.cantidad_sub_lideres(),
            LiderElectoral.get_lideres().count()
        )

    def get_lideres_votos_negativos(self):
        return LiderElectoral.objects.filter(
            sub_lider_electoral=self,
            lista_negra=constants.OK
        )

    def get_cantidad_lideres_votos_negativos(self):
        return self.get_lideres_votos_negativos().count()

    def get_porcentaje_lideres_votos_negativos(self):
        return self.calcular_porcentaje(
            self.get_cantidad_lideres_votos_negativos(),
            self.sub_lideres().count()
        )

    def get_porcentaje_lideres_lider_votos_negativos(self):
        return self.calcular_porcentaje(
            self.get_cantidad_lideres_votos_negativos(),
            self.get_lista_negra().count()
        )

    def get_porcentaje_lideres_lider_votos_negativos_global(self):
        return self.calcular_porcentaje(
            self.get_cantidad_lideres_votos_negativos(),
            LiderElectoral.objects.all().count()
        )

    def get_lideres_votos_positivos(self):
        return LiderElectoral.objects.filter(
            sub_lider_electoral=self,
            lista_negra=False
        )

    def get_cantidad_lideres_votos_positivos(self):
        return self.get_lideres_votos_negativos().count()

    @staticmethod
    def get_lideres():
        return LiderElectoral.objects.all()

    @staticmethod
    def get_cantidad_votos():
        return Votante.objects.all().count()

    @staticmethod
    def get_lista_negra():
        return LiderElectoral.objects.filter(lista_negra=constants.OK)

    @staticmethod
    def get_lista_blanca():
        return LiderElectoral.objects.filter(lista_negra=constants.NO)

    @staticmethod
    def get_porcentaje_global_votos_reales():
        return LiderElectoral.calcular_porcentaje(
            Votante.get_votos_reales().count(),
            Votante.get_votos().count()
        )

    @staticmethod
    def get_porcentaje_global_votos_negativos():
        return LiderElectoral.calcular_porcentaje(
            Votante.get_votos_falsos().count(),
            Votante.get_votos().count()
        )

    @staticmethod
    def get_porcentaje_global_votos_duplicados():
        return LiderElectoral.calcular_porcentaje(
            Votante.get_votos_duplicados().count(),
            Votante.get_votos().count()
        )

    @staticmethod
    def get_porcentaje_global_fallecidos():
        return LiderElectoral.calcular_porcentaje(
            Votante.get_personas_fallecidas().count(),
            Votante.get_votos().count()
        )

    @classmethod
    def clear_lista_negra(cls):
        cls.objects.filter(lista_negra=constants.OK).update(lista_negra=False)

    def get_votantes(self):
        return Votante.objects.filter(lider_electoral=self)


class TipoCoordinador(BaseNombre):
    class Meta:
        verbose_name = 'Tipo coordinadores'
        verbose_name_plural = 'Tipos de coordinadores'

    def get_coordinadores(self):
        return Coordinador.objects.filter(tipo_coordinador=self)


class Coordinador(User, BasePersona):
    lider_electoral = models.ForeignKey(LiderElectoral)
    tipo_coordinador = models.ForeignKey(TipoCoordinador)

    class Meta:
        verbose_name = 'Coordinador'
        verbose_name_plural = 'Coordinadores'


class Supervisor(User, BaseLocation):
    class Meta:
        verbose_name = 'Supervisor'
        verbose_name_plural = 'Supervisores'

    def get_digitadores(self):
        return Digitador.objects.filter(supervisor_id=self.id)


class Digitador(User, BaseLocation):
    supervisor_id = models.PositiveIntegerField(blank=True, null=True)
    lista_negra = models.BooleanField(default=False, editable=False)
    call_center = models.BooleanField(default=False)
    redes_sociales = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Digitador'
        verbose_name_plural = 'Digitadores'

    def get_kwargs(self):
        return {'pk': self.pk}

    def get_absolute_url(self):
        return urls.reverse_lazy('votantes:perfil_digitador', kwargs=self.get_kwargs())

    def get_supervisor(self):
        return Supervisor.objects.get(id=self.supervisor_id)

    def supervisor(self):
        return self.get_supervisor()

    def check_lista_negra(self):
        self.lista_negra = True
        self.save()


class Genero(BaseNombre):
    class Meta:
        verbose_name = 'Genero'
        verbose_name_plural = 'Generos'

    def get_personas(self):
        return Votante.objects.filter(genero=self)


class Votante(BasePersona):
    digitador = models.ForeignKey(User, blank=True, null=True, related_name='digitadores')
    last_update_user = models.ForeignKey(User, blank=True, null=True, related_name='update_users')
    coordinador = models.ForeignKey(Coordinador, blank=True, null=True, related_name='coordinadores')
    lider = models.ForeignKey(LiderElectoral, blank=True, null=True, related_name='lideres')
    creacion = models.DateField(auto_now_add=True)
    hora_creacion = models.TimeField(auto_now_add=True, blank=True, null=True)
    ultima_actualizacion = models.DateTimeField(auto_now=True)
    puesto_votacion = models.ForeignKey('geodata.PuestoVotacion', blank=True, null=True, editable=False,
                                        related_name='+')
    mesa_votacion = models.ForeignKey('geodata.MesaVotacion', blank=True, null=True, editable=False, related_name='+')
    persona_fallecida = models.BooleanField(
        default=False,
        editable=False
    )
    cedula_falsa = models.BooleanField(
        default=False,
        editable=False
    )
    voto_duplicado = models.BooleanField(
        default=False,
        editable=False
    )
    descripcion_novedad = models.TextField(blank=True, null=True, editable=False)
    registro_puesto = models.CharField(
        max_length=30,
        editable=False,
        blank=True,
        null=True
    )
    edad = models.CharField(max_length=255, blank=True, null=True)
    edad_final = models.PositiveIntegerField(default=0, blank=True, null=True)
    tipo_votante = models.CharField(max_length=255, blank=True, null=True)
    fecha_nacimiento = models.CharField(max_length=100, blank=True, null=True)
    genero = models.ForeignKey(Genero, blank=True, null=True)
    sector_social = models.CharField(max_length=255, blank=True, null=True)
    ocupacion = models.CharField(max_length=255, blank=True, null=True)

    referidos_fuera_residencia = models.BooleanField(default=False)
    nivel_formacion = models.CharField(max_length=255, blank=True, null=True)
    lider_electoral = models.BooleanField(default=False)
    egresado_unimag = models.BooleanField(default=False)
    volanteo = models.BooleanField(default=False)
    reuniones = models.BooleanField(default=False)
    sondeos_opinion = models.BooleanField(default=False)
    divulgacion_propuestas = models.BooleanField(default=False)
    distribuir_material = models.BooleanField(default=False)
    participacion_hechos_politicos = models.BooleanField(default=False)
    avanzada_territorial = models.BooleanField(default=False)
    capacitacion_voluntarios = models.BooleanField(default=False)
    apoyos_redes_sociales = models.BooleanField(default=False)
    coordinar_logistica_eventos = models.BooleanField(default=False)
    procesamiento_base_de_datos = models.BooleanField(default=False)
    donacion_material_publicitario = models.BooleanField(default=False)
    tipo_material_publicitario = models.CharField(max_length=255, blank=True, null=True)
    aportes_en_especie = models.BooleanField(default=False)
    aportes_economicos = models.BooleanField(default=False)
    tipo_aporte = models.CharField(max_length=255, blank=True, null=True)
    cantidad_aportada = models.CharField(max_length=255, blank=True, null=True)
    prestamo_vehiculo = models.BooleanField(default=False)
    movilizar_avanzada = models.BooleanField(default=False)
    movilizacion_personal = models.BooleanField(default=False)
    alojamiento_voluntarios = models.BooleanField(default=False)
    visitas_puerta_puerta = models.BooleanField(default=False)
    barrio_registrado = models.CharField(max_length=255, blank=True, null=True)

    votaria_senado = models.BooleanField(default=False)
    votaria_camara = models.BooleanField(default=False)
    votaria_presidencia = models.BooleanField(default=False)

    confirmado_registraduria = models.BooleanField(default=False, editable=False)
    ultima_confirmacion_registraduria = models.BooleanField(default=False, editable=False)

    contactado = models.BooleanField(default=False)
    no_volver_a_llamar = models.BooleanField(default=False)
    no_contesto = models.BooleanField(default=False)
    celular_apagado = models.BooleanField(default=False)
    celular_equivocado = models.BooleanField(default=False)
    volver_a_llamar = models.BooleanField(default=False)

    consecutivo_padron = models.CharField(max_length=100, blank=True, null=True)

    fuerza_ciudadana = models.BooleanField(default=False)
    voto_fuerte = models.BooleanField(default=False)

    editor = models.ForeignKey('auth.User', blank=True, null=True, related_name='editor')

    comparado = models.NullBooleanField(blank=True, null=True, default=None)
    duplicado = models.NullBooleanField(blank=True, null=True, default=None)

    class Meta:
        verbose_name = 'Votante'
        verbose_name_plural = 'Votantes'
        ordering = ['-id']

    def __str__(self):
        return 'id : {},  cc:  {}'.format(
            self.id,
            self.cedula
        )

    def save(self, *args, **kwargs):
        result = super().save(*args, **kwargs)
        self.send_notification()
        return result

    def send_notification(self):
        notification = {
            "id": self.id,
            "editor": True if self.editor else False
        }
        Group("votante_{}".format(self.id)).send({
            "text": json.dumps(notification)
        })
        Group("votantes").send({
            "text": json.dumps(notification)
        })

    def get_kwargs(self):
        return {
            'pk': self.pk
        }

    def get_absolute_url(self):
        return urls.reverse_lazy('votantes:perfil_votante', kwargs=self.get_kwargs())

    @classmethod
    def nuevos_hoy(cls):
        date = datetime.now().date()
        return cls.objects.filter(
            Q(duplicado=None) | Q(duplicado=False),
            creacion__day=date.day,
            creacion__month=date.month,
            creacion__year=date.year
        ).exclude(
            Q(cedula__isnull=True) | Q(cedula__exact='')
        )

    @classmethod
    def get_simpatizantes(cls):
        return cls.objects.filter(
            Q(duplicado=None) | Q(duplicado=False),
            tipo_votante__icontains='SIMPATIZANTE'
        ).exclude(
            Q(cedula__isnull=True) | Q(cedula__exact='')
        )

    @classmethod
    def get_referidos(cls):
        return cls.objects.filter(
            Q(duplicado=None) | Q(duplicado=False),
            tipo_votante__icontains='REFERIDO').exclude(
            Q(cedula__isnull=True) | Q(cedula__exact='')
        )

    @classmethod
    def get_voluntario(cls):
        return cls.objects.filter(
            Q(duplicado=None) | Q(duplicado=False),
            tipo_votante__icontains='VOLUNTARIO'
        ).exclude(
            Q(cedula__isnull=True) | Q(cedula__exact='')
        )

    def set_confirmacion_registraduria(self):
        self.confirmado_registraduria = True
        self.save()

    def set_ultima_confirmacion_registraduria(self):
        self.ultima_confirmacion_registraduria = True
        self.save()

    def set_mesa(self, mesa):
        self.mesa_votacion = mesa
        self.save()

    def set_puesto(self, puesto):
        self.puesto_votacion = puesto
        self.save()

    def set_voto_duplicado(self, novedad):
        self.voto_duplicado = True
        self.save()
        if self.voto_duplicado and self.persona_fallecida:
            self.descripcion_novedad = '{} - {} - {}'.format(
                constants.VOTANTE_EXISTENTE,
                constants.MUERTE,
                novedad
            )
        elif self.cedula_falsa and self.voto_duplicado:
            self.descripcion_novedad = '{} - {}'.format(
                constants.CEDULA_FALSA,
                constants.VOTANTE_EXISTENTE
            )
        self.crear_voto_negativo()

    def get_nombre_lider(self):
        lider = Votante.objects.filter(cedula=self.lider.cedula, duplicado=None)
        if lider.count() > 0:
            lider = lider[0]
            if lider.nombre or lider.cedula:
                return '{} : {}'.format(
                    lider.nombre if lider.nombre else 'sin nombre',
                    lider.cedula if lider.cedula else 'sin cedula'
                )
            else:
                return None
        return 'Lider no registrado, cc : {}'.format(self.lider.cedula)

    def get_lider_object(self):
        lista_lider = Votante.objects.filter(cedula__exact=self.lider.cedula, duplicado=None)
        return lista_lider[0] if lista_lider.count() > 0 else None

    def get_votantes_traidos(self):
        return Votante.objects.filter(lider__cedula=self.cedula)

    def registros_iguales(self):
        return Votante.objects.filter(
            cedula=self.cedula,
            voto_duplicado=True
        )

    def duplicados(self):
        return self.registros_iguales().count() if self.registros_iguales().count() > 0 else None

    def set_cedula_falsa(self):
        self.cedula_falsa = True
        self.save()
        self.crear_voto_negativo()

    def set_persona_fallecida(self):
        self.persona_fallecida = True
        self.save()
        self.crear_voto_negativo()

    def crear_voto_negativo(self):
        VotosNegativosLider.objects.create(
            digitador=self.digitador,
            votante=self
        )

    def crear_reporte(self, reporte):
        self.descripcion_novedad = reporte
        self.save()

    def set_municipio(self, municipio):
        self.municipio = municipio
        self.save()

    def set_corregimiento(self, corregimiento):
        self.corregimiento = corregimiento
        self.save()

    @staticmethod
    def get_votos():
        return Votante.objects.all()

    @staticmethod
    def get_lideres_lista_negra():
        return LiderElectoral.objects.filter(lista_negra=constants.OK)

    @classmethod
    def get_votos_falsos(cls):
        return sum([
            cls.get_votos_duplicados().count(),
            cls.get_personas_fallecidas().count(),
            cls.get_cedulas_falsas().count()
        ])

    @classmethod
    def get_votos_reales(cls):
        return cls.get_query_kwargs(**{
            'persona_fallecida': False,
            'voto_duplicado': False,
            'cedula_falsa': False
        })

    @classmethod
    def get_votos_duplicados(cls):
        return cls.get_query_kwargs(**{
            'voto_duplicado': True
        })

    @classmethod
    def get_personas_fallecidas(cls):
        return cls.get_query_kwargs(**{
            'persona_fallecida': True
        })

    @classmethod
    def get_reales_duplicados(cls):
        return cls.get_query_kwargs(**{
            'cedula_falsa': False,
            'voto_duplicado': True
        })

    @classmethod
    def get_cedulas_falsas(cls):
        return cls.get_query_kwargs(**{
            'cedula_falsa': True
        })

    @classmethod
    def get_fallecidos_duplicados(cls):
        return cls.get_query_kwargs(**{
            'persona_fallecida': True,
            'voto_duplicado': True,
            'cedula_falsa': False
        })

    @classmethod
    def get_cedulas_falsas_duplicadas(cls):
        return cls.get_query_kwargs(**{
            'persona_fallecida': False,
            'voto_duplicado': True,
            'cedula_falsa': True
        })

    @staticmethod
    def get_query_kwargs(**kwargs):
        return Votante.objects.filter(**kwargs)

    @classmethod
    def get_votantes_con_cedula(cls):
        return cls.objects.filter(cedula__isnull=False).exclude(cedula__exact='')

    @classmethod
    def get_votantes_sin_cedula(cls):
        return cls.objects.filter(Q(cedula__isnull=True) | Q(cedula__exact='')).exclude(
           duplicado=True
        )

    @staticmethod
    def get_date(date):
        return datetime.strptime(date, '%Y-%m-%d')

    @classmethod
    def get_votantes_nuevos_fecha1(cls, fecha_1):
        date = cls.get_date(fecha_1)
        return cls.objects.filter(
            creacion__gte=date
        )

    @classmethod
    def get_votantes_nuevos_fecha2(cls, fecha_2):
        date = cls.get_date(fecha_2)
        return cls.objects.filter(
            creacion__lte=date
        )

    @classmethod
    def get_votantes_nuevos_fecha1_fecha2(cls, fecha_1, fecha_2):
        fecha1 = cls.get_date(fecha_1)
        fecha2 = cls.get_date(fecha_2)
        return cls.objects.filter(
            creacion__range=[fecha1, fecha2]
        )

    @classmethod
    def get_votantes_nuevos_fecha_especifica(cls, fecha):
        date = cls.get_date(fecha)
        return cls.objects.filter(
            creacion__exact=date
        )

    def get_current_edad(self):
        try:
            edad = int(self.edad)
        except:
            edad = self.edad_final if self.edad_final > 0 else self.edad
        return edad

    def mark_as_confirm(self):
        self.comparado = True
        if self.lider:
            if self.lider.cedula == '':
                self.lider = None
                self.save(update_fields=['lider'])
        self.save(update_fields=['comparado'])

    def mark_copies(self):
        copias = Votante.objects.filter(cedula=self.cedula).only('id',
                                                                 'cedula',
                                                                 'comparado',
                                                                 'duplicado').exclude(id__exact=self.id)
        print('{} : {}'.format(_green('copias encontradas dentro del rango'), _cyan(copias.count())))
        lista_ids = [copia_.id for copia_ in copias]
        for copia in copias:
            copia.comparado = True
            copia.duplicado = True
            copia.save(update_fields=['comparado', 'duplicado'])
        print('ids procesadas : {} , menos : {}'.format(
            _green(lista_ids),
            _red(self.id)
        ))

    def set_count_attrs(self, count):
        self.attrs_with_value = count
        self.save()

    def get_location(self):
        location = ''
        if not self.departamento and self.municipio:
            location += '{} / {}'.format(
                self.municipio.departamento,
                self.municipio
            )
        elif self.departamento and self.municipio:
            location += '{} / {}'.format(
                self.departamento,
                self.municipio
            )
        elif self.departamento:
            location += '{}'.format(self.departamento)
        return location


class VotosNegativosLider(models.Model):
    lider_electoral = models.ForeignKey(LiderElectoral, related_name='+', blank=True, null=True)
    digitador = models.ForeignKey(User, blank=True, null=True)
    votante = models.ForeignKey(Votante, related_name='+')
    registro = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Votos Negativos Lider'
        verbose_name_plural = 'Votos Negativos Lider'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    def motivo(self):
        motivo = ''
        if self.votante.cedula_falsa:
            motivo = 'Cedula Falsa'
        elif self.votante.voto_duplicado:
            motivo = 'Voto Duplicado'
        elif self.votante.persona_fallecida:
            motivo = 'Persona Fallecida'
        return motivo

    def descripcion(self):
        return self.votante.descripcion_novedad

    def cedula(self):
        return self.votante.cedula
