import time
from django.db.models import Q
from apps.utils.print_colors import _cyan, _red, _orange, _green
from apps.utils.shortcuts import get_object_or_none
from apps.votantes.models import Votante
from project.celery import app


def clean_db():
    for v in Votante.objects.filter(comparado=None).only('id', 'cedula').exclude(
            Q(cedula__isnull=True) | Q(cedula__exact='')
    ):
        if v.cedula != '' or v.cedula is not None and not v.comparado:
            filter_cc = Votante.objects.filter(cedula__exact=v.cedula)
            for v2 in filter_cc:
                count = 0
                nulos = 0
                print('objeto a inspeccionar : id : {} , cedula : {}    '.format(
                    _cyan(v.id),
                    _green(v.cedula)
                ))
                for key, value in list(v2.__dict__.items()):
                    if value is not None and not value == '':
                        if not key.__contains__('_state') and not key.__contains__('attrs_with_value') \
                                and not key.__contains__('cedula'):
                            count += 1
                    else:
                        nulos += 1
                print('{} : ({}) , {} : ({})'.format(
                    _green('Atributos con valor contados'),
                    _orange(count),
                    _cyan('Atributos nulos contados'),
                    _red(nulos)
                ))
                if v2.attrs_with_value == 0:
                    v2.attrs_with_value = count
                    v2.save(update_fields=['attrs_with_value'])

            object_with_max_attrs = filter_cc.order_by('-attrs_with_value', '-creacion').first()
            if filter_cc.count() > 1:
                print(_orange('marcando copias'))
                object_with_max_attrs.mark_copies()
                print(_green('copias marcadas'))

            object_with_max_attrs.mark_as_confirm()

            print(_green('el votante {} - con cedula {} ha sido confirmado'.format(
                _green(object_with_max_attrs.id),
                _orange(object_with_max_attrs.cedula)
            )))

