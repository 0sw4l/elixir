from bs4 import BeautifulSoup
import requests
import re

from celery.task import task
from django.db.models.signals import post_save, post_migrate, post_delete, post_init
from django.db.models.signals import pre_save, pre_migrate, pre_delete, pre_init
from django.db.models.signals import m2m_changed
from django.dispatch import receiver

from apps.geodata.models import PuestoVotacion, MesaVotacion, Municipio, Corregimiento, Departamento
from apps.utils.print_colors import _red
from apps.utils.shortcuts import get_object_or_none, get_list_or_none, remove_accent, get_corregimiento, get_municipio
from . import models, constants as utils


def run_collect_data(instance):
    collect_data.delay(instance)


@task(ignore_result=True)
def collect_data(instance):
    registros = models.Votante.objects.filter(
        cedula=instance.cedula
    )
    if len(registros) > 1:
        registro_alfa = registros.order_by('id')[0]
        instance.set_voto_duplicado(registro_alfa.descripcion_novedad)
        instance.persona_fallecida = registro_alfa.persona_fallecida
        instance.puesto_votacion = registro_alfa.puesto_votacion
        instance.mesa_votacion = registro_alfa.mesa_votacion
        instance.cedula_falsa = registro_alfa.cedula_falsa
        instance.registro_puesto = registro_alfa.registro_puesto
        # instance.municipio = registro_alfa.municipio
        instance.save()
    else:
        req = requests.get(utils.get_url(str(instance.cedula)))
        status_code = req.status_code
        if status_code == 200:
            html = BeautifulSoup(req.text, "html.parser")
            tds = html.find_all('tr')
            lista = []
            for i, tr in enumerate(tds):
                for element, td in enumerate(tr):
                    if element == 3:
                        lista.append(td.getText())
            else:
                if lista:
                    departamento = lista[0]
                    municipio = lista[1]
                    puesto_votacion = lista[2]
                    direccion_puesto = lista[3]
                    registro_puesto = lista[4]
                    mesa = lista[5]
                    registrar_votante(
                        departamento,
                        municipio,
                        puesto_votacion,
                        direccion_puesto,
                        registro_puesto,
                        mesa,
                        instance
                    )
                else:
                    element = html.find_all('strong')
                    if len(element) > 1:
                        if element[1].getText() == utils.MUERTE:
                            li = element[1].next_sibling
                            reporte = '{}'.format(li.replace('. ', ''))
                            instance.set_persona_fallecida()
                            instance.crear_reporte('{} : {}'.format(utils.MUERTE, reporte))
                        else:
                            if element[0].getText() == utils.DOCUMENTO_INCORRECTO:
                                crear_reporte(instance, utils.DOCUMENTO_INCORRECTO)
                            elif element[0].getText() == utils.CEDULA_FALSA:
                                crear_reporte(instance, utils.CEDULA_FALSA)


def _puesto(**kwargs):
    return get_object_or_none(PuestoVotacion, **kwargs)


def _mesa(numero, puesto):
    return get_object_or_none(MesaVotacion, nombre=numero, puesto_votacion=puesto)


def crear_puesto(**data):
    return PuestoVotacion.objects.create(**data)


def crear_mesa_puesto(numero, puesto):
    return MesaVotacion.objects.create(
        nombre=numero,
        puesto_votacion=puesto
    )


def _municipio(**kwargs):
    return get_object_or_none(Municipio, **kwargs)


def _corregimiento(**kwargs):
    return get_object_or_none(Corregimiento, **kwargs)


def crear_municipio(nombre):
    return Municipio.objects.create(
        nombre=nombre
    )


def crear_reporte(instancia, reporte):
    instancia.set_cedula_falsa()
    instancia.crear_reporte(reporte)


def registrar_votante(departamento, municipio, puesto_votacion, direccion_puesto, registro_puesto, numero_mesa,
                      instancia):

    print('cc {2} : departamento {0} / municipio {1}'.format(
        instancia.departamento,
        instancia.municipio,
        instancia.cedula
    ))

    departamento_object = Departamento.search(departamento)
    print('departamento : '.format(departamento))

    try:
        municipio_search = get_municipio(remove_accent(municipio.upper()))
        corregimiento_search = get_corregimiento(remove_accent(municipio.upper))

        municipio_object = Municipio.search(
            departamento=departamento,
            name=municipio_search
        )

        corregimiento_object = Corregimiento.search(**{
            'nombre': corregimiento_search,
            'municipio': municipio_object
        })
        print('corregimiento : {}'.format(corregimiento_object))

        puesto_votacion = {
            'nombre': puesto_votacion,
            'direccion': direccion_puesto,
            'municipio': municipio_object,
            'corregimiento': corregimiento_search
        }

        if not instancia.corregimiento:
            instancia.set_corregimiento(corregimiento_object)
            completar_informacion_votante(municipio_object,
                                          puesto_votacion,
                                          numero_mesa,
                                          instancia)
    except Exception as error:
        print('Error : {}'.format(_red(error)))
        if instancia.municipio:
            print('tiene municipio')
            municipio_object = instancia.municipio
            print(municipio_object)
        else:
            print('no tiene municipio')
            municipio_object = Municipio.search(
                departamento=departamento,
                name=municipio
            )
            print(municipio)
            if not municipio_object and instancia.departamento:
                municipio_object = _municipio(**{
                    'nombre': remove_accent(municipio),
                    'departamento': instancia.departamento
                })

            print(municipio_object)
            instancia.set_municipio(municipio_object)
        puesto_votacion = {
            'nombre': puesto_votacion,
            'direccion': direccion_puesto,
            'municipio': municipio_object if municipio_object else instancia.municipio,
            'departamento': departamento_object
        }
        completar_informacion_votante(municipio_object,
                                      puesto_votacion,
                                      numero_mesa,
                                      instancia)


def completar_informacion_votante(municipio_object,
                                  puesto_votacion,
                                  numero_mesa,
                                  instancia):

    puesto_object = _puesto(**puesto_votacion)
    mesa_object = _mesa(numero_mesa, puesto_object)

    if puesto_object:
        print('if puesto')
        print(municipio_object)
        instancia.set_puesto(puesto=puesto_object)
    else:
        print('crear puesto')
        print(municipio_object)
        puesto_object = crear_puesto(**puesto_votacion)
        instancia.set_puesto(puesto_object)
    if mesa_object:
        instancia.set_mesa(mesa=mesa_object)
    else:
        instancia.set_mesa(
            crear_mesa_puesto(
                numero=numero_mesa,
                puesto=puesto_object
            )
        )
    instancia.set_confirmacion_registraduria()

