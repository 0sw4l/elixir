from django.apps import AppConfig


class VotantesConfig(AppConfig):
    name = 'apps.votantes'
    verbose_name = 'Votantes'

    def ready(self):
        from . import signals
