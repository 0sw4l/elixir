def get_url(cc):
    return "https://wsp.registraduria.gov.co/censo/_censoResultado.php?nCedula={0}&nCedulaH={0}x=0&y=0".format(cc)


MUERTE = 'Cancelada por Muerte'
CEDULA_FALSA = 'No se encuentra en el censo para las próximas elecciones.'
DOCUMENTO_INCORRECTO = 'Número de documento Incorrecto'
VOTANTE_EXISTENTE = 'Este votantes ya ha sido registrado'


def get_good_votes(model_attr, object_model):
    return {
        model_attr: object_model,
        'voto_duplicado': False,
        'cedula_falsa': False,
        'persona_fallecida': False
    }


NUMBERS_AFTER_POINT = 2
OK = True
NO = False
