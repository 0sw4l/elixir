from django import forms
from . import models
from apps.utils.forms import BaseUserCreationForm


class DigitadorForm(BaseUserCreationForm):
    form_title = 'Digitador'

    class Meta(BaseUserCreationForm.Meta):
        model = models.Digitador
        exclude = BaseUserCreationForm.Meta.exclude + (
            'departamento',
            'municipio',
            'corregimiento',
            'localidad',
            'upz',
            'barrio',
            'region'
        )


class CoordinadorForm(BaseUserCreationForm):
    form_title = 'Coordinador'

    class Meta(BaseUserCreationForm.Meta):
        model = models.Coordinador
        exclude = BaseUserCreationForm.Meta.exclude + (
            'departamento',
            'municipio',
        )


class SupervisorForm(BaseUserCreationForm):
    form_title = 'Supervisor'

    class Meta(BaseUserCreationForm.Meta):
        model = models.Supervisor
        exclude = BaseUserCreationForm.Meta.exclude + (
            'departamento',
            'municipio',
            'corregimiento',
            'localidad',
            'upz',
            'barrio',
            'region'
        )
