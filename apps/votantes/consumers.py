import re
import logging
from channels import Group
from channels.sessions import channel_session

from apps.utils.shortcuts import get_object_or_none
from apps.votantes.models import Votante
from . import models
from channels.auth import http_session_user, channel_session_user, channel_session_user_from_http
log = logging.getLogger(__name__)
from django.utils.decorators import method_decorator

from channels.generic.websockets import JsonWebsocketConsumer


@channel_session_user_from_http
def connect_update_votante(message, pk):
    votante = Votante.objects.get(id=pk)
    if not votante.editor:
        votante.editor = message.user
        votante.save()
    message.reply_channel.send({"accept": True})
    Group("votante_{}".format(votante.id)).add(message.reply_channel)


@channel_session_user_from_http
def disconnect_update_votante(message, pk):
    votante = Votante.objects.get(id=pk)
    votante.editor = None
    votante.save()
    Group("votante_{}".format(pk)).discard(message.reply_channel)


def connect_votantes(message):
    message.reply_channel.send({'accept': True})
    Group("votantes").add(message.reply_channel)


def disconnect_votantes(message):
    Group("votantes").discard(message.reply_channel)
