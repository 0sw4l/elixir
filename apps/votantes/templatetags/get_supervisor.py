from django import template as dj_template
from django.contrib.auth.models import User
from apps.utils.shortcuts import get_object_or_none
from apps.votantes.models import Supervisor

register = dj_template.Library()


@register.filter(name='get_supervisor')
def get_supervisor(id):
    supervisor = get_object_or_none(User, id=id)
    if supervisor:
        return '{} ({})'.format(
            supervisor.get_full_name(),
            supervisor.username
        )
    else:
        return None

