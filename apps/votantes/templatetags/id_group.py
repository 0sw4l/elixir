from django import template as dj_template

from apps.utils.shortcuts import get_object_or_none
from apps.votantes.models import Votante

register = dj_template.Library()


@register.filter(name='get_id')
def get_id(label):
    label = label.replace(' ', '_')
    return label.lower()


@register.filter(name='get_lider')
def get_lider(lider_):
    lider = get_object_or_none(Votante, cedula=lider_.cedula)
    if lider:
        return '{} {}'.format(
            lider.nombre,
            lider.cedula
        )
    return 'el lider {} no existe'.format(
        lider_.cedula
    )
