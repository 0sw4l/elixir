from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^votantes/',
        views.VotanteListView.as_view(),
        name='votantes'),

    url(r'^digitacion_rapida/',
        views.DigitacionRapidaView.as_view(),
        name='digitacion_rapida'),

    url(r'^cargue_masivo/',
        views.CargueMasivoView.as_view(),
        name='cargue_masivo'),

    url(r'^estadistica_campaña/',
        views.EstadisticaView.as_view(),
        name='estadistica_campaña'),

    url(r'^filtro_rapido/',
        views.FiltroRapidoListView.as_view(),
        name='filtro_rapido'),

    url(r'^perfil_votante/(?P<pk>\d+)/',
        views.PerfilVotanteView.as_view(),
        name='perfil_votante'),

    url(r'^crear_supervisor/',
        views.SupervisorCreateView.as_view(),
        name='crear_supervisor'),

    url(r'^lista_supervisores/',
        views.SupervisorListView.as_view(),
        name='lista_supervisores'),

    url(r'^crear_digitador/',
        views.DigitadorCreateView.as_view(),
        name='crear_digitador'),

    url(r'^lista_digitadores/',
        views.DigitadorListView.as_view(),
        name='lista_digitadores'),

    url(r'^call_center/',
        views.CallCenterListView.as_view(),
        name='call_center'),

    url(r'^generar_reporte/',
        views.GenerarReporteTemplateView.as_view(),
        name='generar_reporte'),

    url(r'^total_votantes_modificados/',
        views.TotalVotantesModificadosListView.as_view(),
        name='votantes_modificados'),


    url(r'^busqueda_facil/',
        views.BusquedaFacilListView.as_view(),
        name='busqueda_facil'),

    url(r'^reporte_votantes_modificados/',
        views.generar_reporte_votantes_modificados,
        name='reporte_votantes_modificados'),

    url(r'^auditoria_db/',
        views.AuditoriadbTemplateView.as_view(),
        name='auditoria_db'),

    url(r'^perfil_digitador/(?P<pk>\d+)/',
        views.DigitadorDetailView.as_view(),
        name='perfil_digitador')


]
