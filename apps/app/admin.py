from django.contrib import admin
from . import models
# Register your models here.


admin.site.site_header = 'Elixir DB Admin'
admin.site.site_title = 'Elixir DB Admin'
admin.site.index_title = 'Elixir DB Admin'
