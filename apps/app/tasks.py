from __future__ import absolute_import, unicode_literals

from apps.geodata.models import Departamento, Municipio, Corregimiento, Localidad, Barrio
from apps.utils.shortcuts import get_object_or_none, remove_accent, get_coordenadas
from apps.utils import print_colors as color
from apps.votantes.models import Votante, Genero
from project.celery import app
import xlwt, xlrd, re
from io import BytesIO


# 0 codigo departamento
# 1 codigo municipio
# 2 codigo centro poblado
# 3 nombre del departamento
# 4 nombre del municipio
# 5 nombre centro poblado
# 6 tipo centro poblado
# 7 longitud
# 8 latitud
# 9 Distrito
# 10 tipo de municipio
# 11 area metropolitana


@app.task
def load_excel(file):
    print('file load :)')
    _404 = 0
    list_404 = []
    book = xlrd.open_workbook(file_contents=file.read())
    for sheet in book.sheets():
        number_of_rows = sheet.nrows
        number_of_columns = sheet.ncols

        for row in range(0, number_of_rows):
            departamento = remove_accent(sheet.cell(row, 3).value)
            municipio = remove_accent(sheet.cell(row, 4).value)
            corregomiento = sheet.cell(row, 5).value
            longitud = sheet.cell(row, 7).value
            latitud = sheet.cell(row, 9).value

            departamento_ = Departamento.search(
                name=departamento
            )

            if departamento_:
                municipio_ = Municipio.search(
                    departamento=departamento_,
                    name=municipio
                )

                if municipio_:
                    if municipio_.coordenadas is None:
                        municipio_.coordenadas = get_coordenadas(longitud, latitud)
                        municipio_.save()

                    if municipio != corregomiento:
                        corregomiento_ = get_object_or_none(
                            Corregimiento,
                            municipio=municipio_,
                            nombre=remove_accent(corregomiento.upper())
                        )
                        if not corregomiento_:
                            Corregimiento.objects.create(
                                municipio=municipio_,
                                nombre=remove_accent(corregomiento.upper()),
                                coordenadas=get_coordenadas(longitud, latitud)
                            )
                else:
                    _404 += 1
                    list_404.append('[ {} , {} ] '.format(
                        color._green(municipio),
                        color._cyan(departamento)
                    ))
                    print('{} : {} para el departamento {}'.format(
                        color._red('Error municipio no encontrado'),
                        color._cyan(municipio),
                        color._green(departamento)
                    ))
    count = 0
    for i in list_404:
        print('#{1} : {0} '.format(i, count + 1))
        count += 1
    print('total no encontrados : {}'.format(color._red(_404)))


@app.task
def upload_barrios(file):
    print('file load :)')
    book = xlrd.open_workbook(file_contents=file.read())
    for sheet in book.sheets():
        number_of_rows = sheet.nrows
        number_of_columns = sheet.ncols
        municipio_ = None
        departamento_ = None
        for row in range(0, number_of_rows):
            if row == 0:
                departamento = remove_accent(sheet.cell(row, 0).value)
                municipio = remove_accent(sheet.cell(row, 1).value)
                departamento_ = Departamento.search(
                    name=departamento.upper()
                )
                municipio_ = Municipio.search(
                    departamento=departamento_,
                    name=municipio
                )
                print('departamento : {}'.format(departamento_))
                print('municipio  : {}'.format(municipio_))
            elif row >= 2:
                localidad = remove_accent(sheet.cell(row, 0).value)
                barrio = remove_accent(sheet.cell(row, 1).value)

                localidad_ = Localidad.search(
                    municipio=municipio_,
                    name=localidad
                )
                if localidad_:
                    localidad_.new_barrio(barrio)
                    print('new barrio : {1} in {0}'.format(localidad, barrio))
                else:
                    new_localidad = municipio_.new_localidad(localidad.upper())
                    new_localidad.new_barrio(barrio)
                    print('new localidad : {} and barrio : {}'.format(localidad, barrio))


@app.task
def upload_votantes(file, user):
    import datetime
    Votante.objects.all().delete()
    print('file load :)')
    book = xlrd.open_workbook(file_contents=file.read())
    lista_votantes = []
    hora_inicio_lista = datetime.datetime.now()

    for sheet in book.sheets():
        number_of_rows = sheet.nrows
        number_of_columns = sheet.ncols
        for row in range(1, number_of_rows):
            nombre = str(sheet.cell(row, 0).value)
            try:
                cedula = str(int(sheet.cell(row, 1).value))
            except:
                cedula = str(sheet.cell(row, 1).value)
            edad = sheet.cell(row, 2).value
            email = sheet.cell(row, 3).value
            celular = sheet.cell(row, 4).value
            genero = sheet.cell(row, 5).value
            genero_ = get_object_or_none(Genero, nombre=remove_accent(genero))
            fijo = sheet.cell(row, 6).value

            if not genero_ and genero:
                genero_ = Genero.objects.create(
                    nombre=remove_accent(genero)
                )

            departamento = remove_accent(sheet.cell(row, 7).value)
            municipio = remove_accent(sheet.cell(row, 8).value)

            if departamento:
                departamento_ = Departamento.search(
                    name=departamento.upper()
                )
            else:
                departamento_ = None

            if municipio and departamento and departamento_:
                municipio_ = Municipio.search(
                    departamento=departamento_,
                    name=municipio
                )
            elif municipio:
                municipio_ = get_object_or_none(Municipio, nombre=remove_accent(municipio))
            else:
                municipio_ = None

            print('departamento : {}'.format(departamento_))
            print('municipio  : {}'.format(municipio_))

            direccion = remove_accent(sheet.cell(row, 9).value)
            barrio = remove_accent(sheet.cell(row, 10).value)
            barrio_registrado = None

            if barrio:
                barrio_ = get_object_or_none(
                    Barrio,
                    nombre=remove_accent(barrio.upper()),
                    municipio=municipio_
                )

                if barrio_:
                    barrio_ = Barrio.objects.create(
                        municipio=municipio_,
                        nombre=remove_accent(barrio.upper())
                    )
            else:
                if barrio:
                    barrio_registrado = remove_accent(barrio.upper())
                else:
                    barrio_ = None

            ocupacion = remove_accent(sheet.cell(row, 11).value)
            sector_social = remove_accent(sheet.cell(row, 12).value)

            referidos_fuera_residencia = get_boolean(sheet.cell(row, 13).value)
            lider_electoral = get_boolean(sheet.cell(row, 14).value)

            nivel_formacion = str(sheet.cell(row, 15).value)

            egresado_unimag = get_boolean(sheet.cell(row, 16).value)
            volanteo = get_boolean(sheet.cell(row, 19).value)
            reuniones = get_boolean(sheet.cell(row, 20).value)
            sondeos_opinion = get_boolean(sheet.cell(row, 21).value)
            visitas = get_boolean(sheet.cell(row, 22).value)
            divulgacion = get_boolean(sheet.cell(row, 22).value)
            distribuir_material = get_boolean(sheet.cell(row, 24).value)
            participacion = get_boolean(sheet.cell(row, 25).value)
            avanzada_territorial = get_boolean(sheet.cell(row, 26).value)
            capacitacion_voluntarios = get_boolean(sheet.cell(row, 27).value)
            apoyo_redes = get_boolean(sheet.cell(row, 28).value)
            coordinar_logistica = get_boolean(sheet.cell(row, 29).value)
            procesamiento_db = get_boolean(sheet.cell(row, 30).value)
            donacion_material_publicitario = get_boolean(sheet.cell(row, 31).value)
            tipo_material_publicitario = get_boolean(sheet.cell(row, 32).value)
            aportes_especie = get_boolean(sheet.cell(row, 33).value)
            aportes_economicos = get_boolean(sheet.cell(row, 33).value)
            tipo_aporte = sheet.cell(row, 33).value
            cantidad_aportada = sheet.cell(row, 36).value
            prestamo_vehiculo = get_boolean(sheet.cell(row, 37).value)
            movilizacion_personal = get_boolean(sheet.cell(row, 38).value)
            alojamiento = get_boolean(sheet.cell(row, 39).value)
            tipo = sheet.cell(row, 40).value

            votante = Votante(
                digitador=user,
                nombre=nombre,
                cedula=cedula,
                edad=edad,
                email=email,
                ceular=celular,
                genero=genero_,
                telefono_fijo=fijo,
                departamento=departamento_,
                municipio=municipio_,
                direccion=direccion,
                barrio=barrio_,
                ocupacion=ocupacion,
                sector_social=sector_social,
                referidos_fuera_residencia=referidos_fuera_residencia,
                lider_electoral=lider_electoral,
                nivel_formacion=nivel_formacion.upper(),
                egresado_unimag=egresado_unimag,
                volanteo=volanteo,
                reuniones=reuniones,
                sondeos_opinion=sondeos_opinion,
                visitas_puerta_puerta=visitas,
                divulgacion_propuestas=divulgacion,
                distribuir_material=distribuir_material,
                participacion_hechos_politicos=participacion,
                capacitacion_voluntarios=capacitacion_voluntarios,
                apoyos_redes_sociales=apoyo_redes,
                coordinar_logistica_eventos=coordinar_logistica,
                procesamiento_base_de_datos=procesamiento_db,
                donacion_material_publicitario=donacion_material_publicitario,
                tipo_material_publicitario=tipo_material_publicitario,
                aportes_en_especie=aportes_especie,
                aportes_economicos=aportes_economicos,
                tipo_aporte=tipo_aporte,
                cantidad_aportada=cantidad_aportada,
                prestamo_vehiculo=prestamo_vehiculo,
                movilizacion_personal=movilizacion_personal,
                alojamiento_voluntarios=alojamiento,
                tipo_votante=tipo,
                avanzada_territorial=avanzada_territorial,
                barrio_registrado=barrio_registrado
            )
            lista_votantes.append(votante)

            hora_actual = datetime.datetime.now()
            tiempo = hora_actual - hora_inicio_lista
            tiempo_transcurrido = '{} horas , {} minutos , {} segundos'.format(
                tiempo.seconds / 3600,
                tiempo.seconds / 60,
                tiempo.seconds
            )

            print('registro # {} / {} , cc : {} hora inicio : {} - hora actual {} - transcurrido :  {}'.format(
                row+1,
                number_of_rows,
                cedula,
                color._green(hora_inicio_lista.time()),
                color._orange(hora_actual.time()),
                color._cyan(tiempo_transcurrido)
            ))
    hora_inicial = datetime.datetime.now()
    print('{} : hora de comienzo : {}'.format(color._green('Start Bulk'), color._cyan(hora_inicial.time())))
    Votante.objects.bulk_create(lista_votantes)
    hora_final = datetime.datetime.now()
    tiempo = hora_final - hora_inicial
    tiempo_transcurrido = '{} horas , {} minutos , {} segundos'.format(
        tiempo.seconds / 3600,
        tiempo.seconds / 60,
        tiempo.seconds
    )
    print('hora inicial del bulk : {} - hora final del bulk {} - tiempo del bulk : {}'.format(
        color._green(hora_inicial.time()),
        color._red(hora_final.time()),
        color._cyan(tiempo_transcurrido)
    ))


def get_boolean(value):
    if type(value) is str and value == '':
        return False
    elif type(value) is float and float(value) > 0:
        return True
    else:
        return False
