from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^subir-barrios/',
        views.BarriosUploadView.as_view(),
        name='subir_barrios'),

    url(r'^subir-corregimientos/',
        views.CorregimientosUploadView.as_view(),
        name='subir_corregimientos'),

    url(r'^subir-votantes/',
        views.VotanteUploadView.as_view(),
        name='subir_votante'),

    # origenes de datos

    url(r'^departamentos/',
        views.DepartamentoListView.as_view(),
        name='departamentos'),

    url(r'^detalle_departamento/(?P<pk>\d+)/',
        views.DepartamentoDetalleListView.as_view(),
        name='detalle_departamento'),

    url(r'^municipios_departamento/(?P<pk>\d+)/',
        views.MunicipioDepartamentoView.as_view(),
        name='municipios_departamento'),

    url(r'^corregimientos_departamento/(?P<pk>\d+)/',
        views.CorregimientosDepartamentoView.as_view(),
        name='corregimientos_departamento'
        ),

    url(r'^detalle_municipio/(?P<pk>\d+)/',
        views.MunicipioDetailView.as_view(),
        name='detalle_municipio'),

    url(r'^corregimientos_municipio/(?P<pk>\d+)/',
        views.CorregimientosMunicipioView.as_view,
        name='corregimientos_municipio'),

    url(r'^barrios_municipio/(?P<pk>\d+)/',
        views.BarriosMunicipio.as_view(),
        name='barrios_municipio'),

    url(r'^lista_votantes_municipio/(?P<pk>\d+)/',
        views.ListaVotantesMunicipioListView.as_view(),
        name='lista_votantes_municipio'),

    url(r'^lista_votantes_contactados_municipio/(?P<pk>\d+)/',
        views.VotantesContactadosMunicipioListView.as_view(),
        name='lista_votantes_contactados_municipio'),

    url(r'^lista_votantes_no_contactados_municipio/(?P<pk>\d+)/',
        views.VotantesNoContactadosMunicipioListView.as_view(),
        name='lista_votantes_no_contactados_municipio'),

]
