def get_good_votes(model_attr, object_model):
    return {
        model_attr: object_model,
        'voto_duplicado': False,
        'cedula_falsa': False,
        'persona_fallecida': False
    }