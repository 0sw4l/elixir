from django.apps import AppConfig


class GeodataConfig(AppConfig):
    name = 'apps.geodata'
    verbose_name = 'Geo Data'

    def ready(self):
        from . import signals
