from django.contrib import admin
from apps.third_party_apps.gis import admin as admin_
from apps.third_party_apps.mapwidgets import GooglePointFieldWidget
from . import models

# Register your models here.


class AdminPointField(admin_.GeoModelAdmin):
    formfield_overrides = {
        models.models.PointField: {"widget": GooglePointFieldWidget},
        models.models.PolygonField: {"widget": GooglePointFieldWidget}
    }


@admin.register(models.Region)
class RegionAdmin(AdminPointField):
    list_display = [
        'id',
        'nombre',
        'coordenadas',
        'area'
    ]


@admin.register(models.Departamento)
class DepartamentoAdmin(AdminPointField):
    list_display = [
        'id',
        'nombre',
        'region',
        'municipios_total',
        'municipios',
        'cantidad_puestos_votacion',
        'cantidad_mesas',
        'cantidad_votantes'
    ]
    search_fields = ['nombre', ]
    list_filter = ['nombre', 'region']


@admin.register(models.Municipio)
class MunicipioAdmin(AdminPointField):
    list_display = [
        'id',
        'nombre',
        #'coordenadas',
        #'area',
        'region',
        'departamento',
        'get_cantidad_corregimientos',
        'cantidad_puestos_votacion',
        'cantidad_mesas',
        'cantidad_votantes'
    ]
    search_fields = ['nombre', 'contiene_comunas']
    list_filter = ['departamento']


@admin.register(models.Corregimiento)
class CorregimientoAdmin(AdminPointField):
    list_display = [
        'id',
        'nombre',
        'coordenadas',
        'area',
        'municipio',
        'cantidad_votantes'
    ]
    list_filter = ['municipio']
    search_fields = ['nombre']


@admin.register(models.Localidad)
class LocalidadAdmin(AdminPointField):
    list_display = [
        'id',
        'nombre',
        'coordenadas',
        'area',
        'municipio'
    ]
    list_filter = ['municipio']


@admin.register(models.Barrio)
class BarrioAdmin(AdminPointField):
    list_display = [
        'id',
        'nombre',
        'coordenadas',
        'area',
        'municipio',
        'localidad'
    ]
    search_fields = ['nombre']
    list_filter = ['municipio']


@admin.register(models.PuestoVotacion)
class PuestoVotacionAdmin(admin.ModelAdmin):
    list_display = ['id',
                    'nombre',
                    'cantidad_votantes',
                    'municipio',
                    'corregimiento',
                    'barrio',
                    'direccion',
                    'cantidad_mesas',
                    'cantidad_votos_reales',
                    ]
    search_fields = ['nombre']


@admin.register(models.MesaVotacion)
class MesaVotacionAdmin(admin.ModelAdmin):
    list_display = ['id', 'nombre', 'puesto_votacion', 'municipio', 'cantidad_votantes']



