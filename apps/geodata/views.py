from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView

from apps.app import tasks
from apps.geodata.models import Corregimiento
from apps.geodata.tasks import get_places_votation
from apps.utils.security import AdminPermissionMixin
from apps.utils.shortcuts import get_object_or_none
from apps.utils.views import BaseTemplateView, BaseListView, BaseDetailView
from . import models
import pure_pagination


class CorregimientosUploadView(AdminPermissionMixin, BaseTemplateView):

    template_name = 'upload_corregimientos.html'

    def post(self, request):

        try:
            data = request.FILES['excel_file']
            if data:
                tasks.load_excel(data)
        except:
            pass
        return render(request, self.template_name)


class BarriosUploadView(AdminPermissionMixin, BaseTemplateView):

    template_name = 'upload_barrios.html'

    def post(self, request):
        data = request.FILES['excel_file']
        if data:
            tasks.upload_barrios(data)
        return render(request, self.template_name)


class VotanteUploadView(AdminPermissionMixin, BaseTemplateView):
    template_name = 'upload_votantes.html'

    def post(self, request):
        try:
            data = request.FILES['excel_file']
            if data:
                tasks.upload_votantes(data, request.user)
        except:
            get_places_votation.delay()
        return render(request, self.template_name)


class DepartamentoListView(BaseListView):
    model = models.Departamento
    template_name = 'apps/geodata/departamentos/departamentos.html'


class DepartamentoDetalleListView(pure_pagination.PaginationMixin ,BaseListView):
    model = models.Votante
    paginate_by = 50
    template_name = 'apps/geodata/departamentos/detalle_departamento.html'

    def get_queryset(self):
        return self.model.objects.filter(departamento_id=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['departamento'] = models.Departamento.objects.get(
            id=self.kwargs['pk']
        )
        return context


class ListaVotantesMunicipioListView(pure_pagination.PaginationMixin ,BaseListView):
    model = models.Votante
    paginate_by = 50
    template_name = 'apps/geodata/departamentos/lista_votantes_municipio.html'

    def get_queryset(self):
        return self.model.objects.filter(municipio_id=self.kwargs['pk']).order_by('-id')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['municipio'] = models.Municipio.objects.get(
            id=self.kwargs['pk']
        )
        context['contactados'] = models.Votante.objects.filter(
            municipio_id=self.kwargs['pk'],
            contactado=True
        )
        context['no_contactados'] = models.Votante.objects.filter(
            municipio_id=self.kwargs['pk'],
            contactado=False
        )
        return context


class VotantesContactadosMunicipioListView(ListaVotantesMunicipioListView):
    def get_queryset(self):
        return self.model.objects.filter(municipio_id=self.kwargs['pk'],
                                         contactado=True)


class VotantesNoContactadosMunicipioListView(ListaVotantesMunicipioListView):
    def get_queryset(self):
        return self.model.objects.filter(municipio_id=self.kwargs['pk'],
                                         contactado=False)


class MunicipioDetailView(BaseDetailView):
    model = models.Municipio
    template_name = 'apps/geodata/municipios/detalle_municipio.html'


class MunicipioDepartamentoView(pure_pagination.PaginationMixin,
                                BaseListView):
    paginate_by = 50
    model = models.Municipio
    template_name = 'apps/geodata/departamentos/municipios_departamento.html'

    def get_id(self):
        return self.kwargs['pk']

    def get_queryset(self):
        return models.Municipio.objects.filter(departamento=self.get_id())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['departamento'] = get_object_or_none(models.Departamento, id=self.get_id())
        return context


class CorregimientosMunicipioView(BaseListView):
    model = models.Corregimiento
    template_name = 'apps/geodata/municipios/corregimientos_municipio.html'


class CorregimientosDepartamentoView(pure_pagination.PaginationMixin,
                                     BaseListView):
    paginate_by = 100
    model = models.Corregimiento
    template_name = 'apps/geodata/departamentos/corregimientos_departamento.html'

    def get_id(self):
        return self.kwargs['pk']

    def get_queryset(self):
        return models.Corregimiento.objects.filter(
            municipio__departamento_id=self.get_id()
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['departamento'] = get_object_or_none(models.Departamento, id=self.get_id())
        return context


class BarriosMunicipio(BaseListView):
    model = models.Barrio
    template_name = 'apps/geodata/municipios/barrios_municipio.html'


