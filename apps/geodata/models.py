from django.db import models
from datetime import datetime
# Create your models here.
from django.contrib.auth.models import User
from django import urls
from django.db.models import Q
from smart_selects.db_fields import ChainedForeignKey

from . import constants
from apps.third_party_apps.gis.db import models

# Create your models here.
from apps.utils.shortcuts import get_object_or_none, remove_accent
from apps.votantes.models import Votante, LiderElectoral


class Base(models.Model):
    nombre = models.CharField(max_length=255)
    coordenadas = models.PointField(blank=True, null=True)
    area = models.PolygonField(blank=True, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.nombre

    def get_longitude(self):
        return self.get_data_point()[0] if self.coordenadas is not None else 0

    def get_latitude(self):
        return self.get_data_point()[1] if self.coordenadas is not None else 0

    def get_data_point(self):
        return [coord for coord in self.coordenadas]


class BaseNombreVotantes(models.Model):
    nombre = models.CharField(max_length=50)
    registro = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.nombre


class ContarVotosNegativos(object):
    attr_query = None

    def votos_reales(self):
        return Votante.objects.filter(**constants.get_good_votes(
            self.attr_query,
            self
        ))

    def cantidad_votos_reales(self):
        return self.votos_reales().count()


class Region(Base):
    class Meta:
        verbose_name = 'Region'
        verbose_name_plural = 'Regiones'

    def get_departamentos(self):
        return Departamento.objects.filter(region=self)


class Departamento(Base):
    numero = models.CharField(max_length=3, blank=True, null=True)
    region = models.ForeignKey(Region, blank=True, null=True)
    area_total = models.DecimalField(max_digits=30, decimal_places=15, blank=True, null=True)
    hectareas = models.DecimalField(max_digits=30, decimal_places=15, blank=True, null=True)
    perimetro = models.DecimalField(max_digits=30, decimal_places=15, blank=True, null=True)
    area = models.GeometryField(blank=True, null=True)
    imagen = models.ImageField(upload_to='departamentos', blank=True, null=True)

    class Meta:
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'
        ordering = ['nombre']

    def municipios_total(self):
        return "\n".join(['{}. '.format(p.nombre) for p in self.get_municipios()])

    def get_municipios(self):
        return Municipio.objects.filter(departamento=self)

    def municipios(self):
        return self.get_municipios().count()

    def get_corregimientos(self):
        return Corregimiento.objects.filter(municipio__departamento=self)

    def votantes(self):
        return Votante.objects.filter(municipio__departamento=self)

    def cantidad_votantes(self):
        return self.votantes().count()

    def puestos_votacion(self):
        return PuestoVotacion.objects.filter(municipio__departamento=self)

    def cantidad_puestos_votacion(self):
        return self.puestos_votacion().count()

    def mesas(self):
        return MesaVotacion.objects.filter(puesto_votacion__municipio__departamento=self)

    def cantidad_mesas(self):
        return self.mesas().count()

    @classmethod
    def search(cls, name):
        return get_object_or_none(
            cls,
            nombre=remove_accent(name.upper())
        )

    def get_kwargs(self):
        return {
            'pk': self.pk
        }

    def get_absolute_url(self):
        return urls.reverse_lazy('geodata:detalle_departamento', kwargs=self.get_kwargs())

    def get_municipios_url(self):
        return urls.reverse_lazy('geodata:municipios_departamento', kwargs=self.get_kwargs())

    def get_corregimientos_url(self):
        return urls.reverse_lazy('geodata:corregimientos_departamento', kwargs=self.get_kwargs())

    @staticmethod
    def get_votantes_filter(**kwargs):
        return Votante.objects.filter(**kwargs)

    def get_votantes_senado(self):
        return self.get_votantes_filter(**{
            'municipio__departamento': self,
            'corregimiento__isnull': True,
            'votaria_senado': True
        })

    def get_votantes_senado_count(self):
        return self.get_votantes_senado().count()

    def get_votantes_camara(self):
        return self.get_votantes_filter(**{
            'municipio__departamento': self,
            'corregimiento__isnull': True,
            'votaria_camara': True
        })

    def get_votantes_camara_count(self):
        return self.get_votantes_camara().count()

    def get_votantes_presidencia(self):
        return self.get_votantes_filter(**{
            'municipio__departamento': self,
            'corregimiento__isnull': True,
            'votaria_presidencia': True
        })

    def get_votantes_presidencia_count(self):
        return self.get_votantes_presidencia().count()

    def get_votantes_contactados(self):
        return self.get_votantes_filter(**{
            'municipio__departamento': self,
            'corregimiento__isnull': True,
            'contactado': True
        })

    def get_votantes_contactados_count(self):
        return self.get_votantes_contactados().count()

    def get_votantes_no_contactados(self):
        return self.get_votantes_filter(**{
            'municipio__departamento': self,
            'corregimiento__isnull': True,
            'contactado': False
        })

    def get_votantes_no_contactados_count(self):
        return self.get_votantes_no_contactados().count()

    def get_votantes_sin_cedula(self):
        return self.get_votantes_filter(**{
            'cedula__exact': '',
            'municipio__departamento': self
        })

    def get_votantes_sin_cedula_count(self):
        return self.get_votantes_sin_cedula().count()

    def get_simpatizantes(self):
        return self.get_votantes_filter(**{
            'tipo_votante__icontains': 'SIMPATIZANTE',
            'municipio__departamento': self
        })

    def get_referidos(self):
        return self.get_votantes_filter(**{
            'tipo_votante__icontains': 'REFERIDO',
            'municipio__departamento': self
        })

    def get_voluntario(self):
        return self.get_votantes_filter(**{
            'tipo_votante__icontains': 'VOLUNTARIO',
            'municipio__departamento': self
        })

    def get_nuevos_hoy(self):
        date = datetime.now().date()
        return self.get_votantes_filter(**{
            'municipio__departamento': self,
            'creacion__day': date.day,
            'creacion__month': date.month,
            'creacion__year': date.year
        })


class Municipio(Base):
    nombre_cab = models.CharField(max_length=255, blank=True, null=True)
    region = models.ForeignKey(Region, blank=True, null=True)
    departamento = models.ForeignKey(Departamento, blank=True, null=True)
    ciudad = models.BooleanField(default=False, editable=False)
    organizado_localidad = models.BooleanField(default=False)
    capital_departamental = models.BooleanField(default=False)
    contiene_comunas = models.BooleanField(default=False)
    contiene_upz = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Municipio'
        verbose_name_plural = 'Municipios'
        ordering = ['nombre', 'departamento']

    def get_corregimientos(self):
        return Corregimiento.objects.filter(municipio=self)

    def get_votantes_filter(self, **kwargs):
        return Votante.objects.filter(**kwargs)

    def get_votantes_senado(self):
        return self.get_votantes_filter(**{
            'municipio': self,
            'corregimiento__isnull': True,
            'votaria_senado': True
        })

    def get_votantes_senado_count(self):
        return self.get_votantes_senado().count()

    def get_votantes_camara(self):
        return self.get_votantes_filter(**{
            'municipio': self,
            'corregimiento__isnull': True,
            'votaria_camara': True
        })

    def get_votantes_camara_count(self):
        return self.get_votantes_camara().count()

    def get_votantes_presidencia(self):
        return self.get_votantes_filter(**{
            'municipio': self,
            'corregimiento__isnull': True,
            'votaria_presidencia': True
        })

    def get_votantes_presidencia_count(self):
        return self.get_votantes_presidencia().count()

    def get_votantes_contactados(self):
        return self.get_votantes_filter(**{
            'municipio': self,
            'corregimiento__isnull': True,
            'contactado': True
        })

    def get_votantes_contactados_count(self):
        return self.get_votantes_contactados().count()

    def get_votantes_no_contactados(self):
        return self.get_votantes_filter(**{
            'municipio': self,
            'corregimiento__isnull': True,
            'contactado': False
        })

    def get_votantes_no_contactados_count(self):
        return self.get_votantes_no_contactados().count()

    def get_votantes_sin_cedula(self):
        return self.get_votantes_filter(**{
            'cedula__exact': '',
            'municipio': self,
        })

    def get_votantes_sin_cedula_count(self):
        return self.get_votantes_sin_cedula().count()

    def get_cantidad_corregimientos(self):
        return self.get_corregimientos().count()

    def get_municipios(self):
        return Municipio.objects.filter(departamento=self)

    def get_localidades(self):
        return Localidad.objects.filter(municipio=self)

    def get_upz(self):
        return Upz.objects.filter(localidad__municipio=self)

    def get_barrios(self):
        return Barrio.objects.filter(municipio=self)

    def votantes(self):
        return Votante.objects.filter(municipio=self, corregimiento__isnull=True)

    def cantidad_votantes(self):
        return self.votantes().count()

    def puestos_votacion(self):
        return PuestoVotacion.objects.filter(municipio=self)

    def cantidad_puestos_votacion(self):
        return self.puestos_votacion().count()

    def mesas(self):
        return MesaVotacion.objects.filter(puesto_votacion__municipio=self)

    def cantidad_mesas(self):
        return self.mesas().count()

    @classmethod
    def search(cls, departamento, name):
        return get_object_or_none(
            cls,
            departamento=departamento,
            nombre=remove_accent(name.upper())
        )

    def new_localidad(self, nombre):
        return Localidad.objects.create(
            municipio=self,
            nombre=remove_accent(nombre.upper())
        )

    def get_kwargs(self):
        return {
            'pk': self.pk
        }

    def get_absolute_url(self):
        return urls.reverse_lazy('geodata:detalle_municipio', kwargs=self.get_kwargs())

    def get_corregimientos_url(self):
        return urls.reverse_lazy('geodata:corregimientos_municipio', kwargs=self.get_kwargs())

    def get_barrios_url(self):
        return urls.reverse_lazy('geodata:barrios_municipio', kwargs=self.get_kwargs())

    def get_lista_votantes_municipio_url(self):
        return urls.reverse_lazy('geodata:lista_votantes_municipio', kwargs=self.get_kwargs())

    def get_votantes_contactados_url(self):
        return urls.reverse_lazy('geodata:lista_votantes_contactados_municipio',
                                 kwargs=self.get_kwargs()
                                 )

    def get_votantes_no_contactados_url(self):
        return urls.reverse_lazy('geodata:lista_votantes_no_contactados_municipio',
                                 kwargs=self.get_kwargs()
                                 )

    def get_simpatizantes(self):
        return self.get_votantes_filter(**{
            'tipo_votante__icontains': 'SIMPATIZANTE',
            'municipio': self,
        })

    def get_referidos(self):
        return self.get_votantes_filter(**{
            'tipo_votante__icontains': 'REFERIDO',
            'municipio': self,
        })

    def get_voluntario(self):
        return self.get_votantes_filter(**{
            'tipo_votante__icontains': 'VOLUNTARIO',
            'municipio': self,
        })

    def get_nuevos_hoy(self):
        date = datetime.now().date()
        return self.get_votantes_filter(**{
            'municipio': self,
            'creacion__day': date.day,
            'creacion__month': date.month,
            'creacion__year': date.year
        })


class Corregimiento(Base):
    municipio = models.ForeignKey(Municipio)

    class Meta:
        verbose_name = 'Corregimiento'
        verbose_name_plural = 'Corregimientos'
        ordering = ['municipio', 'nombre']

    def votantes(self):
        return Votante.objects.filter(corregimiento=self)

    def cantidad_votantes(self):
        return self.votantes().count()

    @classmethod
    def search(cls, **kwargs):
        return get_object_or_none(
            cls,
            **kwargs
        )


class Localidad(Base):
    municipio = models.ForeignKey(Municipio)

    class Meta:
        verbose_name = 'Localidad'
        verbose_name_plural = 'Localidades'

    def get_upz(self):
        return Upz.objects.filter(localidad=self)

    def get_barrios(self):
        return Barrio.objects.filter(localidad=self)

    def new_barrio(self, nombre):
        return Barrio.objects.create(
            localidad=self,
            municipio=self.municipio,
            nombre=remove_accent(nombre.upper())
        )

    @classmethod
    def search(cls, municipio, name):
        return get_object_or_none(
            cls,
            municipio=municipio,
            nombre=remove_accent(name.upper())
        )


class Upz(Base):
    localidad = models.ForeignKey(
        Localidad,
        blank=True,
        null=True
    )

    @classmethod
    def search(cls, localidad, name):
        return get_object_or_none(
            Municipio,
            localidad=localidad,
            nombre=remove_accent(name.upper())
        )


class Barrio(Base):
    municipio = models.ForeignKey(Municipio, blank=True, null=True)
    localidad = models.ForeignKey(Localidad, blank=True, null=True)
    upz = models.ForeignKey(Upz, blank=True, null=True)

    class Meta:
        verbose_name = 'Barrio'
        verbose_name_plural = 'Barrios'
        ordering = ['upz']

    def votantes(self):
        return Votante.objects.filter(barrio=self)

    def cantidad_votantes(self):
        return self.votantes().count()

    def puestos_votacion(self):
        return PuestoVotacion.objects.filter(barrio=self)

    def cantidad_puestos_votacion(self):
        return self.puestos_votacion().count()

    def mesas(self):
        return MesaVotacion.objects.filter(puesto_votacion__municipio=self)

    def cantidad_mesas(self):
        return self.mesas().count()

    def lideres(self):
        return LiderElectoral.objects.filter(barrio=self)

    def cantidad_lideres(self):
        return self.lideres().count()


class PuestoVotacion(BaseNombreVotantes,
                     ContarVotosNegativos):
    attr_query = 'puesto_votacion'
    municipio = models.ForeignKey(Municipio, related_name='+', blank=True, null=True)
    corregimiento = models.ForeignKey(Corregimiento, blank=True, null=True)
    barrio = ChainedForeignKey(
        Barrio,
        chained_field='municipio',
        chained_model_field='municipio',
        blank=True,
        null=True,
        related_name='+'
    )
    direccion = models.CharField(max_length=50, blank=True, null=True)
    localidad = models.ForeignKey(Localidad, blank=True, null=True)

    class Meta:
        verbose_name = 'Puesto Votacion'
        verbose_name_plural = 'Puestos de votacion'
        ordering = ['nombre']

    def get_absolute_url(self):
        return urls.reverse_lazy('app:detalle_puesto', kwargs={
            'pk': self.pk
        })

    def mesas(self):
        return MesaVotacion.objects.filter(puesto_votacion=self)

    def cantidad_mesas(self):
        return self.mesas().count()

    def votantes(self):
        return Votante.objects.filter(puesto_votacion=self)

    def cantidad_votantes(self):
        return self.votantes().count()


class MesaVotacion(BaseNombreVotantes,
                   ContarVotosNegativos):
    attr_query = 'mesa_votacion'
    nombre = models.CharField(max_length=50)
    puesto_votacion = models.ForeignKey(PuestoVotacion, related_name='+')
    localidad = models.ForeignKey(Localidad, blank=True, null=True)

    class Meta:
        verbose_name = 'Mesa de Votacion'
        verbose_name_plural = 'Mesas de Votaciones'

    def get_absolue_url(self):
        return urls.reverse_lazy(
            'app:detalle_mesa',
            kwargs={
                'pk': self.pk
            }
        )

    def municipio(self):
        return self.puesto_votacion.municipio

    def votantes(self):
        return Votante.objects.filter(mesa_votacion=self)

    def cantidad_votantes(self):
        return self.votantes().count()

    def get_votos_negativos(self):
        return Votante.objects.filter(
            Q(voto_duplicado=True) |
            Q(cedula_falsa=True) |
            Q(persona_fallecida=True),
            mesa_votacion=self
        )

    def get_votos_positivos(self):
        return Votante.objects.filter(
            mesa_votacion=self,
            voto_duplicado=False,
            cedula_falsa=False,
            persona_fallecida=False
        )
