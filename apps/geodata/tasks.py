from __future__ import absolute_import, unicode_literals
from apps.utils.shortcuts import get_object_or_none, remove_accent, get_coordenadas
from apps.utils import print_colors as color
from apps.votantes.models import Votante
from apps.votantes.signals import collect_data
from project.celery import app
from .models import Departamento, Municipio, Corregimiento, Localidad, Barrio, Upz
import xlwt, xlrd, re
from io import BytesIO


# 0 codigo departamento
# 1 codigo municipio
# 2 codigo centro poblado
# 3 nombre del departamento
# 4 nombre del municipio
# 5 nombre centro poblado
# 6 tipo centro poblado
# 7 longitud
# 8 latitud
# 9 Distrito
# 10 tipo de municipio
# 11 area metropolitana


@app.task
def load_excel(file):
    print('file load :)')
    _404 = 0
    list_404 = []
    book = xlrd.open_workbook(file_contents=file.read())
    for sheet in book.sheets():
        number_of_rows = sheet.nrows
        number_of_columns = sheet.ncols

        for row in range(0, number_of_rows):
            departamento = remove_accent(sheet.cell(row, 3).value)
            municipio = remove_accent(sheet.cell(row, 4).value)
            corregomiento = sheet.cell(row, 5).value
            longitud = sheet.cell(row, 7).value
            latitud = sheet.cell(row, 9).value

            departamento_ = Departamento.search(
                name=departamento
            )

            if departamento_:
                municipio_ = Municipio.search(
                    departamento=departamento_,
                    name=municipio
                )

                if municipio_:
                    if municipio_.coordenadas is None:
                        municipio_.coordenadas = get_coordenadas(longitud, latitud)
                        municipio_.save()

                    if municipio != corregomiento:
                        corregomiento_ = Corregimiento.objects.create(
                            municipio=municipio_,
                            nombre=remove_accent(corregomiento.upper()),
                            coordenadas=get_coordenadas(longitud, latitud)
                        )
                else:
                    _404 += 1
                    list_404.append('[ {} , {} ] '.format(
                        color._green(municipio),
                        color._cyan(departamento)
                    ))
                    print('{} : {} para el departamento {}'.format(
                        color._red('Error municipio no encontrado'),
                        color._cyan(municipio),
                        color._green(departamento)
                    ))
    count = 0
    for i in list_404:
        print('#{1} : {0} '.format(i, count+1))
        count += 1
    print('total no encontrados : {}'.format(color._red(_404)))


@app.task
def upload_barrios(file):
    print('file load :)')
    book = xlrd.open_workbook(file_contents=file.read())
    for sheet in book.sheets():
        number_of_rows = sheet.nrows
        number_of_columns = sheet.ncols
        municipio_ = None
        departamento_ = None
        for row in range(0, number_of_rows):
            if row == 0:
                departamento = remove_accent(sheet.cell(row, 0).value)
                municipio = remove_accent(sheet.cell(row, 1).value)
                departamento_ = Departamento.search(
                    name=departamento.upper()
                )
                municipio_ = Municipio.search(
                    departamento=departamento_,
                    name=municipio
                )
                print('departamento : {}'.format(departamento_))
                print('municipio  : {}'.format(municipio_))
            elif row >= 2:
                localidad = remove_accent(sheet.cell(row, 0).value)
                barrio = remove_accent(sheet.cell(row, 1).value)

                localidad_ = Localidad.search(
                    municipio=municipio_,
                    name=localidad
                )
                if localidad_:
                    localidad_.new_barrio(barrio)
                else:
                    new_localidad = municipio_.new_localidad(localidad.upper())
                    new_localidad.new_barrio(barrio)
                    print('new localidad : {}, barrio : {}'.format(localidad, barrio))


@app.task
def get_places_votation():
    import datetime
    votantes = Votante.get_votantes_con_cedula()
    cantidad = votantes.count()
    hora_inicial = str(datetime.datetime.now().time())
    start = 0
    for votante in votantes:
        collect_data(votante)
        print('votantes # {0} / {1} con cedula : {4} , hora inicial : {2} , hora actual {3}'.format(
            color._cyan(start+1),
            color._green(cantidad),
            color._blue(hora_inicial),
            color._orange(str(datetime.datetime.now().time())),
            votante.cedula
        ))
        start += 1

