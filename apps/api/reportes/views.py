import json

from django.http import HttpResponse
from rest_framework.decorators import api_view
from apps.reportes.constants import SIN_CEDULA, SIN_NOMBRE
from apps.reportes.tasks import crear_reporte_votantes_excel
from apps.utils.shortcuts import parse_boolean


@api_view(['GET', 'POST'])
def generar_reporte(request):
    if request.method == 'POST':
        contactado = request.POST.get('contactado', None)
        no_volver_a_llamar = request.POST.get('no_volver_a_llamar', None)
        volver_a_llamar = request.POST.get('volver_a_llamar', None)
        celular_apagado = request.POST.get('celular_apagado', None)
        no_contesto = request.POST.get('no_contesto', None)
        cedula = request.POST.get('cedula', None)
        departamento = request.POST.get('departamento', None)
        municipio = request.POST.get('municipio', None)
        corregimiento = request.POST.get('corregimiento', None)
        edad_minima = request.POST.get('edad_final__gte', None)
        edad_maxima = request.POST.get('edad_final__lte', None)
        nombre = request.POST.get('nombre', None)
        voluntario = request.POST.get('voluntario', None)
        simpatizante = request.POST.get('simpatizante', None)
        votantes_sin_cedula = request.POST.get('votantes_sin_cedula', None)
        votantes_sin_nombre = request.POST.get('votantes_sin_nombre', None)
        telefono = request.POST.get('telefono', None)
        voto_fuerte = request.POST.get('voto_fuerte', None)

        args = []
        query = {}
        query_exclude = {}
        if parse_boolean(voto_fuerte):
            query['voto_fuerte'] = parse_boolean(voto_fuerte)
        if cedula:
            query['cedula'] = str(cedula)
        if parse_boolean(contactado):
            query['contactado'] = parse_boolean(contactado)
            query_exclude['contactado'] = not parse_boolean(contactado)
        if parse_boolean(no_volver_a_llamar):
            query['no_volver_a_llamar'] = parse_boolean(no_volver_a_llamar)
            query_exclude['no_volver_a_llamar'] = not parse_boolean(no_volver_a_llamar)
        if departamento:
            query['departamento_id'] = departamento
        if municipio:
            query['municipio_id'] = municipio
        if corregimiento:
            query['corregimiento_id'] = corregimiento
        if edad_minima:
            query['edad_final__gte'] = edad_minima
        if edad_maxima:
            query['edad_final__lte'] = edad_maxima
        if nombre:
            query['nombre__icontains'] = nombre
        if volver_a_llamar:
            query['volver_a_llamar'] = parse_boolean(volver_a_llamar)
            query_exclude['volver_a_llamar'] = not parse_boolean(volver_a_llamar)
        if celular_apagado:
            query['celular_apagado'] = parse_boolean(celular_apagado)
            query_exclude['celular_apagado'] = not parse_boolean(celular_apagado)
        if no_contesto:
            query['no_contesto'] = parse_boolean(no_contesto)
            query_exclude['no_contesto'] = not parse_boolean(no_contesto)
        if simpatizante:
            if parse_boolean(simpatizante):
                query['tipo_votante__icontains'] = 'SIMPATIZANTE'
        if voluntario:
            if parse_boolean(voluntario):
                query['tipo_votante__icontains'] = 'VOLUNTARIO'
        if telefono:
            query['ceular__icontains'] = telefono
        if votantes_sin_cedula:
            if parse_boolean(votantes_sin_cedula):
                args = SIN_CEDULA
        if votantes_sin_nombre:
            if parse_boolean(votantes_sin_nombre):
                args = SIN_NOMBRE
        crear_reporte_votantes_excel.delay(request.user.id, args, query, query_exclude)
        return HttpResponse(json.dumps({
            "success": True
        }), content_type="application/json")
    else:
        return HttpResponse(json.dumps({
            "data": "only post ;)"
        }), content_type="application/json")

