from django.conf.urls import url, include
from rest_framework import routers
from . import views
router = routers.DefaultRouter()

urlpatterns = [
    url(r'^generar_reporte/', views.generar_reporte, name='generar_reporte  ')
]
