from django.conf.urls import url, include
from rest_framework import routers

urlpatterns = [
    url(r'^app/', include('apps.api.app.urls', namespace='app')),
    url(r'^geodata/', include('apps.api.geodata.urls', namespace='geodata')),
    url(r'^votantes/', include('apps.api.votantes.urls', namespace='votantes')),
    url(r'^reportes/', include('apps.api.reportes.urls', namespace='reportes')),
]
