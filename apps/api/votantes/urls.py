from django.conf.urls import url, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    url(r'^crear_votantes/',
        views.VotanteCreateApiView.as_view(),),

    url(r'^filtro_votantes_/',
        views.filtro_votantes,
        name='filtro_votantes'),

    url(r'^modificar_votante/(?P<pk>[0-9]+)/',
        views.VotanteDetail.as_view()),

    url(r'^update_votantes/',
        views.update_item),

    url(r'^filtro_votantes/',
        views.VotanteGlobalView.as_view(),
        name='votante_global_filter'),

    url(r'^data_from_fuerza/',
        views.VotanteFromFuerzaCreateApiView.as_view()),

    url(r'^comprobar_votante_existente/',
        views.comprobar_votante_existente,
        name='comprobar_votante_existente'),

    url(r'^auditoria_votantes/',
        views.VotanteEmptyFieldsView.as_view(),
        name='auditoria_votantes')

]
