import json
from collections import OrderedDict

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics, mixins
from rest_framework.decorators import api_view
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from apps.api.votantes.serializers import VotanteSerializer, VotanteFiltroSerializer, VotanteDataSerializer, \
    VotanteFuerzaSerializer
from apps.main.models import LogVotante
from apps.utils.shortcuts import parse_boolean, get_object_or_none
from apps.votantes.models import Votante, LiderElectoral


class VotanteCreateApiView(APIView):
    model = Votante
    serializer_class = VotanteSerializer
    permission_classes = (IsAuthenticated,)

    lider_global = None

    def get_lider(self, cc):
        lider = None
        if not self.lider_global:
            lider = get_object_or_none(LiderElectoral, cedula=cc)
            if not lider:
                lider = LiderElectoral.objects.create(
                    cedula=cc
                )
                self.lider_global = lider
            else:
                self.lider_global = lider
        return self.lider_global if self.lider_global else lider

    def post(self, request, format=None):
        data = request.data.copy()
        dictlist = []
        for key, value in data.items():
            temp = [key, value]
            dictlist.append(temp)
        dictlist.sort()
        count = 0
        objetos = 0
        votantes = []
        for index, obj in enumerate(dictlist):
            if count == 0:
                barrio_registrado = dictlist[index][1]
            elif count == 1:
                cedula = dictlist[index][1]
            elif count == 2:
                self.get_lider(dictlist[index][1])
            elif count == 3:
                celular = dictlist[index][1]
            elif count == 4:
                consecutivo_padron = dictlist[index][1]
            elif count == 5:
                contactado = True if dictlist[index][1] == 'true' else False
            elif count == 6:
                corregimiento = int(dictlist[index][1]) if dictlist[index][1] != '' else None
            elif count == 7:
                correo_electronico = dictlist[index][1]
            elif count == 8:
                departamento = int(dictlist[index][1]) if dictlist[index][1] != '' else None
            elif count == 9:
                digitador = int(request.user.id)
            elif count == 10:
                direccion = dictlist[index][1]
            elif count == 11:
                municipio = int(dictlist[index][1]) if dictlist[index][1] != '' else None
            elif count == 12:
                nombre = dictlist[index][1]
            elif count == 13:
                tipo_votante = dictlist[index][1]
            elif count == 14:
                votaria_camara = True if dictlist[index][1] == 'true' else False
            elif count == 15:
                votaria_presidencia = True if dictlist[index][1] == 'true' else False
            elif count == 16:
                votaria_senado = True if dictlist[index][1] == 'true' else False
            elif count == 17:
                voto_fuerte = True if dictlist[index][1] == 'true' else False
            count += 1
            if count == 18:
                if Votante.objects.filter(cedula=cedula).count() == 0:
                    votantes.append(Votante(
                        barrio_registrado=barrio_registrado,
                        cedula=cedula,
                        ceular=celular,
                        corregimiento_id=corregimiento,
                        email=correo_electronico,
                        departamento_id=departamento,
                        digitador_id=digitador,
                        direccion=direccion,
                        municipio_id=municipio,
                        nombre=nombre,
                        votaria_camara=votaria_camara,
                        votaria_presidencia=votaria_presidencia,
                        votaria_senado=votaria_senado,
                        lider_id=self.lider_global.id if self.lider_global else None,
                        consecutivo_padron=consecutivo_padron,
                        tipo_votante=tipo_votante,
                        voto_fuerte=voto_fuerte,
                        contactado=contactado
                    ))
                objetos += 1
                count = 0
        if objetos > 0:
            Votante.objects.bulk_create(votantes)
            return Response({
                'success': True
            }, status=status.HTTP_201_CREATED)

        return Response({
            'success': False
        }, status=status.HTTP_201_CREATED)


@api_view(['GET', 'POST'])
def filtro_votantes(request):
    if request.method == 'POST':
        data = request.data.dict()
        data['contactado'] = parse_boolean(data['contactado'])
        data['no_volver_a_llamar'] = parse_boolean(data['no_volver_a_llamar'])
        if len(data) > 0:
            votantes = Votante.objects.filter(**data)
            serializer = VotanteFiltroSerializer(votantes, many=True)
            return HttpResponse(json.dumps({
                "data": serializer.data,
                "objects": votantes.count()
            }), content_type="application/json")
        return HttpResponse(json.dumps({
            "data": "request post"
        }), content_type="application/json")
    else:
        return HttpResponse(json.dumps({
            "data": "only post ;)"
        }), content_type="application/json")


class VotanteDetail(APIView):

    queryset = Votante.objects.all()
    serializer_class = VotanteDataSerializer
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get_object(pk):
        try:
            return Votante.objects.get(pk=pk)
        except Votante.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        votante = self.get_object(pk)
        serializer = VotanteDataSerializer(votante)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        data = request.data.copy()
        lider = request.POST.get('lider', None)
        if lider:
            cc_lider = data['lider']
            lider = get_object_or_none(LiderElectoral, cedula=str(cc_lider))
            if not lider:
                lider = LiderElectoral.objects.create(cedula=str(cc_lider))
            data['lider'] = lider.id
        serializer = VotanteDataSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@login_required
def update_item(request):
    if request.method == 'POST':
        id = request.POST.get('id')

        votante = get_object_or_none(Votante, id=int(id))

        if votante:
            last_update_user = request.POST.get('last_update_user', None)
            votante.last_update_user_id = int(last_update_user)
            # booleans
            referidos_fuera_residencia = request.POST.get('referidos_fuera_residencia', None)
            egresado_unimag = request.POST.get('egresado_unimag', None)
            volanteo = request.POST.get('volanteo', None)
            reuniones = request.POST.get('reuniones', None)
            sondeos_opinion = request.POST.get('sondeos_opinion', None)
            divulgacion_propuestas = request.POST.get('divulgacion_propuestas', None)
            distribuir_material = request.POST.get('distribuir_material', None)
            participacion_hechos_politicos = request.POST.get('participacion_hechos_politicos', None)
            avanzada_territorial = request.POST.get('avanzada_territorial', None)
            capacitacion_voluntarios = request.POST.get('capacitacion_voluntarios', None)
            apoyos_redes_sociales = request.POST.get('apoyos_redes_sociales', None)
            coordinar_logistica_eventos = request.POST.get('coordinar_logistica_eventos', None)
            procesamiento_base_de_datos = request.POST.get('procesamiento_base_de_datos', None)
            donacion_material_publicitario = request.POST.get('donacion_material_publicitario', None)
            aportes_en_especie = request.POST.get('aportes_en_especie', None)
            aportes_economicos = request.POST.get('aportes_economicos', None)
            prestamo_vehiculo = request.POST.get('prestamo_vehiculo', None)
            movilizar_avanzada = request.POST.get('movilizar_avanzada', None)
            movilizacion_personal = request.POST.get('movilizacion_personal', None)
            alojamiento_voluntarios = request.POST.get('alojamiento_voluntarios', None)
            visitas_puerta_puerta = request.POST.get('visitas_puerta_puerta', None)
            votaria_senado = request.POST.get('votaria_senado', None)
            votaria_camara = request.POST.get('votaria_camara', None)
            votaria_presidencia = request.POST.get('votaria_presidencia', None)
            contactado = request.POST.get('contactado', None)
            no_volver_a_llamar = request.POST.get('no_volver_a_llamar', None)
            no_contesto = request.POST.get('no_contesto', None)
            celular_apagado = request.POST.get('celular_apagado', None)
            celular_equivocado = request.POST.get('celular_equivocado', None)
            voto_fuerte = request.POST.get('voto_fuerte', None)
            volver_a_llamar = request.POST.get('volver_a_llamar', None)

            cambios = []
            no_volver_a_contactar = False
            no_contesto_ = False
            celular_equivocado_ = False
            celular_apagado_ = False
            llamada = False

            if referidos_fuera_residencia:
                cambios.append('referidos_fuera_residencia de {} a {}'.format(
                    votante.referidos_fuera_residencia,
                    referidos_fuera_residencia
                ))
                votante.referidos_fuera_residencia = parse_boolean(referidos_fuera_residencia)
            if egresado_unimag:
                cambios.append('egresado_unimag de {} a {}'.format(
                    votante.egresado_unimag,
                    parse_boolean(egresado_unimag)
                ))
                votante.egresado_unimag = parse_boolean(egresado_unimag)
            if volanteo:
                cambios.append('volanteo de {} a {}'.format(
                    votante.volanteo,
                    parse_boolean(volanteo)
                ))
                votante.volanteo = parse_boolean(volanteo)
            if reuniones:
                cambios.append('reuniones de {} a {}'.format(
                    votante.reuniones,
                    parse_boolean(reuniones)
                ))
                votante.reuniones = parse_boolean(reuniones)
            if sondeos_opinion:
                cambios.append('sondeos_opinion de {} a {}'.format(
                    votante.sondeos_opinion,
                    parse_boolean(sondeos_opinion)
                ))
                votante.sondeos_opinion = parse_boolean(sondeos_opinion)
            if divulgacion_propuestas:
                cambios.append('divulgacion_propuestas de {} a {}'.format(
                    votante.divulgacion_propuestas,
                    parse_boolean(divulgacion_propuestas)
                ))
                votante.divulgacion_propuestas = parse_boolean(divulgacion_propuestas)
            if distribuir_material:
                cambios.append('distribuir_material de {} a {}'.format(
                    votante.distribuir_material,
                    parse_boolean(distribuir_material)
                ))
                votante.distribuir_material = parse_boolean(distribuir_material)
            if participacion_hechos_politicos:
                cambios.append('participacion_hechos_politicos de {} a {}'.format(
                    votante.participacion_hechos_politicos,
                    parse_boolean(participacion_hechos_politicos)
                ))
                votante.participacion_hechos_politicos = parse_boolean(participacion_hechos_politicos)
            if avanzada_territorial:
                cambios.append('avanzada_territorial de {} a {}'.format(
                    votante.avanzada_territorial,
                    parse_boolean(avanzada_territorial)
                ))
                votante.avanzada_territorial = parse_boolean(avanzada_territorial)
            if capacitacion_voluntarios:
                cambios.append('capacitacion_voluntarios de {} a {}'.format(
                    votante.capacitacion_voluntarios,
                    parse_boolean(capacitacion_voluntarios)
                ))
                votante.capacitacion_voluntarios = parse_boolean(capacitacion_voluntarios)
            if apoyos_redes_sociales:
                cambios.append('apoyos_redes_sociales de {} a {}'.format(
                    votante.apoyos_redes_sociales,
                    parse_boolean(apoyos_redes_sociales)
                ))
                votante.apoyos_redes_sociales = parse_boolean(apoyos_redes_sociales)
            if coordinar_logistica_eventos:
                cambios.append('coordinar_logistica_eventos de {} a {}'.format(
                    votante.coordinar_logistica_eventos,
                    parse_boolean(coordinar_logistica_eventos)
                ))
                votante.coordinar_logistica_eventos = parse_boolean(coordinar_logistica_eventos)
            if procesamiento_base_de_datos:
                cambios.append('procesamiento_base_de_datos de {} a {}'.format(
                    votante.procesamiento_base_de_datos,
                    parse_boolean(procesamiento_base_de_datos)
                ))
                votante.procesamiento_base_de_datos = parse_boolean(procesamiento_base_de_datos)
            if donacion_material_publicitario:
                cambios.append('donacion_material_publicitario de {} a {}'.format(
                    votante.donacion_material_publicitario,
                    parse_boolean(donacion_material_publicitario)
                ))
                votante.donacion_material_publicitario = parse_boolean(donacion_material_publicitario)
            if aportes_en_especie:
                cambios.append('aportes_en_especie de {} a {}'.format(
                    votante.aportes_en_especie,
                    parse_boolean(aportes_en_especie)
                ))
                votante.aportes_en_especie = parse_boolean(aportes_en_especie)
            if aportes_economicos:
                cambios.append('aportes_economicos de {} a {}'.format(
                    votante.aportes_economicos,
                    parse_boolean(aportes_economicos)
                ))
                votante.aportes_economicos = parse_boolean(aportes_economicos)
            if prestamo_vehiculo:
                cambios.append('prestamo_vehiculo de {} a {}'.format(
                    votante.prestamo_vehiculo,
                    parse_boolean(prestamo_vehiculo)
                ))
                votante.prestamo_vehiculo = parse_boolean(prestamo_vehiculo)
            if movilizar_avanzada:
                cambios.append('movilizar_avanzada de {} a {}'.format(
                    votante.movilizar_avanzada,
                    parse_boolean(movilizar_avanzada)
                ))
                votante.movilizar_avanzada = parse_boolean(movilizar_avanzada)
            if movilizacion_personal:
                cambios.append('movilizacion_personal de {} a {}'.format(
                    votante.movilizacion_personal,
                    parse_boolean(movilizacion_personal)
                ))
                votante.movilizacion_personal = parse_boolean(movilizacion_personal)
            if alojamiento_voluntarios:
                cambios.append('alojamiento_voluntarios de {} a {}'.format(
                    votante.alojamiento_voluntarios,
                    parse_boolean(alojamiento_voluntarios)
                ))
                votante.alojamiento_voluntarios = parse_boolean(alojamiento_voluntarios)
            if visitas_puerta_puerta:
                cambios.append('visitas_puerta_puerta de {} a {}'.format(
                    votante.visitas_puerta_puerta,
                    parse_boolean(visitas_puerta_puerta)
                ))
                votante.visitas_puerta_puerta = parse_boolean(visitas_puerta_puerta)
            if votaria_senado:
                cambios.append('votaria_senado de {} a {}'.format(
                    votante.votaria_senado,
                    parse_boolean(votaria_senado)
                ))
                votante.votaria_senado = parse_boolean(votaria_senado)
            if votaria_presidencia:
                cambios.append('votaria_presidencia de {} a {}'.format(
                    votante.votaria_presidencia,
                    parse_boolean(votaria_presidencia)
                ))
                votante.votaria_presidencia = parse_boolean(votaria_presidencia)
            if votaria_camara:
                cambios.append('votaria_camara de {} a {}'.format(
                    votante.votaria_camara,
                    parse_boolean(votaria_camara)
                ))
                votante.votaria_camara = parse_boolean(votaria_camara)
            if contactado:
                if parse_boolean(contactado):
                    llamada = True
                contactado = parse_boolean(contactado)
                cambios.append('contactado de {} a {}'.format(
                    votante.contactado,
                    contactado
                ))
                votante.contactado = contactado
            if no_volver_a_llamar:
                if parse_boolean(no_volver_a_llamar):
                    no_volver_a_contactar = True
                cambios.append('no_volver_a_llamar de {} a {}'.format(
                    votante.no_volver_a_llamar,
                    parse_boolean(no_volver_a_llamar)
                ))
                votante.no_volver_a_llamar = parse_boolean(no_volver_a_llamar)
            if celular_equivocado:
                if parse_boolean(celular_equivocado):
                    celular_equivocado_ = True
                cambios.append('celular_equivocado de {} a {}'.format(
                    votante.celular_equivocado,
                    parse_boolean(celular_equivocado)
                ))
                votante.celular_equivocado = parse_boolean(celular_equivocado)
            if no_contesto:
                if parse_boolean(no_contesto):
                    no_contesto_ = True
                cambios.append('no_contesto de {} a {}'.format(
                    votante.no_contesto,
                    parse_boolean(no_contesto)
                ))
                votante.no_contesto = parse_boolean(no_contesto)
            if celular_apagado:
                if parse_boolean(celular_apagado):
                    celular_apagado_ = True
                cambios.append('celular_apagado de {} a {}'.format(
                    votante.celular_apagado,
                    parse_boolean(celular_apagado)
                ))
                votante.celular_apagado = parse_boolean(celular_apagado)
            if voto_fuerte:
                votante.voto_fuerte = parse_boolean(voto_fuerte)
            if volver_a_llamar:
                votante.volver_a_llamar = parse_boolean(volver_a_llamar)
            votante.save()
            x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
            if x_forwarded_for:
                ip = x_forwarded_for.split(',')[-1].strip()
            elif request.META.get('HTTP_X_REAL_IP'):
                ip = request.META.get('HTTP_X_REAL_IP')
            else:
                ip = request.META.get('REMOTE_ADDR')
            LogVotante.objects.create(
                votante=votante,
                usuario_id=int(last_update_user),
                accion='Actualizacion',
                detalles=" ".join(cambios),
                ip=ip,
                llamada=llamada,
                no_volver_a_contactar=no_volver_a_contactar,
                celular_equivocado=celular_equivocado_,
                celular_apagado=celular_apagado_,
                no_contesto=no_contesto_
            )

            return HttpResponse(json.dumps({
                "data": "ok"
            }), content_type="application/json")
        return HttpResponse(json.dumps({
            "data": "fail"
        }), content_type="application/json")
    else:
        return HttpResponse(json.dumps({
            "data": "only post ;)"
        }), content_type="application/json")


class BigPagination(PageNumberPagination):
    page_size = 30

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('current_page', self.page.number),
            ('pages', self.page.paginator.num_pages),
            ('count', self.page.paginator.count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))


class VotanteGlobalView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = VotanteFiltroSerializer
    queryset = Votante.objects.filter(Q(duplicado=None) | Q(duplicado=False)).exclude(
        Q(cedula__isnull=True) | Q(cedula__exact='')
    )
    pagination_class = BigPagination

    def get_queryset(self):
        qs = super().get_queryset()

        contactado = self.request.query_params.get('contactado', None)
        no_volver_a_llamar = self.request.query_params.get('no_volver_a_llamar', None)
        volver_a_llamar = self.request.query_params.get('volver_a_llamar', None)
        celular_apagado = self.request.query_params.get('celular_apagado', None)
        no_contesto = self.request.query_params.get('no_contesto', None)
        cedula = self.request.query_params.get('cedula', None)
        departamento = self.request.query_params.get('departamento', None)
        municipio = self.request.query_params.get('municipio', None)
        corregimiento = self.request.query_params.get('corregimiento', None)
        edad_minima = self.request.query_params.get('edad_final__gte', None)
        edad_maxima = self.request.query_params.get('edad_final__lte', None)
        nombre = self.request.query_params.get('nombre', None)
        voluntario = self.request.query_params.get('voluntario', None)
        simpatizante = self.request.query_params.get('simpatizante', None)
        votantes_sin_cedula = self.request.query_params.get('votantes_sin_cedula', None)
        votantes_sin_nombre = self.request.query_params.get('votantes_sin_nombre', None)
        telefono = self.request.query_params.get('telefono', None)
        voto_fuerte = self.request.query_params.get('voto_fuerte', None)

        query = {}
        query_exclude = {}

        if parse_boolean(voto_fuerte):
            query['voto_fuerte'] = parse_boolean(voto_fuerte)
        if cedula:
            query['cedula'] = str(cedula)
        if parse_boolean(contactado):
            query['contactado'] = parse_boolean(contactado)
            query_exclude['contactado'] = not parse_boolean(contactado)
        if parse_boolean(no_volver_a_llamar):
            query['no_volver_a_llamar'] = parse_boolean(no_volver_a_llamar)
            query_exclude['no_volver_a_llamar'] = not parse_boolean(no_volver_a_llamar)
        if departamento:
            query['departamento_id'] = departamento
        if municipio:
            query['municipio_id'] = municipio
        if corregimiento:
            query['corregimiento_id'] = corregimiento
        if edad_minima:
            query['edad_final__gte'] = edad_minima
        if edad_maxima:
            query['edad_final__lte'] = edad_maxima
        if nombre:
            query['nombre__icontains'] = nombre
        if volver_a_llamar:
            query['volver_a_llamar'] = parse_boolean(volver_a_llamar)
            query_exclude['volver_a_llamar'] = not parse_boolean(volver_a_llamar)
        if celular_apagado:
            query['celular_apagado'] = parse_boolean(celular_apagado)
            query_exclude['celular_apagado'] = not parse_boolean(celular_apagado)
        if no_contesto:
            query['no_contesto'] = parse_boolean(no_contesto)
            query_exclude['no_contesto'] = not parse_boolean(no_contesto)
        if simpatizante:
            if parse_boolean(simpatizante):
                query['tipo_votante__icontains'] = 'SIMPATIZANTE'
        if voluntario:
            if parse_boolean(voluntario):
                query['tipo_votante__icontains'] = 'VOLUNTARIO'
        if telefono:
            query['ceular__icontains'] = telefono
        if votantes_sin_cedula:
            if parse_boolean(votantes_sin_cedula):
                qs = qs.filter(
                    Q(duplicado=None) | Q(duplicado=False) &
                    Q(cedula__isnull=True) | Q(cedula__exact=''))
        if votantes_sin_nombre:
            if parse_boolean(votantes_sin_nombre):
                qs = qs.filter(
                    Q(duplicado=None) | Q(duplicado=False) &
                    Q(nombre__isnull=True) | Q(nombre__exact='')
                )
        else:
            qs = qs.filter(
                Q(duplicado=None) | Q(duplicado=False),
                **query
            )
        return qs.exclude(
                Q(cedula__isnull=True) | Q(cedula__exact=''),
                **query_exclude
            )


class VotanteFromFuerzaCreateApiView(APIView):
    serializer_class = VotanteFuerzaSerializer

    def post(self, request, format=None):
        data = request.data.copy()
        lider = request.POST.get('lider', None)
        if lider:
            cc_lider = data['lider']
            lider = get_object_or_none(LiderElectoral, cedula=str(cc_lider))
            if not lider:
                lider = LiderElectoral.objects.create(cedula=str(cc_lider))
            data['lider'] = lider.id
        serializer = self.serializer_class(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
def comprobar_votante_existente(request):
    if request.method == 'POST':
        cedula = request.POST.get('cedula', None)
        response = {}
        if cedula:
            response['existe'] = False
            votante = Votante.objects.filter(cedula=cedula, duplicado__exact=None).only('id')
            if votante.count() > 0:
                response['existe'] = True
                response['pk'] = votante[0].id
        return HttpResponse(json.dumps(response), content_type="application/json")
    return HttpResponse(json.dumps({
        "data": "only post ;)"
    }), content_type="application/json")


class VotanteEmptyFieldsView(generics.ListAPIView):
    serializer_class = VotanteFiltroSerializer
    queryset = Votante.objects.all().exclude(duplicado=True)
    pagination_class = BigPagination
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        qs = super().get_queryset()
        sin_email = self.request.query_params.get('sin_email', None)
        sin_cedula = self.request.query_params.get('sin_cedula', None)
        sin_telefono = self.request.query_params.get('sin_telefono', None)
        sin_nombre = self.request.query_params.get('sin_nombre', None)
        sin_departamento = self.request.query_params.get('sin_departamento', None)
        sin_municipio = self.request.query_params.get('sin_municipio', None)

        if parse_boolean(sin_email):
            qs = qs.filter(Q(email__isnull=True) | Q(email__exact=''))
        if parse_boolean(sin_cedula):
            qs = qs.filter(Q(cedula__isnull=True) | Q(cedula__exact=''))
        if parse_boolean(sin_telefono):
            qs = qs.filter(Q(ceular__isnull=True) | Q(ceular__exact=''))
        if parse_boolean(sin_nombre):
            qs = qs.filter(Q(nombre__isnull=True) | Q(nombre__exact=''))
        if parse_boolean(sin_departamento):
            qs = qs.filter(departamento__isnull=True)
        if parse_boolean(sin_municipio):
            qs = qs.filter(municipio__isnull=True)
        return qs

