from rest_framework import serializers, pagination

from apps.utils.shortcuts import get_object_or_none
from apps.votantes.models import Votante, LiderElectoral


class VotanteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Votante
        fields = ('id',
                  'digitador',
                  'nombre',
                  'cedula',
                  'ceular',
                  'email',
                  'direccion',
                  'barrio_registrado',
                  'municipio',
                  'departamento',
                  'corregimiento',
                  'votaria_senado',
                  'votaria_camara',
                  'votaria_presidencia',
                  'voto_fuerte')


class VotanteFiltroSerializer(serializers.ModelSerializer):
    edad_actual = serializers.ReadOnlyField(source='get_current_edad')
    ubicacion = serializers.ReadOnlyField(source='get_location')

    class Meta:
        model = Votante
        fields = ('id',
                  'digitador',
                  'nombre',
                  'edad_actual',
                  'cedula',
                  'email',
                  'direccion',
                  'barrio_registrado',
                  'municipio',
                  'departamento',
                  'corregimiento',
                  'votaria_senado',
                  'votaria_camara',
                  'votaria_presidencia',
                  'contactado',
                  'no_volver_a_llamar',
                  'ubicacion',
                  'voto_fuerte',
                  'editor'
                  )


class VotanteDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Votante
        fields = (
            'id',
            'creacion',
            'ultima_actualizacion',
            'lider',
            'departamento',
            'municipio',
            'corregimiento',
            'digitador',
            'last_update_user',
            'barrio_registrado',
            'nombre',
            'ceular',
            'cedula',
            'email',
            'telefono_fijo',
            'direccion',
            'edad_final',
            'tipo_votante',
            'fecha_nacimiento',
            'genero',
            'sector_social',
            'ocupacion',
            'nivel_formacion',
            'tipo_material_publicitario',
            'tipo_aporte',
            'cantidad_aportada',
            'barrio_registrado',
            'consecutivo_padron'
        )


class VotantePerfilSerializer(serializers.ModelSerializer):
    class Meta:
        model = Votante
        fields = (
            'id',
            'creacion',
            'ultima_actualizacion',
            'lider',
            'departamento',
            'municipio',
            'corregimiento',
            'digitador',
            'last_update_user',
            'barrio_registrado',
            'nombre',
            'ceular',
            'cedula',
            'email',
            'telefono_fijo',
            'direccion',
            'edad_final',
            'tipo_votante',
            'fecha_nacimiento',
            'genero',
            'sector_social',
            'ocupacion',
            'referidos_fuera_residencia',
            'nivel_formacion',
            'lider_electoral',
            'tipo_material_publicitario',
            'tipo_aporte',
            'cantidad_aportada',
            'barrio_registrado',
            # booleans
            'egresado_unimag',
            'volanteo',
            'reuniones',
            'sondeos_opinion',
            'divulgacion_propuestas',
            'distribuir_material',
            'participacion_hechos_politicos',
            'avanzada_territorial',
            'capacitacion_voluntarios',
            'apoyos_redes_sociales',
            'coordinar_logistica_eventos',
            'procesamiento_base_de_datos',
            'donacion_material_publicitario',
            'aportes_en_especie',
            'aportes_economicos',
            'prestamo_vehiculo',
            'movilizar_avanzada',
            'movilizacion_personal',
            'alojamiento_voluntarios',
            'visitas_puerta_puerta',
            'votaria_senado',
            'votaria_camara',
            'votaria_presidencia',
            'confirmado_registraduria',
            'ultima_confirmacion_registraduria',
            'contactado',
            'no_volver_a_llamar',
        )


class VotanteFuerzaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Votante
        fields = ('nombre',
                  'cedula',
                  'email',
                  'ceular',
                  'telefono_fijo',
                  'direccion',
                  'barrio_registrado',
                  'lider_electoral',
                  'votaria_senado',
                  'votaria_camara',
                  'votaria_presidencia',
                  'departamento',
                  'municipio',
                  'fuerza_ciudadana',
                  'voto_fuerte')

