from rest_framework import generics, viewsets
from rest_framework.decorators import detail_route
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from apps.geodata.models import Departamento, Municipio, Corregimiento, Localidad, Barrio
from apps.utils.shortcuts import get_object_or_none
from . import serializers


class DepartamentoViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = serializers.DepartamentoSerializer
    queryset = Departamento.objects.all()
    permission_classes = (IsAuthenticated,)

    @detail_route(methods=['get'])
    def municipios(self, request, pk=None):
        departamento = get_object_or_none(Departamento, id=pk)
        municipios = Municipio.objects.filter(departamento_id=pk)
        serializer = serializers.MunicipioSerializer(municipios, many=True)
        return Response({
            'municipios': serializer.data,
            'votantes': departamento.cantidad_votantes(),
            'votantes_presidencia': departamento.get_votantes_presidencia().count(),
            'votantes_camara': departamento.get_votantes_camara().count(),
            'votantes_senado': departamento.get_votantes_senado().count(),
            'votantes_contactados': departamento.get_votantes_contactados().count(),
            'votantes_no_contactados': departamento.get_votantes_no_contactados().count(),
            'votantes_sin_cedula': departamento.get_votantes_sin_cedula().count(),
            'simpatizantes': departamento.get_simpatizantes().count(),
            'referidos': departamento.get_referidos().count(),
            'voluntario': departamento.get_voluntario().count(),
            'nuevos_hoy': departamento.get_nuevos_hoy().count()
        })

    @detail_route(methods=['get'])
    def municipios_(self, request, pk=None):
        departamento = get_object_or_none(Departamento, id=pk)
        municipios = Municipio.objects.filter(departamento_id=pk)
        serializer = serializers.MunicipioInfoSerializer(municipios, many=True)
        return Response({
            'municipios': serializer.data,
            'votantes': departamento.cantidad_votantes(),
            'votantes_presidencia': departamento.get_votantes_presidencia().count(),
            'votantes_camara': departamento.get_votantes_camara().count(),
            'votantes_senado': departamento.get_votantes_senado().count(),
            'votantes_contactados': departamento.get_votantes_contactados().count(),
            'votantes_no_contactados': departamento.get_votantes_no_contactados().count(),
            'votantes_sin_cedula': departamento.get_votantes_sin_cedula().count(),
            'simpatizantes': departamento.get_simpatizantes().count(),
            'referidos': departamento.get_referidos().count(),
            'voluntarios': departamento.get_voluntario().count(),
            'nuevos_hoy': departamento.get_nuevos_hoy().count()
        })


class DepartamentoBasicInfoViewSet(DepartamentoViewSet):
    serializer_class = serializers.DepartamentoInfoSerializer
    permission_classes = (IsAuthenticated,)


class MunicipioViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = serializers.MunicipioSerializer
    queryset = Municipio.objects.all()
    permission_classes = (IsAuthenticated,)

    @detail_route(methods=['get'])
    def localidades(self, request, pk=None):
        localidades = Localidad.objects.filter(municipio_id=pk)
        serializer = serializers.LocalidadSerializer(localidades, many=True)
        return Response({
            'localidades': serializer.data
        })

    @detail_route(methods=['get'])
    def corregimientos(self, request, pk=None):
        municipio = get_object_or_none(Municipio, id=pk)
        corregimientos = Corregimiento.objects.filter(municipio_id=pk)
        serializer = serializers.CorregimientoSerializer(corregimientos, many=True)
        return Response({
            'corregimientos': serializer.data,
            'votantes': municipio.cantidad_votantes(),
            'votantes_presidencia': municipio.get_votantes_presidencia().count(),
            'votantes_camara': municipio.get_votantes_camara().count(),
            'votantes_senado': municipio.get_votantes_senado().count(),
            'votantes_contactados': municipio.get_votantes_contactados().count(),
            'votantes_no_contactados': municipio.get_votantes_no_contactados().count(),
            'votantes_sin_cedula': municipio.get_votantes_sin_cedula().count(),
            'simpatizantes': municipio.get_simpatizantes().count(),
            'referidos': municipio.get_referidos().count(),
            'voluntarios': municipio.get_voluntario().count(),
            'nuevos_hoy': municipio.get_nuevos_hoy().count()
        })

    @detail_route(methods=['get'])
    def barrios(self, request, pk=None):
        barrios = Barrio.objects.filter(municipio_id=pk)
        serializer = serializers.BarrioSerializer(barrios, many=True)
        return Response({
            'barrios': serializer.data
        })


class MunicipioBasicInfoViewSet(MunicipioViewSet):
    serializer_class = serializers.MunicipioInfoSerializer
    permission_classes = (IsAuthenticated,)


class LocalidadViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = serializers.LocalidadSerializer
    queryset = Localidad.objects.all()
    permission_classes = (IsAuthenticated,)

    @detail_route(methods=['get'])
    def barrios(self, request, pk=None):
        barrios = Barrio.objects.filter(localidad_id=pk)
        serializer = serializers.LocalidadSerializer(barrios, many=True)
        return Response({
            'barrios': serializer.data
        })


