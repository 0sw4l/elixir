from django.conf.urls import url, include
from rest_framework import routers
from . import viewsets

router = routers.DefaultRouter()
router.register(r'departamentos', viewsets.DepartamentoViewSet)
router.register(r'municipios', viewsets.MunicipioViewSet)
router.register(r'localidad', viewsets.LocalidadViewSet)
router.register(r'departamentos_', viewsets.DepartamentoBasicInfoViewSet)
router.register(r'municipios_', viewsets.MunicipioBasicInfoViewSet)

urlpatterns = [
    url(r'^viewsets/', include(router.urls)),
]
