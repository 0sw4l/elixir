from rest_framework import serializers
from apps.third_party_apps.rest_framework_gis import serializers as geo_serializers

from apps.geodata.models import Departamento, Municipio, Corregimiento, Localidad, Barrio


class DepartamentoSerializer(geo_serializers.GeoFeatureModelSerializer):

    latitud = serializers.ReadOnlyField(source='get_latitude')
    longitud = serializers.ReadOnlyField(source='get_longitude')
    votantes = serializers.ReadOnlyField(source='cantidad_votantes')
    votantes_sin_cedula = serializers.ReadOnlyField(source='get_votantes_sin_cedula_count')
    votantes_no_contactados = serializers.ReadOnlyField(source='get_votantes_no_contactados_count')
    votantes_presidencia = serializers.ReadOnlyField(source='get_votantes_presidencia_count')
    votantes_camara = serializers.ReadOnlyField(source='get_votantes_camara_count')
    votantes_senado = serializers.ReadOnlyField(source='get_votantes_senado_count')
    votantes_contactados = serializers.ReadOnlyField(source='get_votantes_contactados_count')

    class Meta:
        model = Departamento
        geo_field = 'area'
        fields = ('id', 'nombre', 'area_total', 'perimetro',
                  'hectareas', 'area', 'latitud', 'longitud',
                  'votantes', 'votantes_sin_cedula', 'votantes_presidencia', 'votantes_camara', 'votantes_senado',
                  'votantes_contactados', 'votantes_no_contactados'
                  )


class DepartamentoInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Departamento
        fields = ('id', 'nombre')


class MunicipioSerializer(geo_serializers.GeoFeatureModelSerializer):

    latitud = serializers.ReadOnlyField(source='get_latitude')
    longitud = serializers.ReadOnlyField(source='get_longitude')
    departamento = serializers.ReadOnlyField(source='departamento.nombre')
    corregimientos = serializers.ReadOnlyField(source='get_cantidad_corregimientos')
    votantes = serializers.ReadOnlyField(source='cantidad_votantes')
    votantes_sin_cedula = serializers.ReadOnlyField(source='get_votantes_sin_cedula_count')
    votantes_no_contactados = serializers.ReadOnlyField(source='get_votantes_no_contactados_count')
    votantes_presidencia = serializers.ReadOnlyField(source='get_votantes_presidencia_count')
    votantes_camara = serializers.ReadOnlyField(source='get_votantes_camara_count')
    votantes_senado = serializers.ReadOnlyField(source='get_votantes_senado_count')
    votantes_contactados = serializers.ReadOnlyField(source='get_votantes_contactados_count')

    class Meta:
        model = Municipio
        geo_field = 'area'
        fields = ('id', 'nombre', 'area', 'latitud', 'longitud', 'departamento', 'corregimientos',
                  'votantes', 'votantes_sin_cedula', 'votantes_presidencia', 'votantes_camara', 'votantes_senado',
                  'votantes_contactados', 'votantes_no_contactados')


class MunicipioInfoSerializer(serializers.ModelSerializer):
    corregimientos = serializers.ReadOnlyField(source='get_cantidad_corregimientos')

    class Meta:
        model = Municipio
        fields = ('id', 'nombre', 'departamento', 'corregimientos',)


class CorregimientoSerializer(geo_serializers.GeoFeatureModelSerializer):
    latitud = serializers.ReadOnlyField(source='get_latitude')
    longitud = serializers.ReadOnlyField(source='get_longitude')
    departamento = serializers.ReadOnlyField(source='municipio.departamento.nombre')
    municipio = serializers.ReadOnlyField(source='municipio.nombre')

    class Meta:
        model = Corregimiento
        geo_field = 'coordenadas'
        fields = ('id', 'nombre', 'coordenadas', 'longitud', 'latitud', 'departamento', 'municipio')


class LocalidadSerializer(serializers.ModelSerializer):
    municipio = serializers.ReadOnlyField(source='municipio.nombre')

    class Meta:
        model = Localidad
        fields = ('id', 'nombre', 'municipio')


class BarrioSerializer(serializers.ModelSerializer):
    municipio = serializers.ReadOnlyField(source='municipio.nombre')
    localidad = serializers.ReadOnlyField(source='localidad.nombre')

    class Meta:
        model = Barrio
        fields = ('id', 'nombre', 'municipio', 'localidad')


