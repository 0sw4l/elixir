from django.contrib import admin
from . import models
# Register your models here.


@admin.register(models.Reporte)
class ReporteAdmin(admin.ModelAdmin):
    list_display = ['id', 'hora', 'fecha', 'usuario', 'archivo']
