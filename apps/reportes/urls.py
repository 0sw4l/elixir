from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^reporte_llamadas/',
        views.ReporteLLamadaTemplateView.as_view(),
        name='reporte_llamadas'),

    url(r'^reporte_votantes_nuevos/',
        views.ReporteVotantesNuevosFecha.as_view(),
        name='reporte_votantes_nuevos_fecha'),

    url(r'^reportes/',
        views.ReportesGeneradosListView.as_view(),
        name='reportes_generados')
]
