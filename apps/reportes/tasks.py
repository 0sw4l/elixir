from datetime import datetime
from django.db import connection
import xlwt, xlrd, re
from io import BytesIO

from django.core.files.base import ContentFile
from django.db.models import Q

from apps.reportes.constants import SIN_NOMBRE, SIN_CEDULA
from apps.reportes.models import Reporte
from apps.utils.fitsheet import FitSheetWrapper
from apps.votantes.models import Votante
from project.celery import app
import celery


class ReporteVotantes(object):
    query = None

    def __init__(self, args, attrs, exclude):
        if args == SIN_NOMBRE:
            self.query = Votante.objects.filter((Q(nombre__isnull=True) | Q(nombre__exact='')))
        elif args == SIN_CEDULA:
            self.query = Votante.objects.filter((Q(cedula__isnull=True) | Q(cedula__exact='')))
        else:
            self.query = Votante.objects.filter(**attrs).exclude(**exclude)

    @property
    def file_name(self):
        return 'reporte de votantes generado {}.xls'.format(datetime.now().date())


class DbTask(celery.Task):
    abstract = True

    def after_return(self, *args, **kwargs):
        connection.close()


@app.task(base=DbTask)
def crear_reporte_votantes_excel(user, args, attrs, exclude):
    report = ReporteVotantes(args=args, attrs=attrs, exclude=exclude)
    query = report.query
    workbook = xlwt.Workbook(encoding='utf8')
    worksheet = workbook.add_sheet("reporte de votantes")
    row_num = 0
    worksheet = FitSheetWrapper(worksheet)
    columns = ['cedula',
               'nombre',
               'edad',
               'departamento',
               'municipio',
               'corregimiento',
               'direccion',
               'telefono',
               'voto fuerte',
               'votaria senado',
               'votaria camara',
               'primera vuelta',
               'contactado']

    for col_num in range(len(columns)):
        worksheet.write(row_num, col_num, columns[col_num])

    for votante in query[:5000]:
        row_num += 1
        row = [
            votante.cedula if votante.cedula else '',
            votante.nombre if votante.nombre else '',
            votante.get_current_edad(),
            votante.departamento.nombre if votante.departamento else '',
            votante.municipio.nombre if votante.municipio else '',
            votante.corregimiento.nombre if votante.corregimiento else '',
            votante.direccion if votante.direccion else '',
            votante.ceular if votante.ceular else '',
            'SI' if votante.voto_fuerte else 'NO',
            'SI' if votante.votaria_senado else 'NO',
            'SI' if votante.votaria_camara else 'NO',
            'SI' if votante.contactado else 'NO',
            'SI' if votante.votaria_presidencia else 'NO'
        ]
        for col_num in range(len(row)):
            worksheet.write(row_num, col_num, row[col_num])

    file = BytesIO()
    workbook.save(file)
    file.seek(0)
    reporte = Reporte()
    reporte.usuario_id = user
    reporte.archivo.save(name=report.file_name, content=ContentFile(file.getvalue()))
    file.close()
    reporte.save()
