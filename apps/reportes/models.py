from django.db import models

# Create your models here.


class Reporte(models.Model):
    archivo = models.FileField(upload_to='reportes')
    fecha = models.DateField(auto_now_add=True)
    hora = models.TimeField(auto_now_add=True)
    usuario = models.ForeignKey('auth.User')

    class Meta:
        verbose_name = 'Reporte'
        verbose_name_plural = 'Reportes'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
