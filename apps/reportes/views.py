from django.shortcuts import render

# Create your views here.
from apps.main.models import LogVotante
from apps.reportes.models import Reporte
from apps.utils.views import BaseTemplateView, BaseListView
from apps.votantes.models import Votante


class ReporteLLamadaTemplateView(BaseTemplateView):
    template_name = 'apps/reportes/reporte_llamadas.html'

    def post(self, request, **kwargs):
        fecha = request.POST.get('fecha', None)
        print(fecha)
        context = {}
        if fecha:
            context['datos'] = True
            llamadas = LogVotante.get_llamadas_from_date(date=fecha)
            context['llamadas'] = llamadas[:100]
            context['cantidad_llamadas'] = llamadas.count()
            context['contestadas'] = LogVotante.get_llamadas_contestadas_form_date(date=fecha)
            context['celular_apagado'] = LogVotante.get_celular_apagado_form_date(date=fecha)
            context['celular_equivocado'] = LogVotante.get_celular_equivocado_form_date(date=fecha)
            context['no_contestadas'] = LogVotante.get_llamadas_no_contestadas_form_date(date=fecha)
            context['no_volver_a_llamar'] = LogVotante.get_no_volver_a_llamar_form_date(date=fecha)
        context['fecha'] = fecha
        return render(request, self.template_name, context)


class ReporteVotantesNuevosFecha(BaseTemplateView):
    template_name = 'apps/reportes/reporte_votantes_nuevos.html'

    def post(self, request, **kwargs):
        context = {'error': False}
        fecha_minima = request.POST.get('fecha_minima', None)
        fecha_maxima = request.POST.get('fecha_maxima', None)
        fecha_especifica = request.POST.get('fecha_especifica', None)
        rango = request.POST.get('rango', None)
        votantes = 0
        if rango == 'on':
            if fecha_minima != '' and fecha_maxima == '':
                context['fecha'] = fecha_minima
                votantes = Votante.get_votantes_nuevos_fecha1(fecha_minima).count()
            elif fecha_minima == '' and fecha_maxima != '':
                votantes = Votante.get_votantes_nuevos_fecha2(fecha_maxima).count()
                context['fecha'] = fecha_maxima
            elif fecha_minima != '' and fecha_maxima != '':
                context['fecha'] = '{} - {}'.format(fecha_minima, fecha_maxima)
                votantes = Votante.get_votantes_nuevos_fecha1_fecha2(fecha_minima, fecha_maxima).count()
            else:
                context['error'] = True
        else:
            if fecha_especifica != '':
                votantes = Votante.get_votantes_nuevos_fecha_especifica(fecha_especifica).count()
                context['fecha'] = fecha_especifica
            else:
                context['error'] = True
        context['votantes'] = votantes
        return render(request, self.template_name, context)


class ReportesGeneradosListView(BaseListView):
    template_name = 'apps/reportes/reportes_generados.html'
    model = Reporte

    def get_queryset(self):
        query = self.model.objects.all()
        if not hasattr(self.request.user, 'is_superuser'):
            query = self.model.objects.filter(usuario=self.request.user)
        return query.order_by('-id')
