#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime, re
from django.shortcuts import _get_queryset

from apps.third_party_apps.gis.geos import Point


def get_object_or_none(klass, *args, **kwargs):
    queryset = _get_queryset(klass)
    try:
        return queryset.get(*args, **kwargs)
    except:
        return None


def get_list_or_none(klass, *args, **kwargs):
    queryset = _get_queryset(klass)
    obj_list = list(queryset.filter(*args, **kwargs))
    if not obj_list:
        return None
    return obj_list


def generate_code(klass):
    out = 0
    items = klass.objects.all().count() + 1
    if items < 10:
        out = '000{0}'.format(items)
    elif 10 >= items < 100:
        out = '00{0}'.format(items)
    elif 100 >= items < 1000:
        out = '0{0}'.format(items)
    elif items >= 1000:
        out = '{0}'.format(items)
    data = out
    object_ = get_object_or_none(klass, codigo=data)
    if object_ is None:
        return data
    else:
        generate_code(klass)


def get_coordenadas(longitud, latitud):
    return Point(
        float_(longitud),
        float_(latitud)
    ) if longitud != '0,' and latitud != '0,' else None


def float_(float_string):
    """It takes a float string ("1,23" or "1,234.567.890") and
        converts it to floating point number (1.23 or 1.234567890).
    """
    float_string = str(float_string)
    errormsg = "ValueError: Input must be decimal or integer string"
    try:
        if float_string.count(".") == 1 and float_string.count(",") == 0:
            return float(float_string)
        else:
            midle_string = list(float_string)
            while midle_string.count(".") != 0:
                midle_string.remove(".")
            out_string = str.replace("".join(midle_string), ",", ".")
        return float(out_string)
    except ValueError:
        "%s\n%s".format(errormsg)
        return None


def remove_accent(chain):
    if type(chain) is str:
        chain = chain.replace('á', 'a')
        chain = chain.replace('Á', 'A')
        chain = chain.replace('é', 'e')
        chain = chain.replace('É', 'E')
        chain = chain.replace('í', 'i')
        chain = chain.replace('Í', 'I')
        chain = chain.replace('ó', 'o')
        chain = chain.replace('Ó', 'O')
        chain = chain.replace('ú', 'u')
        chain = chain.replace('Ú', 'U')
        chain = chain.replace('Ü', 'U')
        return chain
    else:
        return None


def remove_yen(chain):
    return chain.replace('¥', 'Ñ')


def get_corregimiento(municipio):
    return re.search('\(([^)]+)', municipio).group(1)


def get_municipio(municipio):
    municipio = re.sub(r'\([^)]*\)', '', municipio)
    municipio = municipio[:-1]
    return municipio


def parse_boolean(chain):
    try:
        if 'true' in chain:
            return True
        else:
            return False
    except:
        return False
