import datetime
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views import generic


class AdminPermissionMixin(generic.View):

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_superuser:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)


class DigitadorLockAccessTimeMixin(generic.View):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if hasattr(self.request.user, 'digitador'):
            now = datetime.datetime.now()
            hora_entrada = now.replace(hour=7, minute=30, second=0, microsecond=0)
            hora_salida = now.replace(hour=20, minute=0, second=0, microsecond=0)
            if now <= hora_entrada or now >= hora_salida:
                return redirect('main:entrar')
        return super().dispatch(request, *args, **kwargs)

