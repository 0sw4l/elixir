import requests

from apps.geodata.models import Municipio
from apps.utils.print_colors import _green, _cyan, _red


def run():
    request = requests.get('https://restcountries.eu/rest/v2/all/')
    response = request.json()

    for country in response:
        pais = country['translations']['es']
        print(pais)
        if pais is not None:
            Municipio.objects.create(
                departamento_id=34,
                nombre=pais.upper()
            )
            print('{} : {} :D'.format(_green('PAIS Creado'), _cyan(pais)))
        else:
            print('{} {} {} no es valido'.format(_red('Error'), _green('el pais'), _cyan(pais)))
