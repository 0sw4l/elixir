from apps.utils.print_colors import _green, _cyan
from apps.votantes.models import Votante


def run():
    votantes = Votante.objects.filter(editor__isnull=False)
    print('votantes cautivos : {}'.format(_cyan(votantes.count())))
    print('{}'.format(
        _green('liberando votantes cautivos ')
    ))
    votantes.update(editor=None)
    print(_green('votantes liberados'))



