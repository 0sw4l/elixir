# python manage_local.py runscript reset_comparados
from django.db.models import Q

from apps.utils.print_colors import _green, _orange
from apps.votantes.models import Votante


def run():
    comparados = Votante.objects.filter(comparado=True).only(
        'id', 'comparado', 'duplicado'
    )
    print('{} : {}'.format(_orange('total comparados '), _green(comparados.count())))
    comparados.update(
        comparado=None, duplicado=None, attrs_with_value=0
    )
    print('{}'.format(_green('reset records')))
