from apps.votantes.models import Votante


def run():
    Votante.objects.filter(duplicado=None).update(duplicado=False)
