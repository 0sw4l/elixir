# python manage_local.py runscript contar
import os
from django.db.models import Q
from apps.utils.print_colors import _orange, _green, _cyan, _purple, _red
from apps.votantes.models import Votante


def run():
    cls()
    comparados = Votante.objects.filter(comparado__exact=True).exclude(
        Q(cedula__isnull=True) | Q(cedula__exact='')
    )
    todos = Votante.objects.all().exclude(
        Q(cedula__isnull=True) | Q(cedula__exact='')
    )
    print('{} {} / {} - {} {}'.format(
        _orange('Total comparados :'),
        _green(comparados.count()),
        _cyan(todos.count()),
        _purple('Faltan :'),
        _red(todos.count() - comparados.count())
    ))


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')
