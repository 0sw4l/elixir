import requests
import unidecode

from apps.geodata.models import Departamento, Municipio
from apps.third_party_apps.gis.geos import GEOSGeometry
from apps.utils import print_colors as color
from apps.utils.shortcuts import get_object_or_none, remove_yen


def run(app, schema_editor):
    print('\n')
    print(color._green('Welcome to api colombia by 0sw4l and N!ck'))

    request = requests.get('https://raw.githubusercontent.com/santiblanko/colombia.geojson/master/mpio.json')
    response = request.json()

    print('{} {} {}'.format(color._green('Request'), color._cyan('Municipios'), color._green('Ready')))
    request_municipios = requests.get('https://gist.githubusercontent.com/john-guerra/43c7656821069d00dcbc/raw/3aadedf47badbdac823b00dbe259f6bc6d9e1899/colombia.geo.json')
    response_departamentos = request_municipios.json()
    print('{} {} {}'.format(color._green('Request'), color._orange('Departamentos'), color._green('Ready')))

    for item in response_departamentos['features']:

        nombre_departamento = remove_yen(item['properties']['NOMBRE_DPT'])
        departamento = get_object_or_none(Departamento, nombre=nombre_departamento)

        if not departamento:
            departamento = Departamento.objects.create(
                nombre=remove_yen(nombre_departamento.upper()),
                numero=item['properties']['DPTO'],
                perimetro=item['properties']['PERIMETER'],
                area_total=item['properties']['AREA'],
                hectareas=item['properties']['HECTARES'],
                area=GEOSGeometry(str(item['geometry']))
            )
            print('{} : {}'.format('se creo el departamento', color._cyan(departamento.nombre)))

    for item in response['features']:
        nombre_municipio = remove_yen(item['properties']['NOMBRE_MPI'])
        municipio = Municipio.objects.create(
            nombre=nombre_municipio.upper(),
            departamento=get_object_or_none(Departamento, nombre=remove_yen(item['properties']['NOMBRE_DPT'])),
            nombre_cab=remove_yen(item['properties']['NOMBRE_CAB']),
            area=GEOSGeometry(str(item['geometry']))
        )
        print('Municipio {} , se le asigno el departamento {}'.format(
            color._green(municipio.nombre),
            color._cyan(municipio.departamento.nombre),
        ))

    print(color._orange('Bye ;) informacion llena'))


