from django.db.models import Q

from apps.utils.print_colors import _green, _cyan, _orange, _blue, _red
from apps.votantes.models import Votante


def run():
    print('limpiando los numeros de la base de datos')
    espacios = 0
    parentesis = 0
    nd = 0
    punto = 0
    votantes = Votante.objects.filter(
        ceular=''
    ).only('ceular', 'id')
    cantidad_votantes = votantes.count()
    print('cantidad votantes: {}'.format(_green(cantidad_votantes)))
    contador = 0
    lte_10 = 0
    for v in votantes:
        print('votante {} de {}, celular : {}'.format(
            _cyan(contador+1),
            _green(cantidad_votantes),
            _blue(v.ceular)
        ))

        if ' ' in v.ceular:
            v.ceular = v.ceular.replace(' ', '')
            espacios += 1
            print(_orange('Se encontraron espacios'))
        if '(' in v.ceular or ')' in v.ceular:
            v.ceular = v.ceular.replace('(', '')
            v.ceular = v.ceular.replace(')', '')
            print(_orange('Se encontraron parentesis'))
            parentesis += 1
        if 'ND' in v.ceular:
            v.ceular = ''
            nd += 1
            print(_orange('Se encontraron ND'))
        if '.0' in v.ceular[-2:]:
            v.ceular = v.ceular.replace('.0', '')
            punto += 1
            print(_orange('Se encontraron .0'))
        if '.' in v.ceular:
            v.ceular = v.ceular.replace('.', '')
            punto += 1

        if len(v.ceular) > 10:
            lte_10 += 1
            v.ceular = v.ceular[:10]
            print('{}'.format(
                _orange('celular invalido, intenando corregir')
            ))
        elif len(v.ceular) < 10:
            v.ceular = None
            print(_red('celular invalido'))

        if v.ceular is '':
            v.ceular = None
            print(_red('celular invalido porque esta vacio como mi corazon 💔'))

        v.save()

        print('{} : {}'.format(
            _cyan('nuevo celular'),
            _orange(v.ceular)
        ))
        contador += 1
    print('total con espacios: {}'.format(espacios))
    print('total con parentesis: {}'.format(parentesis))
    print('total con ND : {}'.format(nd))
    print('total con .0 al final : {}'.format(punto))
    print('total con mas de 10 numeros : {}'.format(lte_10))
