from apps.utils.print_colors import _green
from project.celery import app


def run():
    app.control.purge()
    print('{}'.format(_green('Stop Celery')))
